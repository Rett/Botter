local cp = {}
local paths = {}

local function index_validity_check(val)
    if tonumber(val) then
        error('Sorry, the numeric range of paths is reserved.')
    elseif tostring(val) == '_meta' then
        return false
    end
    return true
end

function cp:mode()
    -- Returns the current mode
    return rawget(rawget(self,'_meta'),'mode')
end

function cp:finished()
	return false
end

function cp:length()
    -- Returns the length of the node list
    return rawget(rawget(self,'_meta'),'length')
end

function cp:pos(t)
    -- Returns your current node
    return rawget(rawget(self,'_meta'),'pos')
end

function cp:append(v)
    -- Adds a new node to the end of the path
    rawset(self,self:length()+1,v)
    rawset(rawget(self,'_meta'),'length',self:length()+1)
end

function cp:last()
    -- Returns the last node in the node list
    return rawget(self,self:length())
end


function cp:get_n_coords()
    -- Returns your current node's coordinates.
    return rawget(self,self:pos())
end

function cp:prv()
    -- Moves you 1 node backwards
    local dir = rawget(rawget(self,'_meta'),'dir')
    local length = self:length()
    local newpos = (self:pos() - dir - 1)%length + 1
    rawset(rawget(self,'_meta'),'pos',newpos)
    return self:get_n_coords()
end

-- function lp:nxt()
    -- -- Move one step in the current direction
    -- local dir = rawget(rawget(self,'_meta'),'dir')
    -- local pos = rawget(rawget(self,'_meta'),'pos') + dir
    -- if pos > self:length() then
        -- rawset(rawget(self,'_meta'),'dir',dir*-1)
        -- pos = pos - 2
    -- elseif pos < 1 then
        -- rawset(rawget(self,'_meta'),'dir',dir*-1)
        -- pos = pos + 2
    -- end
    -- rawset(rawget(self,'_meta'),'pos',pos)
    -- local points = rawget(rawget(self,'_meta'),'points')
    -- return rawget(points,pos)
-- end

function cp:nxt()
    -- Moves you 1 node forward
    local dir = rawget(rawget(self,'_meta'),'dir')
    local length = self:length()
    local newpos = (self:pos() + dir - 1)%length + 1
    rawset(rawget(self,'_meta'),'pos',newpos)
    return self:get_n_coords()
end

function cp:move(k)
    -- Walks you forwards (+) or backwards (-) x nodes
    local n_coords = self:get_n_coords()
    while k > 0 do
        n_coords = self:nxt()
        k = k - 1
    end
    while k < 0 do
        n_coords = self:prv()
        k = k + 1
    end
    return n_coords
end

function cp:get_closest(coo)
    if self:length() <= 1 then return false end
    local c_coords = coo or utils.get_c_coords()
    local diff = {}
    for index = 1,self:length() do
        diff[index] = utils.dist(self[index],c_coords)
    end
    local minimum = math.min(unpack(diff))
    for index,dist in pairs(diff) do
        if dist == minimum then
            return index-self:pos() + 1
        end
    end
end

function cp:get_loc_min_dist(coo)
    if self:length() <= 1 then return false end
    local c_coords = coo or utils.get_c_coords()
    local diff = {}
    for index = -math.floor(self:length()/2),math.floor(self:length()/2) do
        if not self[index] or not c_coords then
            print(index,self[index],c_coords)
        end
        diff[index] = utils.dist(self[index],c_coords)
    end
    local min_pos,min_neg = math.huge,math.huge
    local lind_pos,lind_neg = 0,0
    local freeze_pos,freeze_neg = false,false
    for index = 0,math.floor(self:length()/2)+1 do
        if not freeze_pos and diff[index] > min_pos then
            freeze_pos = true
        elseif not freeze_pos then
            min_pos = diff[index]
            lind_pos = index
        end
        if not freeze_neg and diff[-index] and diff[-index] > min_neg then
            freeze_neg = true
        elseif not freeze_neg and diff[-index] then
            min_neg = diff[-index]
            lind_neg = -index
        end
    end
	
	--print('loc min dist: ',string.format("%.4f",min_neg),lind_neg,string.format("%.4f",min_pos),lind_pos)
    local negligable_difference = (min_neg - min_pos) < 1 -- If the indices are almost equidistance (or literally the same point)
    --print(min_neg,min_pos,lind_pos,lind_neg)
    if negligable_difference then
        if math.abs(lind_pos) - 2 < math.abs(lind_neg) then
            -- Bias towards not backtracking
            return lind_pos,{lind_pos,lind_neg,min_pos,min_neg,math.floor(self:length()/2)+1,utils.dist(self[1],c_coords),utils.dist(self[-1],c_coords),self:pos()}
        else
            return lind_neg,{lind_pos,lind_neg,min_pos,min_neg,math.floor(self:length()/2)+1,utils.dist(self[1],c_coords),utils.dist(self[-1],c_coords),self:pos()}
        end
    elseif min_pos < min_neg then
        return lind_pos,{lind_pos,lind_neg,min_pos,min_neg,math.floor(self:length()/2)+1,utils.dist(self[1],c_coords),utils.dist(self[-1],c_coords),self:pos()}
    else
        return lind_neg,{lind_pos,lind_neg,min_pos,min_neg,math.floor(self:length()/2)+1,utils.dist(self[1],c_coords),utils.dist(self[-1],c_coords),self:pos()}
    end
end

function paths.new_circular_path(tab)
    
    local t = {_meta={length=#tab,pos=1,dir=1,mode="circular"}}
    for i,v in ipairs(tab) do t[i] = v end
    setmetatable(t,{
    __index=function(t,k)
        if tonumber(k) then
            -- Treat k as an offset.
            local dir = rawget(rawget(t,'_meta'),'dir')
            local pos = rawget(rawget(t,'_meta'),'pos')
            local length = rawget(rawget(t,'_meta'),'length')
            local ind = (pos + k * dir -1)%length +1 -- 1-based, max value of length

            return rawget(t,ind)
        else
            return rawget(cp,k)
        end
    end,
    __newindex=function(t,k,v)
        k = tonumber(k) or k
        
        -- Need to adjust the current position because the length is potentially changing.
        local pos = rawget(rawget(t,'_meta'),'pos')
        local length = rawget(rawget(t,'_meta'),'length')
        rawset(rawget(t,'_meta'),'pos',(pos-1)%length+1)
        
        if index_validity_check(k) then
            rawset(t,k,v)
        end
    end})
    rawset(rawget(t,'_meta'),'pos',t:get_closest())
    return t
end




local lp = {}

function lp:mode()
    -- Returns the current mode
    return rawget(rawget(self,'_meta'),'mode')
end

function lp:length()
    -- Returns the length of the node list
    return rawget(rawget(self,'_meta'),'length')
end

function lp:append(v)
    -- Adds a new node
    local points = rawget(rawget(self,'_meta'),'points')
    rawset(points,self:length()+1,v)
    rawset(rawget(self,'_meta'),'length',self:length()+1)
end

function lp:last()
    -- Returns the last node in the node list
    local points = rawget(rawget(self,'_meta'),'points')
    return rawget(points,self:length())
end

function lp:pos()
    -- Returns your current node
    return rawget(rawget(self,'_meta'),'pos')
end

function lp:get_n_coords()
    -- Returns the coordinates of your current node
    local points = rawget(rawget(self,'_meta'),'points')
    return rawget(points,self:pos()),self:pos()
end

function lp:prv()
    -- Move one step in the opposite direction
    local dir = rawget(rawget(self,'_meta'),'dir')
    local pos = rawget(rawget(self,'_meta'),'pos') - dir
    if pos > self:length() then
        rawset(rawget(self,'_meta'),'dir',dir*-1)
        pos = pos - 2
    elseif pos < 1 then
        rawset(rawget(self,'_meta'),'dir',dir*-1)
        pos = pos + 2
    end
    rawset(rawget(self,'_meta'),'pos',pos)
    
    local points = rawget(rawget(self,'_meta'),'points')
    return rawget(points,pos)
end

function lp:nxt()
    -- Move one step in the current direction
    local dir = rawget(rawget(self,'_meta'),'dir')
    local pos = rawget(rawget(self,'_meta'),'pos') + dir
    if pos > self:length() then
        rawset(rawget(self,'_meta'),'dir',dir*-1)
        pos = pos - 2
    elseif pos < 1 then
        rawset(rawget(self,'_meta'),'dir',dir*-1)
        pos = pos + 2
    end
    rawset(rawget(self,'_meta'),'pos',pos)
    local points = rawget(rawget(self,'_meta'),'points')
    return rawget(points,pos)
end

function lp:finished()
	return self:pos() == self:length()
end

function lp:move(k)
    local n_coords = self:get_n_coords()
    while k > 0 do
        n_coords = self:nxt()
        k = k - 1
    end
    while k < 0 do
        n_coords = self:prv()
        k = k + 1
    end
    return n_coords
end

function lp:get_closest(coo)
    -- Returns the node offset of the nearest node.
    if self:length() <= 1 then return false end
    local c_coords = coo or utils.get_c_coords()
    local min_dist = utils.dist(self:get_n_coords(),c_coords)
    local pos = self:pos()
    local ret_ind = 0
    
    for index = 1-pos,self:length()-pos do
        local dist = utils.dist(self[index],c_coords)
        if dist < min_dist then
            min_dist = dist
            ret_ind = index
        end
    end
    return ret_ind
end

function lp:get_loc_min_dist(c_coords,tolerance)
    local tolerance = tolerance or 2
    -- Returns the node offset of the closest node, constrained to being a local minimum
    if self:length() <= 2 then return false end
    c_coords = c_coords or utils.get_c_coords() -- Player coordinates
    local min_pos = utils.dist(self:get_n_coords(),c_coords) -- Distance from the current waypoint
    local min_neg = min_pos
    local lind_pos,lind_neg = 0,0 -- Current waypoint's offset
    for index = 1,(math.floor(self:length()/2)+1) do
        local distance = utils.dist(self[index],c_coords)
        if distance < min_pos then
            min_pos = utils.dist(self[index],c_coords)
            lind_pos = index
        elseif distance > (min_pos + tolerance) then
            break
        end
    end
    for index = 1,(math.floor(self:length()/2)+1) do
        local distance = utils.dist(self[-index],c_coords)
        if distance < min_neg then
            min_neg = utils.dist(self[-index],c_coords)
            lind_neg = -index
        elseif distance > (min_neg + tolerance) then
            break
        end
    end
    --print('loc min dist: ',string.format("%.4f",min_neg),lind_neg,string.format("%.4f",min_pos),lind_pos)
    local negligable_difference = (min_neg - min_pos) < 1 -- If the indices are almost equidistance (or literally the same point)
    --print(min_neg,min_pos,lind_pos,lind_neg)
    if negligable_difference then
        if math.abs(lind_pos) - 2 < math.abs(lind_neg) then
            -- Bias towards not backtracking
            return lind_pos,{lind_pos,lind_neg,min_pos,min_neg,math.floor(self:length()/2)+1,utils.dist(self[1],c_coords),utils.dist(self[-1],c_coords),self:pos()}
        else
            return lind_neg,{lind_pos,lind_neg,min_pos,min_neg,math.floor(self:length()/2)+1,utils.dist(self[1],c_coords),utils.dist(self[-1],c_coords),self:pos()}
        end
    elseif min_pos < min_neg then
        return lind_pos,{lind_pos,lind_neg,min_pos,min_neg,math.floor(self:length()/2)+1,utils.dist(self[1],c_coords),utils.dist(self[-1],c_coords),self:pos()}
    else
        return lind_neg,{lind_pos,lind_neg,min_pos,min_neg,math.floor(self:length()/2)+1,utils.dist(self[1],c_coords),utils.dist(self[-1],c_coords),self:pos()}
    end
end

local linpath_metatable = { __index=function(t,k)
        -- For some reason +1 gives point 1, as if it was rawget.
        if tonumber(k) then
            k = tonumber(k)
            local k_counter = math.floor(k)
            local k_polarity = k/math.abs(k)
            local dir = rawget(rawget(t,'_meta'),'dir')
            local pos = t:pos()
            
            while k_counter ~= 0 do
                pos = pos + dir*k_polarity
                if pos > t:length() then
                    dir = dir*-1
                    pos = pos - 2
                elseif pos < 1 then
                    dir = dir*-1
                    pos = pos + 2
                end
                -- k_counter=1 seems to increment until dir switches?
                k_counter = k_counter - k_polarity -- decrement k towards 0
            end
            
            return rawget(rawget(rawget(t,'_meta'),'points'),pos)
        else
            return rawget(lp,k)
        end
    end,
    
    __newindex=function(tab,k,v)
        k = tonumber(k) or k
        if index_validity_check(k) then
            rawset(tab,k,v)
        end
    end,
}

function paths.new_linear_path(tab)
    local t = {_meta={length=#tab,pos=1,dir=1,mode="linear",points={}}}
    for i,v in ipairs(tab) do t._meta.points[i] = rawget(tab,i) end
    setmetatable(t,linpath_metatable)
    rawset(rawget(t,'_meta'),'pos',t:get_closest() +1) -- Initialize your node to the closest node.
    -- +1 because get_closest() returns an offset relative to your starting position, and pos is initialized to 1.

    return t
end

return paths