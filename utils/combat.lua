-- utils.info.player 
-- utils.info._party 
-- utils.info.Buffs 
-- utils.info.Target 
-- utils.info.Lock 
-- utils.info.JAWSLock 
-- utils.info.MagicLock 
-- utils.info.ItemLock 
-- utils.info.Engaged 
-- utils.info.EyeSight 
-- utils.info.AbilRecasts 
-- utils.info.AvailableAbilities 
-- utils.info.AvailableSpells 
-- utils.info.SpellRecasts 
-- utils.info.PartyList 
-- utils.info.AllianceList 
-- utils.info.Attackers
-- utils.info.Disabled
-- utils.info.NumberOfAttackers
-- utils.info.NumberOfDisabled 
-- utils.info.NumberOfEnemies 
-- utils.info.ActionChain 
-- utils.info.WSChain 
-- utils.info.Running 
-- utils.info.Fighting 

function utils.combat_logic()
	
	if not utils.vars.combat_routine then return end
	
	if not utils.vars.combat_routine.routine or not type(utils.vars.combat_routine.routine) == "table" then return end
	
	for action in utils.vars.combat_routine.routine:it() do
		if action.condition and type(action.condition) == "function" then

			local success,result,err = pcall(action.condition)
			
            if not success then
                print(result)
            end

			if success and result then
				-- No error and the predicate passed, let's do the thing!
				if action.type and action.target and action.action then
					local target
				
					if type(action.target) == "function" then
						local t_success,t_result = pcall(action.target)
						
						if t_success then
							target = t_result
						end
					elseif type(action.target) == "string" then
						target = action.target
					end
                    
                    local action_result

					if target then
						if L{"spell","magic","/ma","ma"}:contains(action.type:lower()) then
							action_result = utils.combat_helpers.use_spell(action.action,action.delay,target)
						elseif L{"ability","job ability","action","ja","/ja"}:contains(action.type:lower()) then
							action_result = utils.combat_helpers.use_ability(action.action,action.delay,target)
						elseif L{"weaponskill","weapon skill","ws","/ws"}:contains(action.type:lower()) then
							action_result = utils.combat_helpers.use_ws(action.action,action.delay,target)
						end
					end
					
					if action_result then break end
				end
			end
		end
	end
	
end


utils.combat_helpers = {}


function utils.combat_helpers.use_ability(ja,delay,target)

	if utils.info.Lock or utils.info.JAWSLock or not S{0,1,48}:contains(utils.info.player.status_id) then return false end
	local v = res.job_abilities[utils.validabils['english']['/ja'][ja:lower()]]
	local targ,targ_mob = utils.validate_target_string(target)
	
	if (S(utils.info.AvailableAbilities.job_abilities)[v.id] and utils.info.AbilRecasts[v.recast_id] and utils.info.AbilRecasts[v.recast_id] <= utils.vars.settings.minimum_recast)
		and (targ and targ_mob.model_size and targ_mob.model_size ~= 0 and utils.vars.monster_array[targ_mob.index] and 
		(utils.vars._static.ranges[v.range] * v.range + targ_mob.model_size) > utils.dist(utils.vars.monster_array[targ_mob.index].s_coords, utils.get_s_coords())) then
		windower.send_command('input /ja "'..ja..'" '..targ_mob.id)
        utils.vars.combat_routine.last[ja] = {ts = os.clock(), mob = targ_mob.id}
        utils.exit_action(delay or utils.vars.settings.ja_delay, 1.2)
        return true
	end

	return false

end

function utils.combat_helpers.use_spell(ma,delay,target)
	
	if utils.info.Lock or utils.info.MagicLock or not utils.validabils['english']['/ma'][ma:lower()] or not S{0,1,48}:contains(utils.info.player.status_id) then return false end
    local v = res.spells[utils.validabils['english']['/ma'][ma:lower()]]
    local targ,targ_mob = utils.validate_target_string(target)
    local range = v.range
    if v.english:sub(1,5) == "Indi-" and utils.info.Buffs['entrust'] then
        range = 12
    end
    
    if utils.info.AvailableSpells[v.id] and (utils.info.SpellRecasts[v.id] and utils.info.SpellRecasts[v.id] <= utils.vars.settings.minimum_recast) and utils.mp_check(v.mp_cost,v.skill,v.type) and 
	(targ and targ_mob.model_size and targ_mob.model_size ~= 0 and utils.vars.monster_array[targ_mob.index] and
        (utils.vars._static.ranges[range] * range + targ_mob.model_size) > utils.dist(utils.vars.monster_array[targ_mob.index].s_coords, utils.get_s_coords() )) then
        windower.send_command('input /ma "'..ma..'" '..targ_mob.id)
		utils.vars.combat_routine.last[ma] = {ts = os.clock(), mob = targ_mob.id}
        utils.exit_action(delay or utils.vars.settings.ma_delay, v.cast_time * (1-math.min(utils.vars.settings.fast_cast,0.8)) + 0.5)
        return true
    end
    return false
end

function utils.combat_helpers.use_ws(ws,delay,target)
    if utils.info.Lock or utils.info.JAWSLock or not utils.validabils['english']['/ws'][ws:lower()] or utils.info.player.tp < 1000 or not S{0,1,48}:contains(utils.info.player.status_id) then return false end
	
	local v = res.weapon_skills[utils.validabils['english']['/ws'][ws:lower()]]
    local targ,targ_mob = utils.validate_target_string(target) -- Eye Sight?
    if v.english:lower() == ws:lower() and S(utils.info.AvailableAbilities.weapon_skills)[v.id] and (targ and targ_mob.model_size and targ_mob.model_size ~= 0 and utils.vars.monster_array[targ_mob.index] and
        (utils.vars._static.ranges[v.range] * v.range + targ_mob.model_size-1) > utils.dist(utils.vars.monster_array[targ_mob.index].s_coords, utils.get_s_coords() )) then
        windower.send_command('input /ws "'..ws..'" '..targ_mob.id)
        utils.exit_action(delay or utils.vars.settings.ws_delay, 2.2)
		utils.vars.combat_routine.last[ws] = {ts = os.clock(), mob = targ_mob.id}
        utils.vars.last_ws_time = os.clock()
        utils.vars.last_ws = v.id
        return true
    end
    return false

end


