-- This addon should always be running to a point with a given tolerance (+/- X).
-- Instead, right now it is running towards a point and stopping a tolerated range range away from it.

function utils.movement_logic(c_coords)
	

    -- Need to put this into another function, so this function basically just handles utils.run_to_point
    local point = false
    if utils.vars.running.target then
        point = utils.run_to_index(utils.vars.running.target.index,utils.vars.running.target.angle)
    end
    
    if not point and utils.vars.running and utils.vars.running.path  then
        -- Running to a path point
        if not utils.vars.running.path:get_n_coords() or not c_coords then
            print(utils.vars.running.path:pos(),utils.vars.running.path:get_n_coords(),c_coords)
        end

        if utils.dist(utils.vars.running.path:get_n_coords(),c_coords) < 1 then -- Condition to advance to the next point (currently arbitrary)		
			while utils.dist(utils.vars.running.path:get_n_coords(),c_coords) < 1 do
				point = utils.vars.running.path:move(1)
            end
            utils.vars.running.stuck = nil
            utils.vars._global.circ_buffer:clear()
        elseif not utils.vars.settings.disable_stuck and utils.tab_comp(utils.vars.running.last_vector, utils.vars._static.stop_vector) or math.abs(utils.vars.running.path:get_loc_min_dist(c_coords)) > 2 then
            -- if stopped or there is a node >2 away from you closer than your current node, advance to it
			point = utils.vars.running.path:move(utils.vars.running.path:get_loc_min_dist(c_coords))
        else
            point = utils.vars.running.path:get_n_coords() -- Otherwise, go to the nearest node.
        end
    end
    
    if point then
        utils.run_to_point(point)
    end
end

function utils.disable_stuck_detection()
	utils.vars.settings.stuck_detection = false
end

function utils.enable_stuck_detection()
	utils.vars.settings.stuck_detection = true
end

function utils.vector_run(vector,c_coords)
    local newvec = {}
    for i=1,3 do
        newvec[i] = vector[i] - ((c_coords or {})[i] or 0)
    end
    if utils.vars.running then
        utils.vars.running.last_vector = newvec
    end
    local den = math.abs(newvec[1])+math.abs(newvec[3])
    if den ~= 0 then
        windower.ffxi.run(newvec[1]/den,newvec[3]/den)
    else
        windower.ffxi.run(false)
    end
end

function utils.run_to_index(index,angle)
    local targ_coords = utils.vars.monster_array[index] and (utils.vars.monster_array[index].c_coords or utils.vars.monster_array[index].s_coords) or utils.extract_coords(windower.ffxi.get_mob_by_index(tonumber(index) or -1))
        
    if not targ_coords then
        error('run_to_index passed an invalid index '..tostring(index))
        return
    end
    
    local c_coords = utils.get_c_coords()
    local lower,upper = utils.calculate_approach_distance(windower.ffxi.get_mob_by_index(index),c_coords)
    local distance = utils.dist(targ_coords,c_coords)
    
    if not distance or not lower or not upper then return false end
    
    local targ_distance = distance < lower and lower or distance > upper and upper or distance
    local targ_angle = angle or math.atan2(targ_coords[3]-c_coords[3],targ_coords[1]-c_coords[1])
    
    local point = {targ_coords[1]-targ_distance*math.cos(targ_angle),targ_coords[2],targ_coords[3] - targ_distance*math.sin(targ_angle)}
    
    if utils.vars.running.path then
        local closest_node = utils.vars.running.path:get_closest(point)
        if (utils.map_dist(utils.vars.running.path[closest_node],c_coords) > utils.vars.settings.path_departure_distance or
        -- Further away from the monster than your departure distance, so just follow the path instead.
        closest_node > 0) and
        -- There is a path point in the direction you are heading closer to the monster than your current point, so just follow the path instead.
        utils.map_dist(point,c_coords) > utils.map_dist(point,utils.vars.running.path[closest_node]) then
        -- If you're further from the target now than if you diverted back to the path.
        
            -- Need to add handling here that adjust the path direction based on where the monster is relative to it.
            return false
        end
    end
    return point
end

function utils.run_to_point(targ_coords,lower,upper)
    lower = lower or 0
    upper = upper or 0.1
    local c_coords = utils.get_c_coords()
    -- This necessarily has to be X/Z distance because we're incapable of moving in the Y direction
    local dist = utils.map_dist(targ_coords,c_coords)
    if dist > upper or dist < lower then
        if utils.vars.settings.stuck_detection and utils.vars.running.stuck then
            if utils.vars.running.stuck%10 == 0 then
                utils.vars.running.stuck = nil
                utils.vars._global.circ_buffer:clear()
                utils.vector_run(targ_coords,c_coords)
            else
                if utils.vars.running.stuck == 1 then
                    utils.vector_run(utils.find_normal(utils.vars.running.last_vector))
                end
                utils.vars.running.stuck = utils.vars.running.stuck + 1
            end
        elseif dist > upper then
            -- Run towards the point
            utils.vector_run(targ_coords,c_coords)
        elseif dist < lower then
            -- Run away from the point
            utils.vector_run(c_coords,targ_coords)
        end
    elseif not utils.tab_comp(utils.vars.running.last_vector,utils.vars._static.stop_vector) then
        -- Stop running
        utils.vector_run(utils.vars._static.stop_vector)
    end
end

function utils.return_to_path()
    if utils.vars.running then
        utils.vars.running.target = nil
    end
end

function utils.calculate_approach_distance(target,coords) --coords is unused?
    if target and utils.vars.monster_array[target.index] and utils.dist(utils.vars.monster_array[target.index].s_coords,utils.extract_coords(target)) > 1 then
        -- Check whether the monster is still in motion headed to its current target. If so, follow it closely.
        -- 0.8 model size = ~3 yalms? Maybe 3.1
        
        return utils.vars.settings.approach_mobile_low,utils.vars.settings.approach_mobile_high + target.model_size
    elseif target then
        return utils.vars.settings.approach_stationary_low,utils.vars.settings.approach_stationary_high + target.model_size
    end
end

function utils.find_normal(vector)
    local ang = math.atan2(vector[3],vector[1]) + math.pi/2
    local dist = utils.map_dist(vector,{0,0,0})
    return {dist*math.cos(ang),vector[2],dist*math.sin(ang)}
end