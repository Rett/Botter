socket = require("socket")
local ipc = {}

local listen_ip = "*"
local port = 8080
local broadcast_ip = "127.255.255.255"
local udp = assert(socket.udp())

assert(udp:setoption("reuseaddr", true))
assert(udp:setoption("broadcast", true))
assert(udp:setsockname(listen_ip, port))
udp:settimeout(0)

function ipc.Read()
    return udp:receive();
end

function ipc.Send(data)
    assert(udp:sendto(data, broadcast_ip, port))
end


return ipc