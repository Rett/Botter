-- Circular Buffer Library

local cb = {}
local circ_buffer = {}

function cb:get_length()
    return rawget(rawget(self,'_meta'),'length') - 1
end

function cb.get_pos(t)
    -- Returns the current nil position
    local c = 1
    while rawget(t,c) do
        c = c + 1
    end
    return c
end

function cb:append(v)
    local pos = self:get_pos()
    local len = self:get_length()+1
    rawset(self,(pos-1)%len+1,v)
    rawset(self,pos%len+1,nil)
end

function cb:clear()
    for i=1,self:get_length()+1 do
        rawset(self,i,nil)
    end
end

function cb:full()
    for i=1,self:get_length() do
        if not self[i] then
            return false
        end
    end
    return true
end

function circ_buffer.new(num)
    if num < 3 then
        error('Circular buffer must be defined to > 3 values or there is no point.')
    end
    local t = {[1]=nil,_meta={length=num+1}}
    return setmetatable(t,{
    __index=function(t,k)
        if tonumber(k) then
            -- Treat k as an offset.
            local len = t:get_length() + 1
            local pos = t:get_pos() + k
            while pos <= 0 do
                pos = pos + len
            end
            return rawget(t,(pos-1)%len+1)
        else
            return rawget(cb,k)
        end
    end,
    __newindex=function(t,k,v)
        k = tonumber(k) or k
        if tonumber(k) and k > 0 then -- Does not compensate for negative numbers, I think.
            error('Sorry, the positive numeric range of circular buffers is reserved.')
        elseif tostring(k) ~= '_meta' then
            rawset(t,k,v)
        end
    end})
end



return circ_buffer