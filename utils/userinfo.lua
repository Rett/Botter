function utils.refresh_info()
	local check,pet = utils.refresh_player()

	if not check then return end
	
	utils.info.player = check
    utils.info.pet = pet
	utils.info._party = windower.ffxi.get_party()
	utils.info.Buffs = utils.calculate_buffs(utils.info.player.buffs)
	utils.info.Target = windower.ffxi.get_mob_by_target('t')
	utils.info.Lock = (utils.info.Buffs.costume or utils.info.Buffs.terror or utils.info.Buffs.stun or utils.info.Buffs.sleep or utils.info.Buffs.petrification or utils.info.Buffs.charm or utils.info.player.hpp == 0)
    utils.info.JAWSLock = utils.info.Buffs.amnesia == true
    utils.info.MagicLock = (utils.info.Buffs.silence == true) or (utils.info.Buffs.mute == true)
    utils.info.ItemLock = (utils.info.Buffs.muddle == true)
    utils.info.Engaged = utils.info.player.status == "Engaged" or utils.vars.fighting and utils.vars.fighting.target
    utils.info.EyeSight = utils.eye_sight(utils.extract_coords(utils.info.player or {}),utils.info.player.facing,utils.extract_coords(utils.info.player.target or {}),true)
    utils.info.AbilRecasts = windower.ffxi.get_ability_recasts()
    utils.info.AvailableAbilities = windower.ffxi.get_abilities()
    utils.info.AvailableSpells = windower.ffxi.get_spells()
    utils.info.SpellRecasts = windower.ffxi.get_spell_recasts()
    utils.info.PartyList = utils.make_party_list(utils.info._party, 1)
    utils.info.AllianceList = utils.make_alliance_list(utils.info._party)
    utils.info.Attackers, utils.info.Disabled,  utils.info.NumberOfAttackers,  utils.info.NumberOfDisabled = utils.count_attackers()
    utils.info.NumberOfEnemies = utils.info.NumberOfAttackers + utils.info.NumberOfDisabled
    utils.info.ActionChain = ((os.clock() - utils.vars.lasttime - utils.vars.lastactiondelay) < 0.2) -- True if you can "chain" actions together now and potentially not wait for the extra second of JA delay, etc.
    utils.info.WSChain = ((os.clock() - utils.vars.last_ws_time) < 10)
    utils.info.Running = utils.vars.running and true
    utils.info.Fighting = utils.vars.fighting and true
end

function utils.refresh_player()
	local player_result = utils.make_user_table()
    local pet = utils.make_user_table()
	
	local pl,player_mob_table 
	pl = windower.ffxi.get_player()

	if not pl or not pl.vitals then return end
	
	player_mob_table = windower.ffxi.get_mob_by_index(pl.index)
	if not player_mob_table then return end
	
	table.reassign(player_result,pl)
	for i,v in pairs(player_result.vitals) do
		player_result[i]=v
	end
    
    -- If we have a pet, create or update the table info.
    if player_mob_table and player_mob_table.pet_index then
        local player_pet_table = windower.ffxi.get_mob_by_index(player_mob_table.pet_index)
        if player_pet_table then
            table.reassign(pet, utils.target_complete(player_pet_table))
            pet.claim_id = nil
            pet.is_npc = nil
            pet.isvalid = true
            if pet.tp then pet.tp = pet.tp/10 end
        else
            table.reassign(pet, {isvalid=false})
        end
    else
        table.reassign(pet, {isvalid=false})
    end
	
	player_result.status_id = player_result.status
	if res.statuses[player_result.status] then
		player_result.status = res.statuses[player_result.status].english
	else
		print(player.status_id)
	end
	
	for i,v in pairs(player_mob_table) do
		if i == 'name' then
			player_result.mob_name = v
		elseif i~= 'is_npc' and i~='tp' and i~='mpp' and i~='claim_id' and i~='status' then
			player_result[i] = v
		end
	end
	
	return player_result,pet

end

function utils.calculate_buffs(cur_buffs)
    local buffs = utils.make_user_table()

    for i,v in pairs(cur_buffs) do
		if res.buffs[v] and res.buffs[v].english then
            if not buffs[v] then
                buffs[v] = 1
                buffs[res.buffs[v].english:lower()] = 1
            else
                buffs[v] = buffs[v] + 1
                buffs[res.buffs[v].english:lower()] = buffs[res.buffs[v].english:lower()] + 1
            end                
        end
    end
    return buffs
end

utils.run = {}

-- Positioning
function utils.run.suspend(delay)
    if utils.vars.running and not utils.vars.running.suspended and not utils.vars.running.mode == 'none' then
        utils.vector_run(utils.vars._static.stop_vector)
        utils.vars.running.suspended = (delay and tonumber(delay) and delay + os.clock()) or true
    end
end

function utils.run.resume()
    if utils.vars.running and utils.vars.running.suspended then
        utils.vars.running.suspended = false
        if not utils.vars.running.mode == 'none' then
            local c_coords = utils.vars.get_c_coords()
            utils.vars.running.path:move(utils.vars.running.path:get_closest(c_coords))
            utils.vars.run_to_point(utils.vars.running.path:get_c_coords())
        end
    end
end

utils.fight = {}

-- Fighting
function utils.fight.suspend(delay)
    if utils.vars.fighting and not utils.vars.fighting.suspended then
        if utils.vars.settings.disengage_on_suspend and utils.vars.fighting.target then
            utils.disengage(utils.vars.fighting.target.index)
        end
        utils.vars.fighting.suspended = (delay and tonumber(delay) and delay + os.clock()) or true
    end
end

function utils.fight.resume()
    if utils.vars.fighting and utils.vars.fighting.suspended then
        utils.vars.fighting.suspended = false
    end
end