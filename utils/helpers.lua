-- Distance in the X and Z axes
function utils.map_dist(coordA,coordB)
    coordB = coordB or utils.get_c_coords()
    if not coordA then error('Passing in nil values to map_dist',2) return end
    return math.sqrt((coordA[1]-coordB[1])^2 + (coordA[3]-coordB[3])^2)
end

-- Distance in the X, Y, and Z axes
function utils.dist(coordA,coordB)
    coordB = coordB or utils.get_c_coords()
    if not coordA then error('Passing in nil values to dist',2) return end
    return math.sqrt((coordA[1]-coordB[1])^2 + (coordA[3]-coordB[3])^2 + (coordA[2]-coordB[2])^2)
end

function utils.z_check(coordA,coordB,max_dif)
    coordB = coordB or utils.get_c_coords()
    max_dif = max_dif or utils.vars.settings.max_z_difference
    if not coordA then error('Passing in nil values to z_check',2) return end
    return math.abs(coordA[2]-coordB[2]) <= max_dif
end

function utils.extract_coords(mob)
    return {[1]=mob.x,[2]=mob.z,[3]=mob.y}
end

function utils.eye_sight(actor_coords,actor_facing,target_c_coords,narrow)
    if not target_c_coords or not target_c_coords[1] then return false end
    
    local xdif = target_c_coords[1] - actor_coords[1] -- Negative if target is west
    local ydif = target_c_coords[3] - actor_coords[3] -- Negative if target is south
    local lim = 0.76
    
    if narrow then lim = 0.05 end
    
    if math.abs(-math.atan2(ydif,xdif)-actor_facing) < lim then
        return true
    else
        return false
    end
end

function utils.valid_monster(index,bool)
    if not index then return false end
    local mob_arr = windower.ffxi.get_mob_by_index(index)
	
    if not mob_arr or
        mob_arr.hpp == 0 or
        not mob_arr.valid_target or
        (bool and (not utils.vars.settings.target_list:find(mob_arr.name:lower()) and not utils.vars.settings.target_list:find(mob_arr.index)) ) or
        --mob_arr.race ~= 0 or -- Exclude Humanoids, NPCs
        mob_arr.in_party or
        mob_arr.in_alliance or
        not mob_arr.is_npc or
        (mob_arr.claim_id ~= 0 and (not utils.info.AllianceList or not utils.info.AllianceList.id[mob_arr.claim_id]) and not utils.info.Buffs['reive mark'] and not utils.info.Buffs['besieged'] and not utils.info.Buffs['allied tags'] ) or
        mob_arr.charmed and utils.vars.monster_array and utils.vars.monster_array[index] or
        mob_arr.model_size == 0 then -- Not sure about this one
            return false
    end
    return true
end

function utils.validate_target_string(str)
    local targ,targ_mob = utils.valid_target(str)
    if not targ and utils.info.player.target then
        targ = utils.info.player.target.type ~= 'None' and utils.info.player.target.name or utils.info.player.name
        targ_mob = utils.info.player.target.type ~= 'None' and utils.info.player.target or utils.info.player
    end
    return targ,targ_mob
end

function utils.valid_target(targ)

    local spelltarget = {}
	
    if utils.vars._static.pass_through_targs[targ] then
		local j = windower.ffxi.get_mob_by_target(targ)
        
        if j then spelltarget = utils.target_complete(j) end
        
        spelltarget.raw = targ
        return targ, spelltarget
    elseif targ and tonumber(targ) and tonumber(targ) > 255 then
        local j = windower.ffxi.get_mob_by_id(tonumber(targ))
        
        if j then spelltarget = utils.target_complete(j) end
        
        spelltarget.raw = targ
        return targ, spelltarget
    elseif targ and not tonumber(targ) and targ ~= '' then
        local mob_array = windower.ffxi.get_mob_array()
        for i,v in pairs(mob_array) do
            if v.name:lower()==targ:lower() and (not v.is_npc or v.spawn_type == 14) then
                spelltarget = utils.target_complete(v)
                spelltarget.raw = targ
                return targ, spelltarget
            end
        end
    end
    return false, false
end

function utils.target_complete(mob_table)
    if mob_table == nil then return {type = 'NONE'} end
    
    mob_table.isallymember = false
    if not mob_table.id then
        mob_table.type = 'None'
    else
	
		if utils.info.AllianceList and utils.info.AllianceList.id[mob_table.id] then mob_table.isallymember = true end
		if utils.info.PartyList and utils.info.PartyList.id[mob_table.id] then mob_table.ispartymember = true end
        
        if utils.info.player and 
            utils.info.player.id == mob_table.id then
            mob_table.type = 'Self'
        elseif mob_table.is_npc then
            if mob_table.id%4096>2047 then
                mob_table.type = 'NPC'
            else
                mob_table.type = 'Monster'
            end
        else
            mob_table.type = 'Player'
        end
    end
    
    if mob_table.race then 
        mob_table.race_id = mob_table.race
        if res.races[mob_table.race] then
            mob_table.race = res.races[mob_table.race][language]
        else
            mob_table.race = 'Unknown'
        end
    end
    if mob_table.status then
        mob_table.status_id = mob_table.status
        if res.statuses[mob_table.status] then
            mob_table.status = res.statuses[mob_table.status].english
        else
            mob_table.status = 'Unknown'
        end
    end
    if mob_table.distance then
        mob_table.distance = math.sqrt(mob_table.distance)
    end
    return mob_table
end

function utils.get_c_coords()
    local play = windower.ffxi.get_mob_by_index(utils.info.player and utils.info.player.index or -1)
    if play then
        return {play.x,play.z,play.y}
    else
        return {0,0,0}
    end 
end

function utils.get_s_coords()
    -- Where the server thinks you are.
    return utils.vars._global.circ_buffer[-1] or utils.get_c_coords()
end

function utils.tab_comp(a,b)
    if type(a) ~= 'table' or type(b) ~= 'table' then
        return
    end
    local b_cpy = {}
    for i,v in pairs(b) do
        b_cpy[i] = v
    end
    for i,v in pairs(a) do
        if b_cpy[i] ~= v then
            return false
        else
            b_cpy[i] = nil
        end
    end
    for i,v in pairs(b_cpy) do
        return false
    end
    return true
end

function utils.exit_action(del,run_del)
    utils.vars.delay = del
    utils.vars.lastactiondelay = del
    if run_del and run_del ~= 0 and utils.vars.settings.auto_suspend then
        utils.suspend(run_del)
    end
    if utils.vars.running then
        utils.vars.running.stuck = nil
    end
end

function utils.suspend(delay)

    if utils.vars.running and not utils.vars.running.suspended and not (utils.vars.running.mode == 'none') then
        utils.vector_run(utils.vars._static.stop_vector)
        utils.vars.running.suspended = (delay and tonumber(delay) and delay + os.clock()) or true
    end
end

function utils.resume()
    if utils.vars.running and utils.vars.running.suspended then
        utils.vars.running.suspended = false
    end
end

function utils.monster_claimed(index)
    local mob = windower.ffxi.get_mob_by_index(index)
    if mob and mob.claim_id == windower.ffxi.get_player().id then
        return true
    else
        return false
    end
end

function utils.count_attackers()
    local attacker_list,disabled_list = {},{}
    for i,v in pairs(utils.vars.monster_array) do
        if v.attacking then
            local attacker = windower.ffxi.get_mob_by_index(i)
            if attacker and attacker.hpp == 0 then
                utils.vars.monster_array[i].attacking = false
            elseif attacker and os.clock() - v.last_attack > utils.vars.settings.enemy_attack_interval then
                utils.vars.monster_array[i].attacking = 'disabled'
                disabled_list[#disabled_list+1] = attacker
                disabled_list[#disabled_list].last_attack = utils.vars.monster_array[i].last_attack
                disabled_list[#disabled_list].last_target = utils.vars.monster_array[i].last_target
                disabled_list[#disabled_list].s_coords = utils.vars.monster_array[i].s_coords -- These would be the real coordinates from the last incoming packet
            elseif attacker then
                -- utils.vars.monster_array[i].attacking = 'attacking'
                attacker_list[#attacker_list+1] = attacker
                attacker_list[#attacker_list].last_attack = utils.vars.monster_array[i].last_attack
                attacker_list[#attacker_list].last_target = utils.vars.monster_array[i].last_target
                attacker_list[#attacker_list].s_coords = utils.vars.monster_array[i].s_coords -- These would be the real coordinates from the last incoming packet
            else
                print('PB: Attacker not found!')
            end
        end
    end
    return attacker_list,disabled_list,#attacker_list,#disabled_list
end

function utils.mp_check(mp,skill,typ)
    if ( ( (utils.info.Buffs.manifestation and skill == 'Elemental Magic') or (utils.info.Buffs.accession and skill == 'Enhancing Magic') ) and player.mp >= math.floor(mp*1.5)) or
       ( ( (utils.info.Buffs.penury and typ == 'WhiteMagic' ) or (utils.info.Buffs.parismony and typ == 'BlackMagic') ) and player.mp >= math.floor(mp*0.5)) or
       ( ( (utils.info.Buffs['light arts'] or utils.info.Buffs['addendum: white'] ) and typ == 'WhiteMagic' ) or ( (utils.info.Buffs['dark arts'] or utils.info.Buffs['addendum: black'] ) and typ == 'Blackmagic') and player.mp > math.floor(mp*0.9)) or
        utils.info.player.mp > mp then
        return true
    end
    return false
end

function utils.make_party_list(party,num)
    local party_list = { name = utils.make_user_table(), id = {}, index = {}}
    
	party_list.count = 0
	local num = num or 1
	local check = L{[1] = L{"p0","p1","p2","p3","p4","p5"},
			[2] = L{"a10","a11","a12","a13","a14","a15"},
			[3] = L{"a20","a21","a22","a23","a24","a25"},}
    for i,v in pairs(party) do
		if check[num]:contains(i) then
            party_list.count = party_list.count + 1
			if type(v) == 'table' and v.name and v.mob and v.mob.id and v.mob.index then
				local tab = {}
				for j,k in pairs(v.mob) do
					tab[j] = k
				end
				tab.mpp = v.mpp
				tab.mp = v.mp
				tab.zone = v.zone
				tab.tp = v.tp
				tab.hpp = v.hpp
				tab.hp = v.hp
				party_list.name[v.name], party_list.id[v.mob.id], party_list.index[v.mob.index] = tab, tab, tab
				if tab.pet_index then
					local temppet = {}
					for j,k in pairs(windower.ffxi.get_mob_by_index(tab.pet_index) or {}) do
						temppet[j] = k
					end
					if temppet.name and temppet.id and temppet.index then
						party_list[temppet.name], party_list[temppet.id], party_list[temppet.index] = temppet, temppet, temppet
					end
				end
			end
		end
    end
    return party_list
end

function utils.make_alliance_list(alliance)
    local alliance_list = utils.make_party_list(alliance,1)
    local party2_list = utils.make_party_list(alliance,2)
    local party3_list = utils.make_party_list(alliance,3)
    alliance_list.count = alliance_list.count + party2_list.count+party3_list.count
    party2_list.count = nil
    party3_list.count = nil
    for k,v in pairs(party2_list) do
        for j,u in pairs(v) do
			if not alliance_list[k] then alliance_list[k] = {} end
            alliance_list[k][j] = u
        end
    end
    for k,v in pairs(party3_list) do
        for j,u in pairs(v) do
			if not alliance_list[k] then alliance_list[k] = {} end
            alliance_list[k][j] = u
        end
    end
    return alliance_list
end

function utils.aoe_targets(index,dist,filt,...)
    local args = {...}
    if tonumber(index) and tonumber(dist) and (not filt or type(filt) == 'function') and windower.ffxi.get_mob_by_index(index) then
        local mob = windower.ffxi.get_mob_by_index(index)
        local mob_coords = utils.vars.monster_array[index] and utils.vars.monster_array[index].s_coords or {mob.x,mob.z,mob.y}
        local count = 0
        for i,v in pairs(utils.vars.monster_array) do
            if utils.dist(mob_coords,v.s_coords or utils.extract_coords(windower.ffxi.get_mob_by_index(i))) < dist and (filt or utils.valid_monster)(i,unpack(args)) then
                count = count + 1
            end
        end
        return count
    end
end

function utils.calculate_time_offset(offset)

    if not utils.info.alliancelist then return 0 end

    offset = tonumber(offset) or .5
    local num = 0

    for k,v in pairs(utils.info.alliancelist.index) do
        if k == utils.info.player.index then
            return num * offset
        end
       num = num + 1
    end

end


utils.user_data_table = {
    __newindex = function(tab, key, val)
            rawset(tab, utils.user_key_filter(key), val)
        end,

    __index = function(tab, key)
        return rawget(tab, utils.user_key_filter(key))
    end
    }

-----------------------------------------------------------------------------------
--Name: user_key_filter()
--Args:
---- val (key): potential key to be modified
-----------------------------------------------------------------------------------
--Returns:
---- Filtered key
-----------------------------------------------------------------------------------
function utils.user_key_filter(val)
    if type(val) == 'string' then
        val = string.lower(val)
    end
    return val
end


-----------------------------------------------------------------------------------
--Name: make_user_table()
--Args:
---- None
-----------------------------------------------------------------------------------
--Returns:
---- Table with case-insensitive keys
-----------------------------------------------------------------------------------
function utils.make_user_table()
    return setmetatable({}, utils.user_data_table)
end