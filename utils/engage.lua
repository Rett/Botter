function utils.engaging_logic()
    -- ENGAGING LOGIG
	
    if not utils.vars.fighting.target then
        -- Attack something if possible.
        utils.attack_closest()
    elseif not utils.valid_monster(utils.vars.fighting.target.index) then
        -- Switch targets if your current target is no longer valid (claimed or dead).
        utils.vars.fighting.target = nil
        utils.attack_closest()
    elseif utils.vars.fighting.target and utils.vars.fighting.engage
        and not utils.vars.fighting.target.engaged and utils.dist(utils.vars.monster_array[utils.vars.fighting.target.index].s_coords,utils.get_s_coords() ) < 29 then
        -- You have a target to engage, but you haven't actually sent an engage packet yet.
        utils.attack_monster(utils.vars.fighting.target.index)
    elseif utils.vars.fighting.target.engaged and utils.vars.fighting.engage
        and utils.vars.fighting.target.ts + 1 < os.clock() and windower.ffxi.get_player().status ~= 1 then
        -- Resend the attack packet if you haven't engaged within 1 second.
        utils.attack_monster(utils.vars.fighting.target.index)
    elseif utils.vars.fighting.target.engaged and utils.vars.fighting.engage and windower.ffxi.get_player().status == 1 then
        -- You should be fighting.
        local targ = windower.ffxi.get_mob_by_target('t')
        if ((not targ or targ.index ~= utils.vars.fighting.target.index) and utils.vars.fighting.target.ts +1 < os.clock())
            or not utils.z_check(utils.get_s_coords(),utils.vars.monster_array[utils.vars.fighting.target.index].s_coords) then
            -- If it has been 1 second and you are not engaged to the right monster, disengage.
            utils.disengage(utils.vars.fighting.target.index)
        end
    end
    
    if utils.vars.fighting.target and utils.info.player and S{0,1,48}:contains(utils.info.player.status_id) then
        -- Face the monster you're fighting.
        local mob_coords = utils.vars.monster_array[utils.vars.fighting.target.index] and utils.vars.monster_array[utils.vars.fighting.target.index].s_coords or utils.extract_coords(windower.ffxi.get_mob_by_index(utils.vars.fighting.target.index))
        utils.turn_relative(mob_coords,utils.get_c_coords(),utils.vars.settings.facing_offset)
    end
end

function utils.find_nearest_enemy()
    local dist_arr = {}
    local c_coords = utils.get_c_coords()
    local engage_dist = utils.vars.settings.engage_distance
    local departure_dist = utils.vars.settings.path_departure_distance
    local min_dist = engage_dist
    local ind = false
    local typ = false
    for index,mob in pairs(utils.vars.monster_array) do
        local dist = utils.dist(mob.s_coords,c_coords)
        local z_check = utils.z_check(mob.s_coords, c_coords)
        if dist < engage_dist and z_check and utils.valid_monster(index,true) and
        (not utils.vars.running or utils.vars.running.mode == 'none' or utils.vars.running.mode == 'follow' or utils.vars.running.path and utils.dist(mob.s_coords,utils.vars.running.path[utils.vars.running.path:get_closest(mob.s_coords)]) < departure_dist) then
            dist_arr[index] = dist
        end
    end
    for index,dist in pairs(dist_arr) do
        -- Priority should be:
            -- Monsters currently attacking you
            -- Monsters currently disabled by you
            -- Monsters that you haven't started fighting yet
        if utils.vars.monster_array[index].attacking == 'attacking' and (not typ or typ ~= 'attacking' or (typ == 'attacking' and dist < min_dist)) then
            min_dist = dist
            ind = index
            typ = 'attacking'
        elseif utils.vars.monster_array[index] == 'disabled' and (not typ or (typ == 'disabled' and dist < min_dist)) then
            min_dist = dist
            ind = index
        elseif dist < min_dist and not typ then -- utils.valid_monster(index,true) and 
            min_dist = dist
            ind = index
        end
    end
	
    return ind,min_dist
end

function utils.attack_closest()
    local targ = utils.find_nearest_enemy()
    if targ and utils.vars.monster_array[targ] and utils.dist(utils.vars.monster_array[targ].s_coords, utils.get_s_coords()) < 29 then
        utils.attack_monster(targ)
    elseif targ and utils.vars.monster_array[targ] then
        utils.vars.fighting.target = {index=targ}
    end
end

function utils.attack_monster(index)
    local target = windower.ffxi.get_mob_by_index(index)
    local current_targ = windower.ffxi.get_mob_by_target('t')
    utils.vars.fighting.target = {index=index,ts=os.clock(),engaged=true}
    utils.vars._global.circ_buffer:clear()

    if utils.vars.fighting.engage then
        if windower.ffxi.get_player().status == 1 or (current_targ and current_targ.hpp == 0) then
            windower.packets.inject_outgoing(0x1A,string.char(0x1A,0x08,0,0)..'I':pack(target.id)..'H':pack(index)..string.char(0x0F,0,0,0,0,0)..string.char(0):rep(12))
        else
            windower.packets.inject_outgoing(0x1A,string.char(0x1A,0x08,0,0)..'I':pack(target.id)..'H':pack(index)..string.char(0x02,0,0,0,0,0)..string.char(0):rep(12))
        end
    end
end

function utils.disengage(index)
    local target = windower.ffxi.get_mob_by_index(index)
    utils.vars.fighting.target = nil
    windower.packets.inject_outgoing(0x1A,string.char(0x1A,0x08,0,0)..'I':pack(target.id)..'H':pack(index)..string.char(0x04,0,0,0,0,0)..string.char(0):rep(12))
end

function utils.turn_relative(c_coords1,c_coords2,offset)
    -- c_coords1/2 is terrible naming. Which is the target and who is turning?
    local angle = ((offset or 0)-math.atan2(c_coords1[3]-c_coords2[3],c_coords1[1]-c_coords2[1]))
    if angle < 0 then
        angle = 2*math.pi + angle
    end
    angle = math.floor(128*angle/math.pi)
    angle = angle == 256 and 0 or angle
    utils.vars._global.facing_angle = angle
    windower.ffxi.turn((offset or 0)-math.atan2(c_coords1[3]-c_coords2[3],c_coords1[1]-c_coords2[1]))
end