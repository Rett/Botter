_addon = {}
_addon.name = 'Coordinator'
_addon.version = '2.0.04.11.17'
_addon.commands = {'coordinator'}

config = require('config')
file = require('files')
require('coroutine')
require('chat')
require('lists')
require('queues')
require('logger')
require('tables')
require('sets')
require('strings')
require('config')
utils = require('utils')


defaults = {
    leader = "",
    salvage = {},
    merits = {
		follow = {},
		combat = S{},
		leeches = S{},
	},
	reisenjima = {
		follow = {},
		combat = S{},
	},
	htbf = {
		leeches = S{},
	},
	jobs1 = {},
	jobs2 = {},
	ambuscade = {
		leeches = S{},
	}
	
}

if windower.ffxi.get_player() then
    defaults.salvage[windower.ffxi.get_player().name] = {}
end

settings = config.load(defaults)

tasks = Q{}
registry = S{}
characters_finished = L{}
control = {
	loaded = L{},
	current = 0,
	loading = false,
	running = true,
	complete = false,
	current_task = nil,
	synchronized = false,
	retries = 0
}


formats = {

	procedure = "!procedure,%s",
	sync = "!sync",
	finished_sync = "!finished_sync",
	complete = "!complete",
	error = "!error",

}

function try(f)
    local result,message = f()
    if message then
        error(message)
    end
	return result
end

function handle_coordinate(message)

		local args = message:split(',')
		
		if args[1] == "synchronized" then
			control.synchronized = false
		elseif args[1] == "load" then
			if not args[2] then
				error("Coordinator was instructed to load a procedure, but no procedure was specified.")
			else
				load_procedure(args[2])
			end
		elseif args[1] == "unload" then			
			control.running = false
			control.current_task = nil
			tasks:clear()
			
			local procedure = control.loaded[control.current]
			
			if procedure then
				utils.finalize()
				procedure.exit()
			end
			
            notice("Terminating the running procedure...")
		elseif args[1] == "rcvd" then
			acknowledge(args[2],args[3])
		end
		

end

function acknowledge(command, ts)
	local result = registry:remove("%s_%s":format(command,ts))

	if command == "complete" and result then
		local procedure = control.loaded[control.current]
		if procedure then procedure.exit() end
	end
	
end

function wait_until_acknowledged(command, msg, timeout)

	local start = os.clock()
	local ts = os.time()
	local entry = "%s_%d":format(command,ts)
	local waiting = 0
	
	registry:add(entry)
	
	while registry:contains(entry) and (not tonumber(timeout) or ((os.clock() - start) < timeout )) do
		if waiting % 30 == 0 then
			windower.coordinate("%s,%d":format(msg,ts))
		end
		waiting = waiting + 1
		coroutine.sleep(.1)
	end
	
	return not tonumber(timeout) or ((os.clock() - start) < timeout )

end

function load_procedure(proc)

	local path = 'data/' .. proc .. '.lua'
	local temp
	
	if file.exists(path) then
		temp,err = dofile(windower.addon_path .. path)
	end
	
	if not temp then
		error("The specified procedure could not be loaded.")
		print(err)
		wait_until_acknowledged("procedure", formats.procedure:format("false"), 15)
	else
		control.loaded:append(temp)
		--clean_up(control.loaded[control.current])
		control.current = control.current + 1
        log("Succesfully loaded the procedure (%s)!":format(procedure.name or "No Name"))
		wait_until_acknowledged("procedure", formats.procedure:format("true"), 15)
		control.running = true
		grab_task(true)
	end
	
end

function grab_task(first_try)
	local procedure = control.loaded[control.current]
	
	if not procedure then
		error("No procedure has been imported.")
	else
		control.loading = true
		control.synchronized = false
		control.retries = 0
		
		local task = procedure.get_task(first_try)
		
		if not task then
			control.complete = true
		else		
            tasks:push(task)
		end

		control.loading = false
			
	end
end

function handle_task(task)
	local procedure = control.loaded[control.current]

	if not procedure then
		error("No procedure has been imported.")
	else
		local f = task.f
		
		if not f then
			error("The specified task could not be found in the loaded procedure.")
		else
			control.synchronized = task.synchronized
			
			if control.synchronized then
				wait_until_acknowledged("sync", formats.sync, 15)
			end
			
			control.current_task = task
			procedure.initialize()
			return try(f)
			
		end
	end

end

function task_result(result)
	result = result or ((result == nil) and true) or false
	
	local procedure = control.loaded[control.current]

	if not procedure and control.running then
		error("A task provided a result but no procedure was loaded.")
	elseif procedure then
		if procedure.is_error() then
			wait_until_acknowledged("error", formats.error, 15)
		else
			if result then
				
				procedure.finalize()
				
				if control.synchronized then 
					wait_until_acknowledged("finished_sync", formats.finished_sync, 15)
					
					while control.running and control.synchronized do
						coroutine.sleep(.5)
					end			
				end
				
				utils.finalize()
				
				procedure.advance_step()
			end
			
			grab_task(result)
		end
	end
end

function follow_keys(tab,keys)

    if not tab or not keys or keys:length() < 2 then error("You must provide at least two values when modifying a setting (key and val).") return end


    local nest = settings
    local path = "settings"

    if #keys > 2 then 
        for i = 1,#keys - 2,1 do
            local check = rawget(nest, keys[i]:lower())
            path = "%s.%s":format(path, keys[i]:lower())

            if not check or type(check) ~= "table" then
                error("%s is not a valid table. Please create it before assigning to it.":format(path))
                return false
            end

            nest = check
            
        end
    end

    return nest,path,keys[-2]:lower(),keys[-1]

end

function set_option(...)
    local args = (...) and {...}

    if not args then return end



    local tab,path,key,val = follow_keys(settings,L(args))


    if tab and key and val then
        if val == "{}" then 
            val = {}
        elseif val == "S{}" then 
            val = S{}
        end
		
		if type(tab[key]) == "table" then
			error("[Settings] %s.%s is a table and cannot be assigned to directly.":format(path,key))
			return false
		end
		
        rawset(tab,key,val)
        notice("[Settings] %s.%s has been set to %s":format(path,key,type(val) == "string" and val or class(val)))
        settings:save('all')
    end
end

function add_option(...)
    local args = (...) and {...}

    if not args then return end



    local tab,path,key,val = follow_keys(settings,L(args))


    if tab and key and val then
        if not tab[key] then error("[Settings] %s does not exist and cannot be added to.":format(path)) return end
        if class(tab[key]) ~= "Set" then error("[Settings] %s is not a set and cannot be added to.":format(path)) return end

        tab[key]:add(val)

        notice("[Settings] %s was added to %s.%s":format(val,path,key))
        settings:save('all')
    end

end

function delete_option(...)
    local args = (...) and {...}

    if not args then return end



    local tab,path,key,val = follow_keys(settings,L(args))


    if tab and key and val then
        if not tab[key] then error("[Settings] %s does not exist and cannot be added to.":format(path)) return end
        if class(tab[key]) ~= "Set" then error("[Settings] %s is not a set and cannot be removed from.":format(path)) return end
		
		if tab[key]:contains(val) then
			tab[key]:remove(val)
			notice("[Settings] %s was removed from %s.%s":format(val,path,key))
			settings:save('all')
		else
			error("[Settings] %s was not found in %s.%s":format(val,path,key))
		end
    end

end

-- function clean_up(procedure)
	-- if not procedure then return end
	

	-- if procedure.registered_events then
        -- print("Unregistering : " .. procedure.registered_events:concat(','))
        -- for event in procedure.registered_events:it() do
            -- windower.unregister_event(event)
        -- end
    -- end
	
	-- procedure.registered_events:clear()
    
    -- windower.send_command('lua m coordinator')

-- end

function main()


	while true do
		if control.running then
			if control.complete then
                utils.finalize()
				control.complete = false
				control.running = false
                wait_until_acknowledged("complete", formats.complete, 15)
                log("The procedure was completed successfully!")
			elseif not control.loading and not tasks:empty() then
				local task = tasks:pop()
				local result = handle_task(task)
                if control.running then
                    task_result(result)
                end
			end 
			
			coroutine.sleep(.5)
		else
			coroutine.sleep(.5)
		end
	
	end

end

function eval(...)
	assert(loadstring(table.concat({...}, ' ')))()
end


handlers = {
	eval = eval,
    set = set_option,
    add = add_option,
    delete = delete_option,
}

local function handle_command(...)
    local cmd  = (...) and (...):lower()
    local args = {select(2, ...)}
    if handlers[cmd] then
        local msg = handlers[cmd](unpack(args))
        if msg then
            log('Result: ' .. msg)
        end
    else
        error("unknown command %s":format(cmd or ""))
    end
end

windower.register_event('addon command', handle_command)

windower.register_event('coordinate', handle_coordinate)
coroutine.schedule(main, 0)