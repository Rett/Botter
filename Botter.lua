_addon = {}
_addon.name = 'Botter'
_addon.version = '1.0'
_addon.command = 'bot'

config = require('config')
file = require('files')
require('coroutine')
require('chat')
require('lists')
require('queues')
require('logger')
require('tables')
require('sets')
require('strings')
utils = require('utils')
ipc = require('utils/ipc')

-- Display setup
DISPLAY = require('Display')
_procedures_display = DISPLAY:new("Procedures")
_procedures_display:visibility(true)

_run_display = DISPLAY:new("Run")
_run_display:visibility(false)
_run_display:update_options(T{"Start", "Reset"})

_record_display = DISPLAY:new("Path Recording")
_record_display:set_position(0, _run_display.settings.display.pos.y + 200)
_record_display:update_options(T{"Start", "Stop"})
_record_display:visibility(true)

tasks = Q(T{})
procedure = nil
registry = T{}
characters_finished = L{}
control = {
    loading = false,
    running = false,
    complete = false,
    procedure_name = nil,
    current_task = nil,
    retries = 0,
    recording = false,
    frame_number = 0,
    path_file_handle = nil,
    path = L{},
    synchronized = true
}

procedure_result_format = "!procedure,%s"
task_result_format = "!task,%s"
wait_for = "!waitfor"
complete = "!complete"
internal_error = "!error"
heartbeat_delim = "@"

function Listen()
    local received = ipc.Read()
    if received then 
        local fields = L{}
        for value in string.gmatch(received, "[^"..heartbeat_delim.."]+") do 
            fields:append(value)
        end

        if fields[3] ~= utils.info.player.name then
            registry[fields[3]] = {procedure = fields[1], task_id = fields[2]}
        end
    end

    coroutine.schedule(Listen, 0.5)
end

function Heartbeat()
    if procedure and control.running and procedure.current_task then
        local packet = control.procedure_name..heartbeat_delim..
            procedure.current_task..heartbeat_delim..
            utils.info.player.name
        
        ipc.Send(packet)
    end
    
    coroutine.schedule(Heartbeat, 1)
end

function Terminate()
    control.running = false
    control.current_task = nil
    tasks:clear()
    
    if procedure then
        utils.finalize()
        procedure.exit()
    end
    
    notice("Terminating the running procedure...")
end

function Sync()
    control.synchronized = true

    for k, v in pairs(registry) do
        if control.procedure_name == v.procedure and
            tonumber(v.task_id) < procedure.current_task 
        then
            control.synchronized = false
            log("Not synced with: "..k)
        end
    end

    return control.synchronized
end

function try(f)
    local result,message = f()
    if message then
        error(message)
    end
    return result
end

function handle_command(...)
    local args = {...}
    
    if args[1] == "task" then
            if not control.loading then
                next_task()
            end
    elseif args[1] == "retry" then
            retry_task()
    elseif args[1] == "load" then
        if not args[2] then
            error("Coordinator was instructed to load a procedure, but no procedure was specified.")
        else
            load_procedure(args[2])
        end
    elseif args[1] == "unload" then
        Terminate()
    end
end

function load_procedure(proc)
    if control.running then
        error("Procedure already running. Stop current procedure first.")
        return
    end

    local path = 'data/' .. proc .. '.lua'
    if file.exists(path) then
        log("Loading procedure "..proc.."...")
        procedure = dofile(windower.addon_path .. path)
    end
    
    if not procedure then
        error("The specified procedure could not be loaded.")
    else
        control.running = true
        log(("Succesfully loaded the procedure (%s)!"):format(procedure.name or "No Name"))
        grab_task(true)
    end
end

function grab_task(first_try)
    if not procedure then
        error("No procedure has been imported.")
    else
        control.loading = true
        control.retries = 0
        
        local task = procedure.get_task(first_try)
        if task then
            tasks:push(task)
        elseif tasks:empty() then
            control.complete = true
        end

        control.loading = false
    end
end

function retry_task()
    control.loading = true
    control.retries = control.retries + 1
    log("Retrying task")
    
    if not control.current_task then
        error("Botter was instructed to retry a task, but no task was running.")
    else
        tasks:push(control.current_task)
    end
    
    control.loading = false
end

function handle_task(task)
    if not procedure then
        error("No procedure has been imported.")
    else
        local f = task.f
        
        if not f then
            error("The specified task could not be found in the loaded procedure.")
        else
            control.current_task = task
            procedure.initialize()
            log("Starting task - "..task.name)
            return try(f)
        end
    end
end

function task_result(result)
    result = result or ((result == nil) and true) or false

    if not procedure and control.running then
        error("A task provided a result but no procedure was loaded.")
    elseif procedure then
        if procedure.is_error() then
            return
        else
            if result then
                procedure.finalize()
                utils.finalize()
                procedure.advance_step()
            end
        end

        grab_task(result)
    end
end

function main()
    while true do
        if control.running and Sync() and not procedure.is_error() then
            if control.complete then
                if procedure.iterations and procedure.iterations > 0 then
                    procedure.restart()
                    control.complete = false
                    grab_task(true)
                    log("The procedure is restarting")
                else
                    utils.finalize()
                    procedure.exit()
                    control.current_task = nil
                    control.complete = false
                    control.running = false
                    procedure = nil
                    log("The procedure was completed successfully!")
                end
            elseif not control.loading and not tasks:empty() then
                local task = tasks:pop()
                local result = handle_task(task)
                if control.running then
                    task_result(result)
                end
            end 
            coroutine.sleep(.5)
        else
            coroutine.sleep(.5)
        end
    end
end

windower.register_event('addon command', handle_command)
coroutine.schedule(main, 0)
coroutine.schedule(Heartbeat, 1)
coroutine.schedule(Listen, 0.5)

function get_coords()
    local play = nil
    play = windower.ffxi.get_mob_by_target('me')
    
    if play then
        return {play.x,play.z,play.y}
    else
        return {0,0,0}
    end 
end

windower.register_event('prerender',function()
    local coords = get_coords()
    if control.recording and control.frame_number%3 == 0 then
        if not control.path:last() or control.path:last()[1] ~= coords[1] or
            control.path:last()[2] ~= coords[2] or
            control.path:last()[3] ~= coords[3]
        then
            if control.path:length() == 0 then
                control.path_file_handle:append("["..table.concat(coords,',').."]")
            else
                control.path_file_handle:append(",["..table.concat(coords,',').."]")
            end
            control.path:append(coords)
        end
    end
    control.frame_number = control.frame_number + 1
end)

function Record(start)
    if control.running then
        windower.add_to_chat(8,'Procedure error: You cannot record while running. First stop the bot')
    else
        if start and not control.recording then
            control.path_file_handle = file.new("data/paths/createdPath.json",true)
            control.recording = true
            control.path = L{}
            control.frame_number = 0
            control.path_file_handle:write('{"path": "[')
            log('Recording....')
        else
            control.recording = false
            if control.path:length() < 2 then
                error('Must have at least 2 points in a path.')
                control.path = L{}
            else
                control.path_file_handle:append(']"}')
                log("Path created: data/paths/createdPath.json  -- Rename if necessary")
            end
        end
    end
end

function PopulateProcedures(page_num)
    local i, t, popen = 0, {}, io.popen
    local data_dir = windower.addon_path .. 'data/'
    local count = 0
    for filename in popen('dir "'..data_dir..'" /b'):lines() do
        if string.find(filename, ".lua") then
            i = i + 1
            if i > (page_num * 7) and count <= 7 then
                count = count + 1
                t[count] = filename
            end
        end
    end

    if count == 8 then
        count = count + 1
        t[count] = "Next Page"
    end
    if page_num > 0 then
        count = count + 1
        t[count] = "Previous Page"
    end
    _procedures_display:update_options(t)
end

current_procedure_page = 0
PopulateProcedures(current_procedure_page)
windower.register_event('keyboard', function(dik,flags,blocked)
    local selection = dik - 1
    if _record_display:selected() and selection > 0 and selection < 10 and not flags then 
        local selection = _record_display:get_text(selection)
        if selection then
            if selection:lower() == "start" then
                Record(true)
            elseif selection:lower() == "stop" then
                Record(false)
            end
            return true
        end
    end
    if _procedures_display:selected() and selection > 0 and selection < 10 and not flags then 
        local selection = _procedures_display:get_text(selection)
        if selection then
            if selection == "Previous Page" then
                current_procedure_page = current_procedure_page - 1
                PopulateProcedures(current_procedure_page)
                return true
            end
            if selection == "Next Page" then
                current_procedure_page = current_procedure_page + 1
                PopulateProcedures(current_procedure_page)
                return true
            end 

            selection = selection:gsub(".lua", "")
            control.procedure_name = selection
            _procedures_display:visibility(false)
            _run_display:visibility(true)
            return true
        end
    end
    if _run_display:selected() and selection > 0 and selection < 10 and not flags then 
        local selection = _run_display:get_text(selection)
        if selection then
            if selection:lower() == "start" then
                load_procedure(control.procedure_name)
                _run_display:update_options(T{"Stop", "Pause"})
            elseif selection:lower() == "stop" then
                Terminate()
                _run_display:update_options(T{"Start", "Reset"})
            elseif selection:lower() == "pause" then
                control.running = false
                if procedure then
                    utils.finalize()
                end
                control.complete = false
                tasks:clear()
                tasks:push(control.current_task)
                log("Pausing procedure "..control.procedure_name.."...")
                _run_display:update_options(T{"Resume", "Stop"})
            elseif selection:lower() == "resume" then
                control.running = true
                log("Resuming procedure "..control.procedure_name.."...")
                _run_display:update_options(T{"Stop", "Pause"})
            elseif selection:lower() == "reset" then
                current_procedure_page = 0
                PopulateProcedures(current_procedure_page)
                _procedures_display:visibility(true)
                _run_display:visibility(false)
            end
            return true
        end
    end

    return false
end)
