---------------------------------------------------------------------------
---------------------------------------------------------------------------
---------------------------------------------------------------------------
---------------------------------------------------------------------------

function get_base()

	local f, err = dofile(windower.addon_path .. 'data/base.lua')
	
	if f and not err then
		return f
	else
		error("Could not locate the base procedure file.")
		return {}
	end


end

procedure = get_base()

if not procedure then return false end


---------------------------------------------------------------------------
---------------------------------------------------------------------------
---------------- CUSTOM DEFINED SECTION: EDIT BELOW -----------------------
---------------------------------------------------------------------------
---------------------------------------------------------------------------

packets = require('packets')

procedure._repeat = false
procedure.name = "Assault Points"

procedure.settings = {}
procedure.settings.is = 0

function procedure.update_currency()
	local p = packets.new('outgoing', 0x10F)
	packets.inject(p)
end

function procedure.validate(index)
	
    local mob = type(index) == "number" and windower.ffxi.get_mob_by_index(index) or type(index) == "string" and windower.ffxi.get_mob_by_name(index) or nil
	local valid = false
	
	if mob then
		valid = true
		if math.sqrt(mob.distance) < 9 then
			return mob
		end
    end

    if valid then
        warning("Too far from away from NPC")
    else
        warning("Unable to locate NPC.")
    end
end

function procedure.poke(index)
    local mob = procedure.validate(index)
    if mob then
        local p = packets.new('outgoing', 0x1a, {
            ["Target"] = mob.id,
            ["Target Index"] = mob.index,
        })
        packets.inject(p)
    end
end


function procedure.check_is()
    procedure.block("update_currency")
		
	procedure.update_currency()
    
    if not procedure.wait(10, "update_currency") then return false end
    
    if procedure.settings.is < 100 then
        procedure.error("You need more Imperial Standing (%d)":format(procedure.settings.is))
    end

end

function procedure.to_aht_urhgan()

	if windower.ffxi.get_info().zone ~= 50 then

		procedure.block("zone")
		procedure.block("hp_click")
		
		for hp in L{"Home Point #1", "Home Point #2", "Home Point #3", "Home Point #4", "Home Point #5"}:it() do
			local mob = windower.ffxi.get_mob_by_name(hp)
			if mob and mob.distance < 9 then
				procedure.poke(mob.index)
				break
			end
		end
				
		if not procedure.wait(10, "hp_click") then return false end
		
		return procedure.wait(20, "zone")
	else
		return true
	end

end

function procedure.hp_to_commission_door()
    utils.run_path('assault/hp_to_commission_door')
    utils.wait_for_path()

end

function procedure.get_tag()
	
	local kis = S(windower.ffxi.get_key_items())
	
	if not kis:contains(787) then
		procedure.block("get_tag")
        
		procedure.poke(155)
		
		return procedure.wait(30,"get_tag")
	end
	
	return true
	
end

function procedure.approach_assault_npcs()
	
	utils.run_path("assault/to_assault_npcs")
    utils.wait_for_path()

end

function procedure.grab_assault()
	
	procedure.block("get_orders")
	procedure.block("receive_ki")
	
	procedure.poke("Bhoy Yhupplo")
	
	if not procedure.wait(10,"get_orders") then return false end
	
	return procedure.wait(10, "receive_ki")

end

function procedure.to_door_from_orders()

	utils.run_path("assault/to_door_from_orders")
    utils.wait_for_path()
end

function procedure.to_runic_portal_door()

	procedure.poke("Door: Commissions Agency")
	
	utils.run_path("assault/to_runic_portal_door")
    utils.wait_for_path()
end

function procedure.to_runic_portal()

	procedure.poke("Door: Chamber of Passage")
	
	utils.run_path("assault/to_runic_portal")
    utils.wait_for_path()
end

function procedure.use_runic_portal()
		
		procedure.block("zone")
		procedure.block("portal_response")
		
		procedure.poke("Runic Portal")
		
		if not procedure.wait(10, "portal_response") then return false end
        
		return procedure.wait(20, "zone")

end

function procedure.to_armband()

	utils.run_path("assault/to_armband_ilrusi")
    utils.wait_for_path()
end

function procedure.get_armband()
	
	procedure.block("get_armband")

		
	procedure.poke("Meyaada")
		
	return procedure.wait(10,"get_armband")
end

function procedure.to_seal()

	utils.run_path("assault/to_seal_ilrusi")
    utils.wait_for_path()

end

function procedure.commence_assault()
		
		procedure.block("zone")		
		
		procedure.poke("Runic Seal")
        
        if not procedure.wait(10, 'runic_seal_response') then return false end
		
		coroutine.sleep(2)
		
		windower.send_command('setkey enter down;setkey enter up') 
		
		coroutine.sleep(2)

		windower.send_command('setkey enter down;setkey enter up') 
		
		coroutine.sleep(2)
		
		windower.send_command('setkey up down;setkey up up') 
		
		coroutine.sleep(2)
		
		windower.send_command('setkey enter down;setkey enter up') 
		
		coroutine.sleep(2)
		
		windower.send_command('setkey up down;setkey up up') 
		
		coroutine.sleep(2)
		
		windower.send_command('setkey enter down;setkey enter up') 
		
		return procedure.wait(60, "zone")
end

function procedure.to_khimaira()
    utils.run_path('assault/to_khimaira')
    utils.wait_for_path()
end

function procedure.fight_khimaira()
    utils.run_path('assault/fight_khimaira')
    utils.fight_monsters(L{"Khimaira 14X"})
    utils.initiate_combat_routine('blu_trash')
    
    coroutine.sleep(5)
    
    local mob = windower.ffxi.get_mob_by_index(155)
    
    return mob and mob.hpp == 0
    
end

function procedure.to_rune_of_release()
    utils.run_path('assault/to_rune_of_release')
    utils.wait_for_path()
end

function procedure.wait_for_rune()
    
    coroutine.sleep(5)

    local mob = windower.ffxi.get_mob_by_index(159)
    
    return mob and mob.valid_target

end

function procedure.exit_assault()

    procedure.block("rune_of_release_response")
    procedure.block("zone")

    procedure.poke(159)
    
    if not procedure.wait(10, "rune_of_release_response") then return false end
    
    return procedure.wait(30, "zone")
    

end

function procedure.to_town_portal()

	utils.run_path("assault/to_town_portal_ilrusi")
    utils.wait_for_path()
end

function procedure.to_door_from_portal()
	
	utils.run_path("assault/to_door_from_portal")
    utils.wait_for_path()
end

function procedure.runic_portal_to_commission_door()

	procedure.poke("Door: Chamber of Passage")
	
	utils.run_path("assault/runic_portal_to_commission_door")
    utils.wait_for_path()

end

function procedure.to_orders_npc()

	procedure.poke("Door: Commissions Agency")
	
	utils.run_path("assault/to_orders_npc")
    utils.wait_for_path()

end

function procedure.complete_assault()
    procedure.poke("Rytaal")
    coroutine.sleep(2)
end

function procedure.return_to_homepoint()
	procedure.poke("Door: Commissions Agency")
    utils.run_path("assault/return_to_homepoint")
    utils.wait_for_path()
end

procedure.triggers = {}

function procedure.triggers.zone(id, data)	
	if not procedure.blocked('zone') then return end
    
    if id == 0x0a then
			procedure.complete("zone")
	end
end

function procedure.triggers.click_portal(id,data)
	if not procedure.blocked('portal_response') then return end
    
    if id == 0x32 then
		local p = packets.parse('incoming',data)
		
		local mob = windower.ffxi.get_mob_by_index(p['NPC Index'])
		
		if mob and mob.name == "Runic Portal" then
			   local inject = packets.new("outgoing", 0x5b, {
					['Target'] = p['NPC'],
					['Option Index'] = 0x01,
					['Target Index'] = p['NPC Index'],
					['Zone'] = p['Zone'],
					['Menu ID'] = p['Menu ID']
				})
				packets.inject(inject)
				procedure.complete("portal_response")
			return true
		end
	end
end

function procedure.triggers.click_rune_of_release(id,data)
	if not procedure.blocked('rune_of_release_response') then return end
    
    if id == 0x34 then
		local p = packets.parse('incoming',data)
		
		local mob = windower.ffxi.get_mob_by_index(p['NPC Index'])
		
		if mob and mob.name == "Rune of Release" then
			   local inject = packets.new("outgoing", 0x5b, {
					['Target'] = p['NPC'],
					['Option Index'] = 1,
					['Target Index'] = p['NPC Index'],
					['Zone'] = p['Zone'],
					['Menu ID'] = p['Menu ID']
				})
				packets.inject(inject)
				procedure.complete("rune_of_release_response")
			return true
		end
	end
end

function procedure.triggers.get_orders(id,data)
    if not procedure.blocked('get_orders') then return end

	if id == 0x34 then
		local p = packets.parse('incoming',data)
		
		local mob = windower.ffxi.get_mob_by_index(p['NPC Index'])
		
		if mob and L{"Yahsra","Isdebaaq","Famad","Lageegee","Bhoy Yhupplo"}:contains(mob.name) then
			   local inject = packets.new("outgoing", 0x5b, {
					['Target'] = p['NPC'],
					['Option Index'] = 0x0321,
					['Target Index'] = p['NPC Index'],
					['Zone'] = p['Zone'],
					['Menu ID'] = p['Menu ID']
				})
				packets.inject(inject)
				procedure.complete("get_orders")
			return true
		end
	end
end

function procedure.triggers.receive_ki(id,data)
    if not procedure.blocked('receive_ki') then return end

	if id == 0x2a then
		local p = packets.parse('incoming',data)
		
		if L{762,763,764,765,766}:contains(p['Param 1']) then
			procedure.complete('receive_ki')
		end		
		
	end
end

function procedure.triggers.get_armband(id,data)
	if not procedure.blocked('get_armband') then return end
    
    if id == 0x34 then
		local p = packets.parse('incoming',data)
		
		local mob = windower.ffxi.get_mob_by_index(p['NPC Index'])
		
		if mob and L{"Nareema","Daswil","Waudeen","Nahshib","Meyaada"}:contains(mob.name) then
			   local inject = packets.new("outgoing", 0x5b, {
					['Target'] = p['NPC'],
					['Option Index'] = 1,
					['Target Index'] = p['NPC Index'],
					['Zone'] = p['Zone'],
					['Menu ID'] = p['Menu ID']
				})
				packets.inject(inject)
				procedure.complete("get_armband")
			return true
		end
	end
end

function procedure.triggers.get_tag(id,data)
    if not procedure.blocked('get_tag') then return end

	if id == 0x34 then
		local p = packets.parse('incoming',data)
		
		local mob = windower.ffxi.get_mob_by_index(p['NPC Index'])
		
		if mob and mob.name == "Rytaal" then
			
			local tags = p['Menu Parameters']:unpack('I', 5)
			
			if tags > 0 then
			   local inject = packets.new("outgoing", 0x5b, {
					['Target'] = p['NPC'],
					['Option Index'] = 0x0001,
					['Target Index'] = p['NPC Index'],
					['Zone'] = p['Zone'],
					['Menu ID'] = p['Menu ID']
				})
				packets.inject(inject)
				procedure.complete("get_tag")
			else
				local inject = packets.new("outgoing", 0x5b, {
					['Target'] = p['NPC'],
					['Option Index'] = 0x0000,
					['_unknown1'] = 0x4000,
					['Target Index'] = p['NPC Index'],
					['Zone'] = p['Zone'],
					['Menu ID'] = p['Menu ID']
				})
				packets.inject(inject)
			end
			
			return true
		end
	
	end
	
end

function procedure.triggers.update_currency(id,data)
	if not procedure.blocked('update_currency') then return end

	if id == 0x113 then
		local p = packets.parse('incoming',data)
		procedure.settings.is = p['Imperial Standing']
		procedure.complete('update_currency')
	end
end

function procedure.triggers.runic_seal_click(id,data)
    if not procedure.blocked('runic_seal_response') then return end
    
    if id == 0x34 then
        local p = packets.parse('incoming', data)
        
        local mob = windower.ffxi.get_mob_by_index(p['NPC Index'])
        
        if mob and mob.name == "Runic Seal" then
            procedure.complete('runic_seal_response')
        end
    end

end

function procedure.triggers.hp_click(id,data)
	if not procedure.blocked('hp_click') then return end

	if id == 0x34 then
		local p = packets.parse('incoming',data)
		
		local mob = windower.ffxi.get_mob_by_index(p['NPC Index'])
		
		if mob and S{"Home Point #1","Home Point #2","Home Point #3","Home Point #4","Home Point #5"}:contains(mob.name) then
		
			   local inject = packets.new("outgoing", 0x5b, {
					['Target'] = p['NPC'],
					['Option Index'] = 0x0002,
					['_unknown1'] = 0x006A,
					['Target Index'] = p['NPC Index'],
					['Zone'] = p['Zone'],
					['Menu ID'] = p['Menu ID']
				})
				packets.inject(inject)
				procedure.complete("hp_click")
			
			return true
		end
	
	end

end

procedure.register_local_event('incoming chunk', procedure.triggers.hp_click)
procedure.register_local_event('incoming chunk', procedure.triggers.click_portal)
procedure.register_local_event('incoming chunk', procedure.triggers.get_armband)
procedure.register_local_event('incoming chunk', procedure.triggers.get_orders)
procedure.register_local_event('incoming chunk', procedure.triggers.zone)
procedure.register_local_event('incoming chunk', procedure.triggers.click_portal)
procedure.register_local_event('incoming chunk', procedure.triggers.get_tag)
procedure.register_local_event('incoming chunk', procedure.triggers.receive_ki)
procedure.register_local_event('incoming chunk', procedure.triggers.click_rune_of_release)
procedure.register_local_event('incoming chunk', procedure.triggers.update_currency)
procedure.register_local_event('incoming chunk', procedure.triggers.runic_seal_click)

procedure.add("check_is", procedure.check_is, false)
procedure.add("to_aht_urhgan", procedure.to_aht_urhgan, false)
procedure.add("hp_to_commission_door", procedure.hp_to_commission_door, false)
procedure.add("to_orders_npc", procedure.to_orders_npc, false)
procedure.add("get_tag", procedure.get_tag, false)
procedure.add("approach_assault_npcs", procedure.approach_assault_npcs, false)
procedure.add("grab_assault", procedure.grab_assault, false)
procedure.add("to_door_from_orders", procedure.to_door_from_orders, false)
procedure.add("to_runic_portal_door", procedure.to_runic_portal_door, false)
procedure.add("to_runic_portal", procedure.to_runic_portal, false)
procedure.add("use_runic_portal", procedure.use_runic_portal, false)
procedure.add("to_armband", procedure.to_armband, false)
procedure.add("get_armband", procedure.get_armband, false)
procedure.add("to_seal", procedure.to_seal, false)
procedure.add("commence_assault", procedure.commence_assault, false)
procedure.add("to_khimaira", procedure.to_khimaira, false)
procedure.add("fight_khimaira", procedure.fight_khimaira, false)
procedure.add("to_rune_of_release", procedure.to_rune_of_release, false)
procedure.add("wait_for_rune", procedure.wait_for_rune, false)
procedure.add("exit_assault", procedure.exit_assault, false)
procedure.add("to_town_portal", procedure.to_town_portal, false)
procedure.add("use_runic_portal", procedure.use_runic_portal, false)
procedure.add("to_door_from_portal", procedure.to_door_from_portal, false)
procedure.add("runic_portal_to_commission_door", procedure.runic_portal_to_commission_door, false)
procedure.add("to_orders_npc", procedure.to_orders_npc, false)
procedure.add("complete_assault", procedure.complete_assault, false)
procedure.add("to_door_from_orders", procedure.to_door_from_orders, false)
procedure.add("return_to_homepoint", procedure.return_to_homepoint, false)


return procedure