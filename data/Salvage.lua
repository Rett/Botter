---------------------------------------------------------------------------
---------------------------------------------------------------------------
---------------------------------------------------------------------------
---------------------------------------------------------------------------

function get_base()
    local f, err = dofile(windower.addon_path .. 'data/base.lua')
    
    if f and not err then
        return f
    else
        error("Could not locate the base procedure file.")
        return {}
    end
end

procedure = get_base()

if not procedure then
    return false
end

---------------------------------------------------------------------------
---------------------------------------------------------------------------
---------------- CUSTOM DEFINED SECTION: EDIT BELOW -----------------------
---------------------------------------------------------------------------
---------------------------------------------------------------------------

packets = require('packets')

procedure.name = "Salvage (Bhaflau)"

packets.raw_fields.outgoing[0x05C] = L {
    {ctype = 'float', label = 'X'}, -- 04
    {ctype = 'float', label = 'Z'}, -- 08
    {ctype = 'float', label = 'Y'}, -- 0C
    {ctype = 'unsigned int', label = 'Target ID', fn = id}, -- 10   NPC that you are requesting a warp from
    {ctype = 'unsigned int', label = '_unknown1'}, -- 14   01 00 00 00 observed
    {ctype = 'unsigned short', label = 'Zone'}, -- 18   Likely contains information about the particular warp being requested, like menu ID
    {ctype = 'unsigned short', label = 'Menu ID'}, -- 1A   Likely contains information about the particular warp being requested, like menu ID
    {ctype = 'unsigned short', label = 'Target Index', fn = index}, -- 1C
    {ctype = 'unsigned short', label = '_unknown3'} -- 1E   Not zone ID
}

procedure.settings = {}

procedure.settings.main1 = "Colada"
procedure.settings.main2 = "Colada"
procedure.settings.main3 = "Iris"
procedure.settings.main4 = "Tanmogayi"
procedure.settings.main5 = "Usonmunku"
procedure.settings.main6 = "Tanmogayi +1"
procedure.settings.main7 = "Claidheamh Soluis"
procedure.settings.main8 = "Buramenk'ah"
procedure.settings.main9 = "Hammerfists"
procedure.settings.main10 = "Nibiru sainti"
procedure.settings.main11 = "Oatixur"
procedure.settings.main12 = "Nibiru blade"
procedure.settings.dummy = "Firmament"
procedure.settings.dummy1 = "Eight-Sided Pole"
procedure.settings.points = {}
procedure.settings.assault = false

procedure.iterations = 8

procedure.portals = {[1] = {['X'] = 620, ['Z'] = -0.4990000128746, ['Y'] = -303, ['Zone ID'] = 72, ['Menu ID'] = 220, ['_unknown3'] = 0xBF01}}

function procedure.equip_primary_weapons()
    if windower.ffxi.get_player().name == "Konijn" then
        windower.send_command(('input /equip main "%s";wait 1;input /equip sub "%s"'):format(procedure.settings.main1, procedure.settings.main2))
    elseif windower.ffxi.get_player().name == "Mrevrart" then
        windower.send_command(('input /equip main "%s";wait 1;input /equip sub "%s"'):format(procedure.settings.main1, procedure.settings.main2))
    elseif windower.ffxi.get_player().name == "Gogoeb" then
        windower.send_command(('input /equip main "%s";wait 1;input /equip sub "%s"'):format(procedure.settings.main7, procedure.settings.main8))
    elseif windower.ffxi.get_player().name == "Snookiepants" then
        windower.send_command(('input /equip main "%s";wait 1;input /equip sub "%s"'):format(procedure.settings.main6, procedure.settings.main2))
    elseif windower.ffxi.get_player().name == "Arico" then
        windower.send_command(('input /equip main "%s";wait 1;input /equip sub "%s"'):format(procedure.settings.main1, procedure.settings.main3))
    elseif windower.ffxi.get_player().name == "Parmenides" then
        windower.send_command(('input /equip main "%s";wait 1;input /equip sub "%s"'):format(procedure.settings.main4, procedure.settings.main5))
    elseif windower.ffxi.get_player().name == "Tatulife" then
        windower.send_command(('input /equip main "%s";wait 1;input /equip sub "%s"'):format(procedure.settings.main9, procedure.settings.main9))
    elseif windower.ffxi.get_player().name == "Rettg" then
        windower.send_command(('input /equip main "%s";wait 1;input /equip sub "%s"'):format(procedure.settings.main9, procedure.settings.main9))
    elseif windower.ffxi.get_player().name == "Ropalie" then
        windower.send_command(('input /equip main "%s";wait 1;input /equip sub "%s"'):format(procedure.settings.main10, procedure.settings.main10))
    elseif windower.ffxi.get_player().name == "Sanholo" then
        windower.send_command(('input /equip main "%s";wait 1;input /equip sub "%s"'):format(procedure.settings.main9, procedure.settings.main9))
    elseif windower.ffxi.get_player().name == "Dahakone" then
        windower.send_command(('input /equip main "%s";wait 1;input /equip sub "%s"'):format(procedure.settings.main10, procedure.settings.main10))
    elseif windower.ffxi.get_player().name == "Babyangus" then
        windower.send_command(('input /equip main "%s";wait 1;input /equip sub "%s"'):format(procedure.settings.main12, procedure.settings.main12))
    elseif windower.ffxi.get_player().name == "Calypso" then
        windower.send_command(('input /equip main "%s";wait 1;input /equip sub "%s"'):format(procedure.settings.main12, procedure.settings.main12))
    elseif windower.ffxi.get_player().name == "Lesieben" then
        windower.send_command(('input /equip main "%s";wait 1;input /equip sub "%s"'):format(procedure.settings.main11, procedure.settings.main11))
    else
        windower.send_command(('input /equip main "%s";wait 1;input /equip sub "%s"'):format(procedure.settings.main2, procedure.settings.main2))
    end
end

function procedure.equip_dummy_weapons()
    if windower.ffxi.get_player().name == "Tatulife" then
        windower.send_command(('input /equip main "%s";wait 1;input /equip sub "%s"'):format(procedure.settings.dummy1, procedure.settings.dummy1))
    elseif windower.ffxi.get_player().name == "Sanholo" then
        windower.send_command(('input /equip main "%s";wait 1;input /equip sub "%s"'):format(procedure.settings.dummy1, procedure.settings.dummy1))
    elseif windower.ffxi.get_player().name == "Rettg" then
        windower.send_command(('input /equip main "%s";wait 1;input /equip sub "%s"'):format(procedure.settings.dummy1, procedure.settings.dummy1))
    elseif windower.ffxi.get_player().name == "Lesieben" then
        windower.send_command(('input /equip main "%s";wait 1;input /equip sub "%s"'):format(procedure.settings.dummy1, procedure.settings.dummy1))
    elseif windower.ffxi.get_player().name == "Ropalie" then
        windower.send_command(('input /equip main "%s";wait 1;input /equip sub "%s"'):format(procedure.settings.dummy1, procedure.settings.dummy1))
    elseif windower.ffxi.get_player().name == "Dahakone" then
        windower.send_command(('input /equip main "%s";wait 1;input /equip sub "%s"'):format(procedure.settings.dummy1, procedure.settings.dummy1))
    else
        windower.send_command(('input /equip main "%s";wait 1;input /equip sub "%s"'):format(procedure.settings.dummy, procedure.settings.dummy))
    end
end


function procedure.update_currency()
    local p = packets.new('outgoing', 0x10F)
    packets.inject(p)
end

function procedure.to_aht_urhgan()
    windower.send_command('input /pcmd leave')

    if windower.ffxi.get_info().zone ~= 50 then
        procedure.block("zone")
        procedure.block("hp_click")
        
        for hp in L {"Home Point #1", "Home Point #2", "Home Point #3", "Home Point #4", "Home Point #5"}:it() do
            local mob = windower.ffxi.get_mob_by_name(hp)
            if mob and math.sqrt(mob.distance) < 6 then
                procedure.poke(mob.index)
                break
            end
        end
        
        local res = procedure.wait(10, "hp_click")
        
        if not res then
            return res
        end
        
        return procedure.wait(20, "zone")
    else
        return true
    end
end

function procedure.to_zasshal()
    utils.run_path('salvage/to_zasshal')
    utils.wait_for_path()
end

function procedure.get_permit()
    local kis = S(windower.ffxi.get_key_items())
    
    if not kis:contains(854) then
        procedure.block("receive_ki")
        procedure.block("zasshal_response")
        procedure.block("update_currency")
        
        procedure.update_currency()
        
        if not procedure.wait(10, "update_currency") then
            return false
        end
        
        local choice = nil
        
        for k, v in pairs(procedure.settings.points) do
            if v >= 500 then
                choice = k
                break
            end
        end
        
        if not choice then
            --Go do an assault
            procedure.error("You ran out of assault points!")
            return true
        else
            procedure.poke("Zasshal")
            
            if not procedure.wait(10, "zasshal_response") then
                return false
            end
            
            local choices = {0x000a, 0x000b, 0x000c, 0x000d, 0x000e}
            local inject = packets.new("outgoing", 0x5b, {['Target'] = 16982309, ['Option Index'] = 0x0000, ['_unknown1'] = 0x0000, ['Automated Message'] = true, ['Target Index'] = 293, ['Zone'] = 50, ['Menu ID'] = 820})
            local inject2 = packets.new("outgoing", 0x5b, {['Target'] = 16982309, ['Option Index'] = choices[choice], ['_unknown1'] = 0x0000, ['Automated Message'] = true, ['Target Index'] = 293, ['Zone'] = 50, ['Menu ID'] = 820})
            local inject3 = packets.new("outgoing", 0x5b, {['Target'] = 16982309, ['Option Index'] = 0x0064, ['_unknown1'] = 0x0000, ['Automated Message'] = false, ['Target Index'] = 293, ['Zone'] = 50, ['Menu ID'] = 820})
            
            packets.inject(inject)
            packets.inject(inject2)
            packets.inject(inject3)
        end
        
        if not procedure.wait(10, "receive_ki") then
            return false
        end
        
        coroutine.sleep(5)
        
        kis = S(windower.ffxi.get_key_items())
        
        return kis:contains(854)
    end
end

function procedure.to_chamber_of_passage()
    utils.run_path('salvage/salvage_to_chamber_of_passage')
    utils.wait_for_path()
end

function procedure.to_runic_portal()
    procedure.poke("Door: Chamber of Passage")
    
    utils.run_path('salvage/to_runic_portal')
    utils.wait_for_path()
end

function procedure.click_portal()
    procedure.block("zone")
    procedure.block("portal_response")
    
    procedure.poke("Runic Portal")
    
    if not procedure.wait(10, "portal_response") then
        return false
    end
    
    return procedure.wait(20, "zone")
end

function procedure.to_gilded_doors()
    utils.run_path('salvage/runic_portal_to_gilded_doors')
    utils.wait_for_path()
end

function procedure.click_gilded_doors()
    procedure.block("assault_gilded_doors_response")
    
    procedure.poke(89)
    
    return procedure.wait(10, "assault_gilded_doors_response")
end

function procedure.to_second_gilded_doors()
    utils.run_path('salvage/gilded_doors_to_gilded_doors')
    utils.wait_for_path()
end

function procedure.to_salvage_portal()
    procedure.block("portal_popup_alzaadal")
    procedure.block("warp_response")
    
    procedure.poke(96)
    utils.run_path('salvage/gilded_doors_to_portal')
    
    if not procedure.wait(15, "portal_popup_alzaadal") then
        return false
    end
    
    return procedure.wait(10, "warp_response")
end

function procedure.alzaadal_portal_2()
    procedure.block("portal_popup_alzaadal")
    procedure.wait(5, "portal_popup_alzaadal")
end

function procedure.use_salvage_portal()
    procedure.block("exit_cutscene")
    coroutine.sleep(2)
    windower.send_command('setkey up down;setkey up up')
    coroutine.sleep(2)
    windower.send_command('setkey enter down;setkey enter up')
    coroutine.sleep(2)
    return procedure.wait(20, "exit_cutscene")
end

function procedure.to_salvage_entrance()
    utils.run_path('salvage/to_salvage_entrance')
    utils.wait_for_path()
end

function procedure.enter_salvage()
    procedure.block('zone')
    procedure.block('gilded_gateway_response')
    procedure.block('salvage_reservation')
    
    procedure.poke(105)
    
    if not procedure.wait(10, "gilded_gateway_response") then
        return false
    end
    
    local reserve = packets.new("outgoing", 0x5b, {['Target'] = 17072233, ['_unknown1'] = 0x0C, ['Target Index'] = 105, ['Zone'] = 72, ['Menu ID'] = 409, ['Automated Message'] = true})
    
    packets.inject(reserve)
    
    if not procedure.wait(30, "salvage_reservation") then
        local esc = packets.new("outgoing", 0x5b, {['Target'] = 17072233, ['Option Index'] = 0x0000, ['_unknown1'] = 0x4000, ['Target Index'] = 105, ['Zone'] = 72, ['Menu ID'] = 409})
        packets.inject(esc)
        return false
    end
    
    local enter = packets.new("outgoing", 0x5b, {['Target'] = 17072233, ['Option Index'] = 0x04, ['Target Index'] = 105, ['Zone'] = 72, ['Menu ID'] = 409})
    
    procedure.wait(1)
    packets.inject(enter)
    
    return procedure.wait(60, "zone")
end

function procedure.f1_entrance_to_gilded_doors()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.run_path('salvage/f1_entrance_to_gilded_doors')
    utils.wait_for_path()
end

function procedure.f1_click_gilded_doors_1()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    procedure.block("gilded_doors_response")
    
    procedure.poke(473)
    
    return procedure.wait(10, "gilded_doors_response")
end

function procedure.f1_gilded_doors_to_runic_lamp()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.run_path('salvage/f1_gilded_doors_to_runic_lamp')
    
    utils.wait_for_path()
end

function procedure.click_runic_lamp()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    procedure.block("runic_lamp_response")
    
    procedure.poke(442)
    
    return procedure.wait(10, "runic_lamp_response")
end

function procedure.f1_runic_lamp_to_gilded_doors()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.run_path('salvage/f1_runic_lamp_to_gilded_doors')
    utils.wait_for_path()
end

function procedure.f1_click_gilded_doors_2()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    procedure.block("gilded_doors_response")
    
    procedure.poke(475)
    
    return procedure.wait(10, "gilded_doors_response")
end

function procedure.f1_gilded_doors_to_wamouracampa()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.run_path('salvage/f1_gilded_doors_to_wamouracampa')
    utils.wait_for_path()
end

function procedure.f1_kill_wamouracampa()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.initiate_combat_routine(('%s_salvage'):format(windower.ffxi.get_player().main_job:lower()))
    
    utils.run_path('salvage/f1_kill_wamouracampa')
    utils.fight_monsters(L {"Wamouracampa"})
    
    procedure.wait(5)
    
    local mob = utils.vars.monster_array[314]
    
    return mob and mob.hpp == 0
end

function procedure.f1_wamouracampa_to_wamoura()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.run_path('salvage/f1_wamouracampa_to_wamoura')
    utils.wait_for_path()
end

function procedure.f1_kill_wamoura()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.initiate_combat_routine(('%s_salvage'):format(windower.ffxi.get_player().main_job:lower()))
    
    utils.run_path('salvage/f1_kill_wamoura')
    utils.fight_monsters(L {"Wandering Wamoura"})
    
    procedure.wait(5)
    
    local mob = utils.vars.monster_array[317]
    
    return mob and mob.hpp == 0
end

function procedure.f1_wamoura_to_gilded_doors()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.run_path('salvage/f1_wamoura_to_gilded_doors')
    utils.wait_for_path()
end

function procedure.f1_click_gilded_doors_3()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    procedure.block("gilded_doors_response")
    
    procedure.poke(476)
    
    return procedure.wait(10, "gilded_doors_response")
end

function procedure.f1_gilded_doors3_to_gilded_doors4()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.run_path('salvage/f1_gilded_doors3_to_gilded_doors4')
    utils.wait_for_path()
end

function procedure.f1_click_gilded_doors_4()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    procedure.block("gilded_doors_response")
    
    procedure.poke(477)
    
    return procedure.wait(10, "gilded_doors_response")
end

function procedure.cast_invisible()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    if L {"RDM", "WHM"}:contains(utils.info.player.main_job) or L {"RDM", "WHM"}:contains(utils.info.player.sub_job) then
        windower.send_command('input /ma "Invisible" <me>')
        procedure.wait(5)
        local buffs = L(windower.ffxi.get_player().buffs)
        return buffs:contains(69)
    elseif utils.info.player.main_job == "DNC" or utils.info.player.sub_job == "DNC" then
        windower.send_command('input /ja "Spectral Jig" <me>')
        procedure.wait(5)
        local buffs = L(windower.ffxi.get_player().buffs)
        return buffs:contains(69)
    else
        return true
    end
end

function procedure.cancel_invisible()
    windower.ffxi.cancel_buff(69)
end

function procedure.f1_gilded_doors_to_goblin()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.run_path('salvage/f1_gilded_doors_to_goblin')
    utils.wait_for_path()
end

function procedure.f1_kill_goblin()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.initiate_combat_routine(('%s_salvage'):format(windower.ffxi.get_player().main_job:lower()))
    
    utils.run_path('salvage/f1_kill_goblins', 'circular')
    utils.fight_monsters(L {"Moblin Poniardman"})
    
    procedure.wait(5)
    
    for index in L {319}:it() do
        local mob = utils.vars.monster_array[index]
        
        if not mob or (mob and mob.hpp and mob.hpp ~= 0) then
            --print("[Goblins] Didn't finish killing.")
            return false
        end
    end
end

function procedure.f1_goblin_to_gilded_doors()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.run_path('salvage/f1_goblin_to_gilded_doors')
    utils.wait_for_path()
end

function procedure.f1_click_gilded_doors_5()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    procedure.block("gilded_doors_response")
    
    procedure.poke(478)
    
    return procedure.wait(10, "gilded_doors_response")
end

function procedure.f1_gilded_doors5_to_gilded_doors6()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.run_path('salvage/f1_gilded_doors5_to_gilded_doors6')
    utils.wait_for_path()
end

function procedure.f1_click_gilded_doors_6()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    procedure.block("gilded_doors_response")
    
    procedure.poke(482)
    
    return procedure.wait(10, "gilded_doors_response")
end

function procedure.f1_gilded_doors_to_dahak()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.run_path('salvage/f1_gilded_doors_to_dahak')
    utils.wait_for_path()
end

function procedure.f1_kill_dahak()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.initiate_combat_routine(('%s_salvage'):format(windower.ffxi.get_player().main_job:lower()))
    
    utils.run_path('salvage/f1_kill_dahak')
    utils.fight_monsters(L {"Dahak"})
    
    procedure.wait(5)
    
    local mob = utils.vars.monster_array[321]
    
    return mob and mob.hpp == 0
end

function procedure.f1_dahak_to_gilded_doors()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.run_path('salvage/f1_dahak_to_gilded_doors')
    utils.wait_for_path()
end

function procedure.f1_click_gilded_doors_7()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    procedure.block("gilded_doors_response")
    
    procedure.poke(483)
    
    return procedure.wait(10, "gilded_doors_response")
end

function procedure.f1_gilded_doors_to_portal()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    procedure.block("portal_popup_f1")
    procedure.block("warp_response")
    
    utils.run_path('salvage/f1_gilded_doors_to_portal')
    
    if not procedure.wait(15, "portal_popup_f1") then
        return false
    end
    
    return procedure.wait(10, "warp_response")
end

------ FLOOR 2

function procedure.f2_portal_to_gilded_doors()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    procedure.wait(2)
    utils.initiate_combat_routine(('%s_salvage'):format(windower.ffxi.get_player().main_job:lower()))
    utils.fight_monsters(L {"Troll Stoneworker"})
    utils.run_path('salvage/f2_portal_to_gilded_doors')
    utils.wait_for_path()
end

function procedure.f2_click_gilded_doors_1()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    procedure.block("gilded_doors_response")
    
    procedure.poke(485)
    
    return procedure.wait(10, "gilded_doors_response")
end

function procedure.f2_gilded_doors_to_second_troll()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.run_path('salvage/f2_gilded_doors_to_second_troll')
    utils.wait_for_path()
end

function procedure.f2_kill_second_troll()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.initiate_combat_routine(('%s_salvage'):format(windower.ffxi.get_player().main_job:lower()))
    
    utils.run_path('salvage/f2_kill_second_troll')
    utils.fight_monsters(L {"Troll Lapidarist"})
    
    procedure.wait(5)
    
    local mob = utils.vars.monster_array[328]
    
    if mob and mob.hpp ~= 0 then
        return false
    end
end

function procedure.f2_to_gilded_doors_2()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.run_path('salvage/f2_kill_second_troll')
    
    utils.wait_for_path()
end

function procedure.f2_click_gilded_doors_2()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    procedure.block("gilded_doors_response")
    
    procedure.poke(489)
    
    return procedure.wait(10, "gilded_doors_response")
end

function procedure.f2_gilded_doors2_to_gilded_doors3()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.run_path('salvage/f2_gilded_doors2_to_gilded_doors3')
    utils.wait_for_path()
end

function procedure.f2_click_gilded_doors_3()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    procedure.block("gilded_doors_response")
    
    procedure.poke(493)
    
    return procedure.wait(10, "gilded_doors_response")
end

function procedure.f2_gilded_doors_to_portal()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    procedure.block("portal_popup_f2")
    procedure.block("warp_response")
    
    utils.run_path('salvage/f2_gilded_doors_to_portal')
    
    if not procedure.wait(15, "portal_popup_f2") then
        return false
    end
    
    return procedure.wait(10, "warp_response")
end

--- FLOOR 3

function procedure.f3_portal_to_hallway()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    procedure.wait(2)
    utils.run_path('salvage/f3_portal_to_hallway')
    utils.wait_for_path()
end

function procedure.f3_hallway_to_gilded_doors_1()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.initiate_combat_routine(('%s_salvage'):format(windower.ffxi.get_player().main_job:lower()))
    
    coroutine.sleep(3)
    
    utils.run_path('salvage/f3_hallway_to_gilded_doors_1')
    utils.fight_monsters(L {"Archaic Gear"})
    
    utils.wait_for_path()
end

function procedure.f3_click_gilded_doors_1()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    procedure.wait(3)
    
    procedure.block("gilded_doors_response")
    
    procedure.poke(499)
    
    return procedure.wait(10, "gilded_doors_response")
end

function procedure.f3_gilded_doors_to_hallway()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.run_path('salvage/f3_gilded_doors_to_hallway')
    utils.wait_for_path()
end

function procedure.f3_hallway_to_gilded_doors_2()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    coroutine.sleep(3)
    
    utils.initiate_combat_routine(('%s_salvage'):format(windower.ffxi.get_player().main_job:lower()))
    
    utils.run_path('salvage/f3_hallway_to_gilded_doors_2')
    utils.fight_monsters(L {"Archaic Gear"})
    
    utils.wait_for_path()
end

function procedure.f3_click_gilded_doors_2()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    procedure.wait(3)
    
    procedure.block("gilded_doors_response")
    
    procedure.poke(500)
    
    return procedure.wait(10, "gilded_doors_response")
end

function procedure.f3_gilded_doors_to_west_room()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.run_path('salvage/f3_gilded_doors_to_west_room')
    utils.wait_for_path()
end

function procedure.f3_kill_single_archaic_gears_west()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.initiate_combat_routine(('%s_salvage'):format(windower.ffxi.get_player().main_job:lower()))
    
    utils.run_path('salvage/f3_gears_west', 'circular')
    utils.fight_monsters(L {"Archaic Gear"})
    
    procedure.wait(5)
    
    for index in L {400, 401, 402}:it() do
        local mob = utils.vars.monster_array[index]
        
        if not mob or (mob and mob.hpp and mob.hpp ~= 0) then
            --print("[1] Didn't finish killing.")
            return false
        end
    end
end

function procedure.f3_to_rampart_west()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.run_path('salvage/f3_to_rampart_west')
    utils.fight_monsters(L {"Archaic Rampart"})
    
    procedure.wait(5)
    
    local gear_pop = utils.vars.monster_array[404]
    
    if not gear_pop then
        return false
    end
end

function procedure.f3_kill_triple_gears_west()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.initiate_combat_routine(('%s_salvage'):format(windower.ffxi.get_player().main_job:lower()))
    
    utils.run_path('salvage/f3_gears_west', 'circular')
    utils.fight_monsters(L {"Archaic Gears", "Archaic Rampart"})
    
    procedure.wait(5)
    
    for index in L {404, 405, 406, 407, 408, 409, 410, 411, 412, 413, 414, 415, 416, 417}:it() do
        local mob = utils.vars.monster_array[index]
        
        if (mob and mob.hpp and mob.hpp ~= 0) then
            --print("[2] Didn't finish killing.")
            return false
        end
    end
end

function procedure.f3_west_room_to_east_room()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.run_path('salvage/f3_west_room_to_east_room')
    utils.wait_for_path()
end

function procedure.f3_kill_single_archaic_gears_east()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.initiate_combat_routine(('%s_salvage'):format(windower.ffxi.get_player().main_job:lower()))
    
    utils.run_path('salvage/f3_gears_east', 'circular')
    utils.fight_monsters(L {"Archaic Gear"})
    
    procedure.wait(5)
    
    for index in L {382, 383, 384}:it() do
        local mob = utils.vars.monster_array[index]
        
        if not mob or (mob and mob.hpp and mob.hpp ~= 0) then
            --print("[3] Didn't finish killing.")
            return false
        end
    end
end

function procedure.f3_to_rampart_east()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.run_path('salvage/f3_to_rampart_east')
    utils.fight_monsters(L {"Archaic Rampart"})
    
    procedure.wait(5)
    
    local gear_pop = utils.vars.monster_array[386]
    
    if not gear_pop then
        return false
    end
end

function procedure.f3_kill_triple_gears_east()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.initiate_combat_routine(('%s_salvage'):format(windower.ffxi.get_player().main_job:lower()))
    
    utils.run_path('salvage/f3_gears_east', 'circular')
    utils.fight_monsters(L {"Archaic Gears", "Archaic Rampart"})
    
    procedure.wait(5)
    
    for index in L {386, 387, 388, 389, 390, 391, 392, 393, 394, 395, 396, 397, 398, 399}:it() do
        local mob = utils.vars.monster_array[index]
        
        if (mob and mob.hpp and mob.hpp ~= 0) then
            --print("[4] Didn't finish killing.")
            return false
        end
    end
end

function procedure.f3_rampart_to_gilded_doors()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.run_path('salvage/f3_rampart_to_gilded_doors')
    utils.wait_for_path()
end

function procedure.f3_click_gilded_doors_3()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    procedure.block("gilded_doors_response")
    
    procedure.poke(498)
    
    return procedure.wait(10, "gilded_doors_response")
end

function procedure.f3_gilded_doors_to_portal()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    procedure.block("portal_popup_f3")
    procedure.block("warp_response")
    
    utils.run_path('salvage/f3_gilded_doors_to_portal')
    
    if not procedure.wait(15, "portal_popup_f3") then
        return false
    end
    
    return procedure.wait(10, "warp_response")
end

function procedure.f4_portal_to_hallway()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    procedure.wait(2)
    utils.run_path('salvage/f4_portal_to_hallway')
    utils.wait_for_path()
end

function procedure.f4_hallway_to_gilded_doors()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    coroutine.sleep(3)
    
    utils.initiate_combat_routine(('%s_salvage'):format(windower.ffxi.get_player().main_job:lower()))
    
    utils.run_path('salvage/f4_hallway_to_gilded_doors')
    utils.fight_monsters(L {"Archaic Gear", "Acrolith", "Long-Bowed Chariot"})
    utils.wait_for_path()
end

function procedure.f4_click_gilded_doors_1()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    procedure.block("gilded_doors_response")
    
    procedure.wait(3)
    
    procedure.poke(503)
    
    return procedure.wait(10, "gilded_doors_response")
end

function procedure.f4_enter_gears_room()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.run_path('salvage/f4_enter_gears_room')
    utils.wait_for_path()
end

function procedure.f4_kill_triple_gears()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.initiate_combat_routine(('%s_salvage'):format(windower.ffxi.get_player().main_job:lower()))
    
    utils.run_path('salvage/f4_triple_gears', 'circular')
    utils.fight_monsters(L {"Archaic Gears", "Acrolith", "Long-Bowed Chariot"})
    
    procedure.wait(5)
    
    for index in L {432, 433, 434}:it() do
        local mob = utils.vars.monster_array[index]
        
        if mob and mob.hpp and mob.hpp ~= 0 then
            return false
        end
    end
end

function procedure.f4_to_rampart()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.run_path('salvage/f4_to_rampart')
    utils.fight_monsters(L {"Archaic Rampart"})
    
    procedure.wait(5)
    
    local cerb = utils.vars.monster_array[437]
    
    if not cerb then
        return false
    end
end

function procedure.f4_to_portal_room()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.initiate_combat_routine(('%s_salvage'):format(windower.ffxi.get_player().main_job:lower()))
    
    utils.run_path('salvage/f4_to_portal_room')
    utils.fight_monsters(L {"Archaic Rampart"})
    utils.wait_for_path()
    
    local mob = utils.vars.monster_array[436]
    
    return mob and mob.hpp == 0
end

function procedure.f4_to_portal()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    procedure.block("portal_popup_f4")
    procedure.block("warp_response")
    
    utils.run_path('salvage/f4_to_portal')
    
    if not procedure.wait(10, "portal_popup_f4") then
        return false
    end
    
    return procedure.wait(10, "warp_response")
end

function procedure.f5_to_gilded_doors()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    procedure.wait(2)
    utils.run_path('salvage/f5_to_gilded_doors')
    utils.wait_for_path()
end

function procedure.f5_click_gilded_doors()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    procedure.block("gilded_doors_response")
    
    procedure.poke(504)
    
    return procedure.wait(10, "gilded_doors_response")
end

function procedure.f5_to_gilded_gateway()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.run_path('salvage/f5_to_gilded_gateway')
    utils.wait_for_path()
end

function procedure.f5_to_boss_room()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    procedure.poke(506)
    utils.run_path('salvage/f5_to_boss_room')
    utils.wait_for_path()
end

function procedure.f5_kill_boss()
    if windower.ffxi.get_info().zone ~= 75 then
        procedure.error()
        return 
    end
    
    utils.initiate_combat_routine(('%s_salvage'):format(windower.ffxi.get_player().main_job:lower()))
    
    utils.run_path('salvage/f5_kill_boss')
    utils.fight_monsters(L {"Orthrus Seether"})
    
    procedure.wait(5)
    
    local mob = utils.vars.monster_array[438]
    
    if not mob then
        return false
    end
    
    return mob.hpp == 0
end

function procedure.go_home()
    procedure.block("zone")
    
    coroutine.sleep(5)
    
    windower.send_command('input /equip ring1 "Warp Ring";wait 10;input /item "Warp Ring" <me>')

    local wait_time = math.random(3600,7200) 
    procedure.settings.next = os.time() + wait_time
    
    return procedure.wait(30, "zone")
end

function procedure.wait_hour_to_two_hours()
    procedure.block("wait_hour_to_two_hours")
    
    return procedure.wait(60, "wait_hour_to_two_hours")
end

procedure.triggers = {}

function procedure.triggers.wait_hour_to_two_hours()
    if not procedure.blocked('wait_hour_to_two_hours') then
        return 
    end
    
    if os.time() > procedure.settings.next then
        procedure.iterations = procedure.iterations - 1
        procedure.complete("wait_hour_to_two_hours")
    end
end

--Incoming Chunk
function procedure.triggers.zone(id, data)
    if not procedure.blocked('zone') then
        return 
    end
    
    if id == 0x0a then
        procedure.complete("zone")
    end
end

--Status Change
function procedure.triggers.exit_cutscene(new, old)
    if not procedure.blocked('exit_cutscene') then
        return 
    end
    
    if new == 0 then
        procedure.complete('exit_cutscene')
    end
end

function procedure.triggers.receive_ki(id, data)
    if not procedure.blocked('receive_ki') then
        return 
    end
    
    if id == 0x2a then
        local p = packets.parse('incoming', data)
        
        if p['Param 1'] == 854 then
            procedure.complete('receive_ki')
        end
    end
end

function procedure.triggers.update_currency(id, data)
    if not procedure.blocked('update_currency') then
        return 
    end
    
    if id == 0x113 then
        local p = packets.parse('incoming', data)
        procedure.settings.points = T {p['Assault Points (Leujaoam Sanctum)'], p['Assault Points (M.J.T.G.)'], p['Assault Points (Lebros Cavern)'], p['Assault Points (Periqia)'], p['Assault Points (Ilrusi Atoll)']}
        procedure.complete('update_currency')
    end
end

function procedure.triggers.hp_click(id, data)
    if not procedure.blocked('hp_click') then
        return 
    end
    
    if id == 0x34 then
        local p = packets.parse('incoming', data)
        
        local mob = windower.ffxi.get_mob_by_index(p['NPC Index'])
        
        if mob and S {"Home Point #1", "Home Point #2", "Home Point #3", "Home Point #4", "Home Point #5"}:contains(mob.name) then
            local inject = packets.new("outgoing", 0x5b, {['Target'] = p['NPC'], ['Option Index'] = 0x0002, ['_unknown1'] = 0x006A, ['Target Index'] = p['NPC Index'], ['Zone'] = p['Zone'], ['Menu ID'] = p['Menu ID']})
            packets.inject(inject)
            procedure.complete("hp_click")
            
            return true
        end
    end
end

--Incoming Chunk
function procedure.triggers.click_portal(id, data)
    if not procedure.blocked('portal_response') then
        return 
    end
    
    if id == 0x34 then
        local p = packets.parse('incoming', data)
        
        local mob = windower.ffxi.get_mob_by_index(p['NPC Index'])
        
        if mob and mob.name == "Runic Portal" then
            local inject = packets.new("outgoing", 0x5b, {['Target'] = p['NPC'], ['Option Index'] = 0x06, ['Target Index'] = p['NPC Index'], ['Zone'] = p['Zone'], ['Menu ID'] = p['Menu ID']})
            packets.inject(inject)
            procedure.complete("portal_response")
            return true
        end
    end
end

function procedure.triggers.click_zasshal(id, data)
    if not procedure.blocked('zasshal_response') then
        return 
    end
    
    if id == 0x34 then
        local p = packets.parse('incoming', data)
        
        local mob = windower.ffxi.get_mob_by_index(p['NPC Index'])
        
        if mob and mob.name == "Zasshal" then
            procedure.complete("zasshal_response")
            return true
        end
    end
end

function procedure.triggers.portal_popup(id, data)
    if not procedure.blocked('portal_popup_alzaadal') and not procedure.blocked('portal_popup_f1') and not procedure.blocked('portal_popup_f2') and not procedure.blocked('portal_popup_f3') and not procedure.blocked('portal_popup_f4') then
        return 
    end
    
    if id == 0x32 then
        local p = packets.parse('incoming', data)
        local success = packets.new("outgoing", 0x5b, {['Target'] = utils.info.player.id, ['Option Index'] = 0x01, ['Target Index'] = utils.info.player.index, ['Zone'] = p['Zone'], ['Menu ID'] = p['Menu ID']})
        -- Alzaadal Main -> Bhaflau Entrance
        if procedure.blocked('portal_popup_alzaadal') then
            local inject = packets.new("outgoing", 0x5c, {['X'] = 620, ['Z'] = -0.5, ['Y'] = -303, ['Target ID'] = utils.info.player.id, ['_unknown1'] = 1, ['Zone'] = p['Zone'], ['Menu ID'] = p['Menu ID'], ['Target Index'] = utils.info.player.index, ['_unknown3'] = 0xBF01})
            packets.inject(inject)
            packets.inject(success)
            procedure.complete('portal_popup_alzaadal')
            return true
        elseif procedure.blocked('portal_popup_f1') then
            local inject = packets.new("outgoing", 0x5c, {['X'] = 340, ['Z'] = -0.5, ['Y'] = 140, ['Target ID'] = utils.info.player.id, ['_unknown1'] = 1, ['Zone'] = p['Zone'], ['Menu ID'] = p['Menu ID'], ['Target Index'] = utils.info.player.index, ['_unknown3'] = 0x0001})
            packets.inject(inject)
            packets.inject(success)
            procedure.complete('portal_popup_f1')
            return true
        elseif procedure.blocked('portal_popup_f2') then
            local inject = packets.new("outgoing", 0x5c, {['X'] = -300, ['Z'] = -0.5, ['Y'] = -580, ['Target ID'] = utils.info.player.id, ['_unknown1'] = 1, ['Zone'] = p['Zone'], ['Menu ID'] = p['Menu ID'], ['Target Index'] = utils.info.player.index, ['_unknown3'] = 0xBF01})
            packets.inject(inject)
            packets.inject(success)
            procedure.complete('portal_popup_f2')
            return true
        elseif procedure.blocked('portal_popup_f3') then
            local inject = packets.new("outgoing", 0x5c, {['X'] = -220, ['Z'] = -0.5, ['Y'] = -20, ['Target ID'] = utils.info.player.id, ['_unknown1'] = 1, ['Zone'] = p['Zone'], ['Menu ID'] = p['Menu ID'], ['Target Index'] = utils.info.player.index, ['_unknown3'] = 0x7F01})
            packets.inject(inject)
            packets.inject(success)
            procedure.complete('portal_popup_f3')
            return true
        elseif procedure.blocked('portal_popup_f4') then
            local inject = packets.new("outgoing", 0x5c, {['X'] = -340, ['Z'] = -0.5, ['Y'] = 340, ['Target ID'] = utils.info.player.id, ['_unknown1'] = 1, ['Zone'] = p['Zone'], ['Menu ID'] = p['Menu ID'], ['Target Index'] = utils.info.player.index, ['_unknown3'] = 0xBF01})
            packets.inject(inject)
            packets.inject(success)
            procedure.complete('portal_popup_f4')
            return true
        end
    end
end

function procedure.triggers.click_gilded_gateway(id, data)
    if not procedure.blocked('gilded_gateway_response') then
        return 
    end
    
    if id == 0x34 then
        local p = packets.parse('incoming', data)
        
        local mob = windower.ffxi.get_mob_by_index(p['NPC Index'])
        
        if mob and mob.name == "Gilded Gateway" then
            procedure.complete("gilded_gateway_response")
            return true
        end
    end
end

function procedure.triggers.salvage_reservation(id, data)
    if not procedure.blocked('salvage_reservation') then
        return 
    end
    
    if id == 0xBF and data:byte(7) == 0x04 then
        procedure.complete('salvage_reservation')
    end
end

function procedure.triggers.click_gilded_doors(id, data)
    if not procedure.blocked('gilded_doors_response') then
        return 
    end
    
    if id == 0x32 then
        local p = packets.parse('incoming', data)
        
        local mob = windower.ffxi.get_mob_by_index(p['NPC Index'])
        
        if mob and mob.name == "Gilded Doors" then
            local inject = packets.new("outgoing", 0x5b, {['Target'] = p['NPC'], ['Option Index'] = 0x01, ['Target Index'] = p['NPC Index'], ['Zone'] = p['Zone'], ['Menu ID'] = p['Menu ID']})
            packets.inject(inject)
            procedure.complete("gilded_doors_response")
            return true
        end
    end
end

function procedure.triggers.click_assault_gilded_doors(id, data)
    if not procedure.blocked('assault_gilded_doors_response') then
        return 
    end
    
    if id == 0x32 then
        p = packets.parse('incoming', data)
        local mob = windower.ffxi.get_mob_by_index(p['NPC Index'])
        
        if mob and mob.name == "Gilded Doors" then
            local inject = packets.new("outgoing", 0x5b, {['Target'] = p['NPC'], ['Option Index'] = 0x0000, ['_unknown1'] = 0x0000, ['Automated Message'] = true, ['Target Index'] = p['NPC Index'], ['Zone'] = p['Zone'], ['Menu ID'] = p['Menu ID']})
            local warp = packets.new("outgoing", 0x5c, {['X'] = 180.00001525879, ['Z'] = 0, ['Y'] = -43.581001281738, ['Target ID'] = p['NPC'], ['Target Index'] = p['NPC Index'], ['Zone'] = 0x0048, ['Menu ID'] = 0x0073, ['_unknown3'] = 16129})
            local inject2 = packets.new("outgoing", 0x5b, {['Target'] = p['NPC'], ['Option Index'] = 0x0000, ['_unknown1'] = 0x0000, ['Automated Message'] = false, ['Target Index'] = p['NPC Index'], ['Zone'] = p['Zone'], ['Menu ID'] = p['Menu ID']})
            packets.inject(inject)
            packets.inject(warp)
            packets.inject(inject2)
            procedure.complete("assault_gilded_doors_response")
            return true
        end
    end
end

-- procedure.encumberance = {
-- [0x02] = "Main",
-- [0x03] = "Head",
-- [0x04] = "Body",
-- [0x05] = "Hand",
-- [0x06] = "Leg",
-- [0x07] = "Back",
-- [0x08] = "Ranged",
-- [0x09] = "Earring",
-- [0x0a] = "SJ",
-- [0x0b] = "JA",
-- [0x0c] = "Spells",
-- [0x0d] = "STR",
-- [0x0e] = "DEX",
-- [0x0f] = "VIT",
-- [0x10] = "AGI",
-- [0x11] = "INT",
-- [0x12] = "MND",
-- [0x13] = "CHR",
-- [0x14] = "HP",
-- [0x15] = "MP"

-- }

function procedure.triggers.click_runic_lamp(id, data)
    if not procedure.blocked('runic_lamp_response') then
        return 
    end
    
    if id == 0x34 then
        local p = packets.parse('incoming', data)
        
        if p['Menu ID'] == 500 then
            local bitwork = bit.tobit(p['Menu Parameters']:unpack('I', 1))
            
            bitwork = bit.rshift(bitwork, 2)
            for i = 0x02, 0x15, 1 do
                local check = bit.band(bitwork, 0x01)
                
                if check == 0 then
                    local inject = packets.new("outgoing", 0x5b, {['Target'] = p['NPC'], ['Option Index'] = i, ["_unknown1"] = 0, ['Target Index'] = p['NPC Index'], ["Automated Message"] = true, ["_unknown2"] = 0, ['Zone'] = p['Zone'], ['Menu ID'] = p['Menu ID']})
                    packets.inject(inject)
                --log('Unlocking : ' .. procedure.encumberance[i])
                end
                
                bitwork = bit.rshift(bitwork, 1)
            end
            
            local finish = packets.new("outgoing", 0x5b, {['Target'] = p['NPC'], ['Option Index'] = 0, ["_unknown1"] = 0x4000, ['Target Index'] = p['NPC Index'], ["Automated Message"] = false, ["_unknown2"] = 0, ['Zone'] = p['Zone'], ['Menu ID'] = p['Menu ID']})
            packets.inject(finish)
            
            procedure.complete('runic_lamp_response')
            return true
        end
    end
end

function procedure.triggers.warp_response(id, data)
    if not procedure.blocked('warp_response') then
        return 
    end
    
    if id == 0x65 then
        procedure.complete('warp_response')
    end
end

procedure.register_local_event('incoming chunk', procedure.triggers.click_runic_lamp)
procedure.register_local_event('incoming chunk', procedure.triggers.click_assault_gilded_doors)
procedure.register_local_event('incoming chunk', procedure.triggers.click_gilded_doors)
procedure.register_local_event('incoming chunk', procedure.triggers.salvage_reservation)
procedure.register_local_event('incoming chunk', procedure.triggers.portal_popup)
procedure.register_local_event('incoming chunk', procedure.triggers.click_zasshal)
procedure.register_local_event('incoming chunk', procedure.triggers.click_portal)
procedure.register_local_event('incoming chunk', procedure.triggers.click_gilded_gateway)
procedure.register_local_event('incoming chunk', procedure.triggers.hp_click)
procedure.register_local_event('incoming chunk', procedure.triggers.update_currency)
procedure.register_local_event('incoming chunk', procedure.triggers.receive_ki)
procedure.register_local_event('incoming chunk', procedure.triggers.zone)
procedure.register_local_event('incoming chunk', procedure.triggers.warp_response)
procedure.register_local_event('time change', procedure.triggers.wait_hour_to_two_hours)
procedure.register_local_event('status change', procedure.triggers.exit_cutscene)

--BEGIN

procedure.add("to_aht_urhgan", procedure.to_aht_urhgan, false)
procedure.add("to_zasshal", procedure.to_zasshal, false)
procedure.add("get_permit", procedure.get_permit, false)
procedure.add("to_chamber_of_passage", procedure.to_chamber_of_passage, false)
procedure.add("to_runic_portal", procedure.to_runic_portal, false)
procedure.add("click_portal", procedure.click_portal, false)
procedure.add("to_gilded_doors", procedure.to_gilded_doors, false)
procedure.add("click_gilded_doors", procedure.click_gilded_doors, false)
procedure.add("to_second_gilded_doors", procedure.to_second_gilded_doors, false)
procedure.add("to_salvage_portal", procedure.to_salvage_portal, false)
procedure.add("alzaadal_portal_2", procedure.alzaadal_portal_2, false)
procedure.add("to_salvage_entrance", procedure.to_salvage_entrance, false)
procedure.add("enter_salvage", procedure.enter_salvage, false)
procedure.add("f1_entrance_to_gilded_doors", procedure.f1_entrance_to_gilded_doors, false)
procedure.add("f1_click_gilded_doors_1", procedure.f1_click_gilded_doors_1, false)
procedure.add("f1_gilded_doors_to_runic_lamp", procedure.f1_gilded_doors_to_runic_lamp, false)
procedure.add("click_runic_lamp", procedure.click_runic_lamp, false)
procedure.add("f1_runic_lamp_to_gilded_doors", procedure.f1_runic_lamp_to_gilded_doors, false)
procedure.add("f1_click_gilded_doors_2", procedure.f1_click_gilded_doors_2, false)
procedure.add("f1_gilded_doors_to_wamouracampa", procedure.f1_gilded_doors_to_wamouracampa, false)
procedure.add("equip_primary_weapons", procedure.equip_primary_weapons, false)
procedure.add("f1_kill_wamouracampa", procedure.f1_kill_wamouracampa, false)
procedure.add("f1_wamouracampa_to_wamoura", procedure.f1_wamouracampa_to_wamoura, false)
procedure.add("f1_kill_wamoura", procedure.f1_kill_wamoura, false)
procedure.add("f1_wamoura_to_gilded_doors", procedure.f1_wamoura_to_gilded_doors, false)
procedure.add("f1_click_gilded_doors_3", procedure.f1_click_gilded_doors_3, false)
procedure.add("f1_gilded_doors3_to_gilded_doors4", procedure.f1_gilded_doors3_to_gilded_doors4, false)
procedure.add("f1_click_gilded_doors_4", procedure.f1_click_gilded_doors_4, false)
procedure.add("cast_invisible", procedure.cast_invisible, false)
procedure.add("f1_gilded_doors_to_goblin", procedure.f1_gilded_doors_to_goblin, false)
procedure.add("f1_kill_goblin", procedure.f1_kill_goblin, false)
procedure.add("f1_goblin_to_gilded_doors", procedure.f1_goblin_to_gilded_doors, false)
procedure.add("f1_click_gilded_doors_5", procedure.f1_click_gilded_doors_5, false)
procedure.add("f1_gilded_doors5_to_gilded_doors6", procedure.f1_gilded_doors5_to_gilded_doors6, false)
procedure.add("f1_click_gilded_doors_6", procedure.f1_click_gilded_doors_6, false)
procedure.add("f1_gilded_doors_to_dahak", procedure.f1_gilded_doors_to_dahak, false)
procedure.add("f1_kill_dahak", procedure.f1_kill_dahak, false)
procedure.add("f1_dahak_to_gilded_doors", procedure.f1_dahak_to_gilded_doors, false)
procedure.add("f1_click_gilded_doors_7", procedure.f1_click_gilded_doors_7, false)
procedure.add("f1_gilded_doors_to_portal", procedure.f1_gilded_doors_to_portal, false)
procedure.add("f2_portal_to_gilded_doors", procedure.f2_portal_to_gilded_doors, false)
procedure.add("f2_click_gilded_doors_1", procedure.f2_click_gilded_doors_1, false)
procedure.add("cast_invisible", procedure.cast_invisible, false)
procedure.add("f2_gilded_doors_to_second_troll", procedure.f2_gilded_doors_to_second_troll, false)
procedure.add("f2_kill_second_troll", procedure.f2_kill_second_troll, false)
procedure.add("f2_to_gilded_doors_2", procedure.f2_to_gilded_doors_2, false)
procedure.add("f2_click_gilded_doors_2", procedure.f2_click_gilded_doors_2, false)
procedure.add("f2_gilded_doors2_to_gilded_doors3", procedure.f2_gilded_doors2_to_gilded_doors3, false)
procedure.add("f2_click_gilded_doors_3", procedure.f2_click_gilded_doors_3, false)
procedure.add("f2_gilded_doors_to_portal", procedure.f2_gilded_doors_to_portal, false)
procedure.add("f3_portal_to_hallway", procedure.f3_portal_to_hallway, false)
procedure.add("f3_hallway_to_gilded_doors_1", procedure.f3_hallway_to_gilded_doors_1, false)
procedure.add("f3_click_gilded_doors_1", procedure.f3_click_gilded_doors_1, false)
procedure.add("f3_gilded_doors_to_hallway", procedure.f3_gilded_doors_to_hallway, false)
procedure.add("f3_hallway_to_gilded_doors_2", procedure.f3_hallway_to_gilded_doors_2, false)
procedure.add("f3_click_gilded_doors_2", procedure.f3_click_gilded_doors_2, false)
procedure.add("f3_gilded_doors_to_west_room", procedure.f3_gilded_doors_to_west_room, false)
procedure.add("f3_kill_single_archaic_gears_west", procedure.f3_kill_single_archaic_gears_west, false)
procedure.add("equip_dummy_weapons", procedure.equip_dummy_weapons, false)
procedure.add("f3_to_rampart_west", procedure.f3_to_rampart_west, false)
procedure.add("equip_primary_weapons", procedure.equip_primary_weapons, false)
procedure.add("f3_kill_triple_gears_west", procedure.f3_kill_triple_gears_west, false)
procedure.add("f3_west_room_to_east_room", procedure.f3_west_room_to_east_room, false)
procedure.add("f3_kill_single_archaic_gears_east", procedure.f3_kill_single_archaic_gears_east, false)
procedure.add("equip_dummy_weapons", procedure.equip_dummy_weapons, false)
procedure.add("f3_to_rampart_east", procedure.f3_to_rampart_east, false)
procedure.add("equip_primary_weapons", procedure.equip_primary_weapons, false)
procedure.add("f3_kill_triple_gears_east", procedure.f3_kill_triple_gears_east, false)
procedure.add("f3_rampart_to_gilded_doors", procedure.f3_rampart_to_gilded_doors, false)
procedure.add("f3_click_gilded_doors_3", procedure.f3_click_gilded_doors_3, false)
procedure.add("f3_gilded_doors_to_portal", procedure.f3_gilded_doors_to_portal, false)
procedure.add("f4_portal_to_hallway", procedure.f4_portal_to_hallway, false)
procedure.add("f4_hallway_to_gilded_doors", procedure.f4_hallway_to_gilded_doors, false)
procedure.add("f4_click_gilded_doors_1", procedure.f4_click_gilded_doors_1, false)
procedure.add("f4_enter_gears_room", procedure.f4_enter_gears_room, false)
procedure.add("f4_kill_triple_gears", procedure.f4_kill_triple_gears, false)
procedure.add("equip_dummy_weapons", procedure.equip_dummy_weapons, false)
procedure.add("f4_to_rampart", procedure.f4_to_rampart, false)
procedure.add("equip_primary_weapons", procedure.equip_primary_weapons, false)
procedure.add("f4_to_portal_room", procedure.f4_to_portal_room, false)
procedure.add("f4_to_portal", procedure.f4_to_portal, false)
procedure.add("f5_to_gilded_doors", procedure.f5_to_gilded_doors, false)
procedure.add("f5_click_gilded_doors", procedure.f5_click_gilded_doors, false)
procedure.add("f5_to_gilded_gateway", procedure.f5_to_gilded_gateway, false)
procedure.add("f5_to_boss_room", procedure.f5_to_boss_room, false)
procedure.add("equip_primary_weapons", procedure.equip_primary_weapons, false)
procedure.add("f5_kill_boss", procedure.f5_kill_boss, false)
procedure.add("go_home", procedure.go_home, false)
procedure.add("wait_hour_to_two_hours", procedure.wait_hour_to_two_hours, false)

procedure.settings.next = os.time() + 2000

return procedure
