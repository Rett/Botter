local sequence = {}
sequence.last = {}


local Entrust = {
    type = "Ability",
    action = "Entrust",
    target = "<me>",
    condition = function()
        return not utils.info.Buffs['entrust']
    end

}

local Stoneskin = {

	type = "Magic", 
	action = "Stoneskin",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['stoneskin']
	end,
}


local IndiRefresh = {

	type = "Magic", 
	action = "Indi-Refresh",
	target =
	function()
        for k,v in pairs(utils.info.PartyList.name) do
			if settings.leader and settings.leader:lower() == k then
                return v.id
			end
		end
    end,
	condition =
	function()
		return utils.info.Buffs['entrust']
	end,
}


local IndiHaste = {

	type = "Magic", 
	action = "Indi-Haste",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs[580]
	end,
}

local CureIII = {

	type = "Magic", 
	action = "Cure III",
	target = 
	function()
		local hp = 100
		local t
	
		for k,v in pairs(utils.info.PartyList.name) do
			if not L{"kingofhearts","moru-moru","joachim","apururu","luopan"}:contains(k) then
				local res = math.min(v.hpp,hp)
				t =  (res == v.hpp) and (res < 85) and v.id or t
			end
		end
	
		
		if t then
			return t
		end
	
	end,
	condition =
	function()
		local hp = 100
		local t = false
	
		for k,v in pairs(utils.info.PartyList.name) do
			if not L{"kingofhearts","moru-moru","joachim","apururu","luopan"}:contains(k) then
				local res = math.min(v.hpp,hp)
				t = (res == v.hpp) and (res < 85) and true or t
			end
		end
	
		
		return t
	end,
}

local CureIV = {

	type = "Magic", 
	action = "Cure IV",
	target = 
	function()
		local hp = 100
		local t
	
		for k,v in pairs(utils.info.PartyList.name) do
			if not L{"kingofhearts","moru-moru","joachim","apururu","luopan"}:contains(k) then
				local res = math.min(v.hpp,hp)
				t =  (res == v.hpp) and (res < 75) and v.id or t
			end
		end

		if t then
			return t
		end
	
	end,
	condition =
	function()
		local hp = 100
		local t = false
	
		for k,v in pairs(utils.info.PartyList.name) do
			if not L{"kingofhearts","moru-moru","joachim","apururu","luopan"}:contains(k) then
				local res = math.min(v.hpp,hp)
				t = (res == v.hpp) and (res < 75) and true or t
			end
		end
		
		return t
	end,

}

sequence.routine = L{}

sequence.routine:append(CureIV):append(CureIII):append(IndiHaste):append(Entrust):append(IndiRefresh)

return sequence