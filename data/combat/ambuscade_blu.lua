local sequence = {}

local ErraticFlutter = {

	type = "Magic", 
	action = "Erratic Flutter",
	target = "<me>",
	condition =
	function()
		return utils.info.Engaged and not utils.info.Buffs.haste
	end,

}

local Provoke = {
	type = "Ability",
	action = "Provoke",
	target = "<t>",
	condition = 
	function()
		return utils.info.target and utils.info.target.is_npc and not sequence.last["Provoke"]
	end

}

local Cocoon = {
	type = "Magic", 
	action = "Cocoon",
	target = "<me>",
	condition =
	function()
		return not utils.info.buffs[93]
	end,
}

local Chant = {
	type = "Weaponskill",
	action = "Chant du Cygne",
	target = "<t>",
	condition = 
	function()
		return utils.info.Engaged and utils.info.Target ~= nil
	end
}

local UnbridledLearning = {
	type = "Ability",
	action = "Unbridled Learning",
	target = "<me>",
	condition = 
	function()
		return not utils.info.Buffs['unbridled learning'] and utils.info.player.vitals.mp > 400
	end

}

local BlindingFulgor = {
	type = "Magic",
	action = "Blinding Fulgor",
	target = "<bt>",
	condition = 
	function()
		local mob = windower.ffxi.get_mob_by_target('bt')
		return mob and (sequence.last and ((not sequence.last["Blinding Fulgor"]) or ((os.clock() - sequence.last["Blinding Fulgor"].ts) > 60))) and true
	end

}

local Diffusion = {
	type = "Ability",
	action = "Diffusion",
	target = "<me>",
	condition = 
	function()
		return utils.info.Engaged and utils.info.Buffs['unbridled learning'] and not utils.info.Buffs['diffusion']
	end

}

local Berserk = {
	type = "Ability",
	action = "Berserk",
	target = "<me>",
	condition = 
	function()
		return not utils.info.Buffs['berserk']
	end

}

local Aggressor = {
	type = "Ability",
	action = "Aggressor",
	target = "<me>",
	condition = 
	function()
		return not utils.info.Buffs['aggressor']
	end

}


local MightyGuard = {

	type = "Magic", 
	action = "Mighty Guard",
	target = "<me>",
	condition =
	function()
		return utils.info.Engaged and utils.info.Buffs['unbridled learning'] and utils.info.player.vitals.mp > 400
	end,


}

local BarrierTusk = {

	type = "Magic", 
	action = "Barrier Tusk",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['phalanx']
	end,

}

local NatMed = {

	type = "Magic", 
	action = "Nat. Meditation",
	target = "<me>",
	condition =
	function()
		return not utils.info.buffs['attack boost']
	end,

}

sequence.routine = L{}

sequence.routine:append(Provoke):append(ErraticFlutter):append(Cocoon):append(BarrierTusk):append(Chant):append(BlindingFulgor):append(UnbridledLearning):append(Diffusion):append(MightyGuard):append(NatMed):append(Berserk):append(Aggressor)

return sequence