local sequence = {}

local Waltz = {

	type = "Ability", 
	action = "Curing Waltz III",
	target = "<me>",
	condition =
	function()
		return utils.info.player.hpp < 80 and utils.info.player.tp > 500 and not utils.info.buffs['paralysis']
	end,


}

local HWaltz = {

	type = "Ability", 
	action = "Healing Waltz",
	target = "<me>",
	condition =
	function()
		return utils.info.buffs['paralysis'] and utils.info.player.tp > 200
	end,

}

local Chakra = {

	type = "Ability", 
	action = "Chakra",
	target = "<me>",
	condition =
	function()
		return utils.info.player.hpp < 75
	end,

}


-- local Samba = {

	-- type = "Ability", 
	-- action = "Haste Samba",
	-- target = "<me>",
	-- condition =
	-- function()
		-- return not utils.info.Buffs['haste samba'] and utils.info.player.tp > 550 and utils.info.player.hpp > 80
	-- end,

-- }

local Impetus = {

	type = "Ability", 
	action = "Impetus",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['impetus']
	end,

}

local Dodge = {

	type = "Ability", 
	action = "Dodge",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['dodge']
	end,

}

local CStance = {

	type = "Ability", 
	action = "Counterstance",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['counterstance']
	end,

}


local Sspiral = {
	type = "Weaponskill",
	action = "Shijin Spiral",
	target = "<t>",
	condition = 
	function()
		return utils.info.Engaged and utils.info.Target ~= nil and utils.info.player.hpp > 80
	end
}

-- local Hundred = {
	-- type = "Ability",
	-- action = "Hundred Fists",
	-- target = "<me>",
	-- condition = 
	-- function()
		-- return utils.info.target and L{"Cerberus","Orthrus Seether"}:contains(utils.info.target.name) and utils.info.target.hpp <= 90
	-- end
-- }
local Formless = {
	type = "Ability",
	action = "Formless Strikes",
	target = "<me>",
	condition = 
	function()
		return not utils.info.buffs['paralysis'] and utils.info.target and L{"Cerberus","Orthrus Seether"}:contains(utils.info.target.name) and utils.info.target.hpp <= 55
	end
}

local InnerStr = {
 type = "Ability",
 action = "Inner Strength",
 target = "<me>",
 condition = 
 function()
  return utils.info.target and L{"Cerberus","Orthrus Seether"}:contains(utils.info.target.name) and utils.info.player.hpp < 35
 end
}



sequence.routine = L{}

sequence.routine:append(InnerStr):append(HWaltz):append(Chakra):append(Formless):append(Waltz):append(Impetus):append(Sspiral):append(Dodge):append(CStance)

return sequence