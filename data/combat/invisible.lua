local sequence = {}
sequence.last = {}



local Invisible = {
	type = "Magic",
	action = "Invisible",
	target = "<me>",
	condition = 
	function()
		return not utils.info.buffs.invisible or (sequence.last and ((not sequence.last["Invisible"]) or (os.clock() - sequence.last["Invisible"].ts) > 125))
	end

}

sequence.routine = L{}

sequence.routine:append(Invisible)

return sequence