local sequence = {}


local Entrust = {
    type = "Ability",
    action = "Entrust",
    target = "<me>",
    condition = function()
        return settings.leader and settings.leader ~= "" and not utils.info.Buffs['entrust']
    end

}

local IndiFade = {
    type = "Magic",
    action = "Indi-Fade",
    target =
    function()
        for k,v in pairs(utils.info.PartyList.name) do
			if settings.leader and settings.leader:lower() == k then
                return v.id
			end
		end
    end,
    condition = function()
        return utils.info.Buffs['entrust']
    end
    
}

local IndiWilt = {
    type = "Magic",
    action = "Indi-Wilt",
    target =
    function()
        for k,v in pairs(utils.info.PartyList.name) do
			if settings.leader and settings.leader:lower() == k then
                return v.id
			end
		end
    end,
    condition = function()
        return utils.info.Buffs['entrust']
    end
    
}

local Haste = {

	type = "Magic", 
	action = "Haste",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['haste']
	end,
}

sequence.routine = L{}

sequence.routine:append(Entrust):append(IndiWilt):append(IndiBarrier):append(Haste)

return sequence