local sequence = {}
sequence.last = {}

local IndiAttunement = {

	type = "Magic", 
	action = "Indi-Attunement",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs[556]
	end,
}

local BlazeOfGlory = {
    type = "Ability",
    action = "Blaze of Glory",
    target = "<me>",
    condition =
    function()
        return not utils.info.pet.isvalid
    end

}

local DiaII = {
	type = "Magic",
	action = "Dia II",
	target = "<bt>",
	condition = 
	function()
		local mob = windower.ffxi.get_mob_by_target('bt')
		return mob and (sequence.last and ((not sequence.last["Dia II"]) or ((os.clock() - sequence.last["Dia II"].ts) > 125) or (sequence.last["Dia II"].mob ~= mob.id))) and true
	end

}

local Dematerialize = {
    type = "Ability",
    action = "Dematerialize",
    target = "<me>",
    condition =
    function()
        return utils.info.pet.isvalid
    end
}

local EclipticAttrition = {
    type = "Ability",
    action = "Ecliptic Attrition",
    target = "<me>",
    condition =
    function()
        return utils.info.pet.isvalid
    end
}

local GeoFrailty = {

	type = "Magic", 
	action = "Geo-Frailty",
	target = "<bt>",
	condition =
	function()
		return not utils.info.pet.isvalid and windower.ffxi.get_mob_by_target('bt') and true
	end,
}

local Entrust = {
    type = "Ability",
    action = "Entrust",
    target = "<me>",
    condition = function()
        return settings.leader and settings.leader ~= "" and not utils.info.Buffs['entrust']
    end

}

local IndiFade = {
    type = "Magic",
    action = "Indi-Fade",
    target =
    function()
        for k,v in pairs(utils.info.PartyList.name) do
			if settings.leader and settings.leader:lower() == k then
                return v.id
			end
		end
    end,
    condition = function()
        return utils.info.Buffs['entrust']
    end
    
}

local IndiVex = {
    type = "Magic",
    action = "Indi-Vex",
    target =
    function()
        for k,v in pairs(utils.info.PartyList.name) do
			if settings.leader and settings.leader:lower() == k then
                return v.id
			end
		end
    end,
    condition = function()
        return utils.info.Buffs['entrust']
    end
    
}

local Stoneskin = {

	type = "Magic", 
	action = "Stoneskin",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['stoneskin']
	end,
}

local FullCircle = {
    type = "Ability",
    action = "Full Circle",
    target = "<me>",
    condition = function()
		local mob = windower.ffxi.get_mob_by_target('bt')
        return utils.info.pet.isvalid and mob and (utils.dist({mob.x, mob.y, mob.z}, {utils.info.pet.x, utils.info.pet.y, utils.info.pet.z}) > 12)
    end

}



sequence.routine = L{}

sequence.routine:append(IndiAttunement):append(BlazeOfGlory):append(GeoFrailty):append(Dematerialize):append(EclipticAttrition):append(Entrust):append(IndiVex):append(Stoneskin):append(DiaII):append(FullCircle)

return sequence