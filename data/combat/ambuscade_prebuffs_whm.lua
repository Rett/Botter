local sequence = {}

local LightArts = {
	type = "Action",
	action = "Light Arts",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['addendum: white'] and not utils.info.Buffs['light arts']
	end,
}

local AfflatusSolace = {
	type = "Action",
	action = "Afflatus Solace",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['afflatus solace']
	end,
}

local AddendumWhite = {
	type = "Action",
	action = "Addendum: White",
	target = "<me>",
	condition =
	function()
		return utils.info.Buffs['light arts']
	end,
}

local ShellraV = {

	type = "Magic", 
	action = "Shellra V",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['shell']
	end,
}



local ProtectraV = {

	type = "Magic", 
	action = "Protectra V",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['protect']
	end,
}

local Aquaveil = {

	type = "Magic", 
	action = "Aquaveil",
	target = "<me>",
	condition =
	function()
		return not utils.info.buffs['aquaveil']
	end,
}


local Barfira = {

	type = "Magic", 
	action = "Barfira",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['barfire']
	end,
}

local Haste = {

	type = "Magic", 
	action = "Haste",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['haste']
	end,
}


local Auspice = {

	type = "Magic", 
	action = "Auspice",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['auspice']
	end,
}

local BoostSTR = {

	type = "Magic", 
	action = "Boost-STR",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['str boost']
	end,
}

local Accession = {
	type = "Action",
	action = "Accession",
	target = "<me>",
	condition =
	function()
		return utils.info.Buffs['addendum: white'] or utils.info.Buffs['light arts'] and (sequence.last and ((not sequence.last["Regen IV"]) or (os.clock() - sequence.last["Regen IV"].ts) > 120))
	end,
}

local RegenIV = {

	type = "Magic", 
	action = "Regen IV",
	target = 
	function()
        return utils.info.partylist.name[settings.leader] and settings.leader or "<me>"
	end,
	condition =
	function()		
		return utils.info.buffs['accession']
	end,

}


sequence.routine = L{}

sequence.routine:append(LightArts):append(Accession):append(RegenIV):append(Auspice):append(BoostSTR):append(Haste):append(ShellraV):append(ProtectraV):append(Barfira):append(Aquaveil)

return sequence