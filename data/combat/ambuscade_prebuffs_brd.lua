local sequence = {}

local Paeon = {

	type = "Magic", 
	action = "Army's Paeon",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['minuet'] and not utils.info.Buffs['paeon'] 
	end,
}

local PaeonII = {

	type = "Magic", 
	action = "Army's Paeon II",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['minuet'] and utils.info.Buffs['paeon'] == 1
	end,
}

local PaeonIII = {

	type = "Magic", 
	action = "Army's Paeon III",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['minuet'] and utils.info.Buffs['paeon'] == 2
	end,
}

local March = {

	type = "Magic", 
	action = "Victory March",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['march']
	end,
}

local MinneIV = {

	type = "Magic", 
	action = "Knight's Minne IV",
	target = "<me>",
	condition =
	function()
		return utils.info.Buffs['minne'] == 1
	end,
}

local MinneV = {

	type = "Magic", 
	action = "Knight's Minne V",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['minne']
	end,
}

local Minuet = {

	type = "Magic", 
	action = "Valor Minuet V",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['minuet']
	end,
}

local Troubadour = {

	type = "Ability", 
	action = "Troubadour",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['troubadour']
	end,
}

local Nightingale = {

	type = "Ability", 
	action = "Nightingale",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['nightingale']
	end,
}


sequence.routine = L{}

sequence.routine:append(Troubadour):append(Nightingale):append(Paeon):append(PaeonII):append(PaeonIII):append(Minuet):append(MinneV):append(MinneIV)

return sequence