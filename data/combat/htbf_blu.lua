local sequence = {}

local ErraticFlutter = {

	type = "Magic", 
	action = "Erratic Flutter",
	target = "<me>",
	condition =
	function()
		return utils.info.Engaged and not utils.info.Buffs.haste
	end,

}

local Provoke = {
	type = "Ability",
	action = "Provoke",
	target = "<t>",
	condition = 
	function()
		return utils.info.target and utils.info.target.is_npc and not sequence.last["Provoke"]
	end

}

local Occultation = {
	type = "Magic", 
	action = "Occultation",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['blink']
	end,
}

local Chant = {
	type = "Weaponskill",
	action = "Chant du Cygne",
	target = "<t>",
	condition = 
	function()
		return utils.info.Engaged and utils.info.Target ~= nil
	end
}

local UnbridledLearning = {
	type = "Ability",
	action = "Unbridled Learning",
	target = "<me>",
	condition = 
	function()
		return not utils.info.Buffs['unbridled learning'] and utils.info.player.vitals.mp > 400
	end

}

local Diffusion = {
	type = "Ability",
	action = "Diffusion",
	target = "<me>",
	condition = 
	function()
		return utils.info.Engaged and utils.info.Buffs['unbridled learning'] and not utils.info.Buffs['diffusion']
	end

}

local Berserk = {
	type = "Ability",
	action = "Berserk",
	target = "<me>",
	condition = 
	function()
		return not utils.info.Buffs['berserk']
	end

}

local Aggressor = {
	type = "Ability",
	action = "Aggressor",
	target = "<me>",
	condition = 
	function()
		return not utils.info.Buffs['aggressor']
	end

}


local MightyGuard = {

	type = "Magic", 
	action = "Mighty Guard",
	target = "<me>",
	condition =
	function()
		return utils.info.Engaged and utils.info.Buffs['unbridled learning'] and utils.info.player.vitals.mp > 400
	end,


}

local BarrierTusk = {

	type = "Magic", 
	action = "Barrier Tusk",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['phalanx']
	end,

}

local NatMed = {

	type = "Magic", 
	action = "Nat. Meditation",
	target = "<me>",
	condition =
	function()
		return not utils.info.buffs['attack boost']
	end,

}

sequence.routine = L{}

sequence.routine:append(Provoke):append(ErraticFlutter):append(Occultation):append(BarrierTusk):append(Chant):append(UnbridledLearning):append(Diffusion):append(MightyGuard):append(NatMed):append(Berserk):append(Aggressor)

return sequence