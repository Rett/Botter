local sequence = {}
sequence.last = {}

local IndiHaste = {

	type = "Magic", 
	action = "Indi-Haste",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs[580]
	end,
}


local Entrust = {
    type = "Ability",
    action = "Entrust",
    target = "<me>",
    condition = function()
        return not utils.info.Buffs['entrust']
    end

}

local IndiAcument = {
    type = "Magic",
    action = "Indi-Acumen",
    target =
    function()
        for k,v in pairs(utils.info.PartyList) do
			if type(k) == "string" and not L{"KingOfHearts","Koru-Moru","Joachim","Apururu","_count","Luopan",utils.info.player.name}:contains(k) then
                return v.id
			end
		end
    end,
    condition = function()
        return utils.info.Buffs['entrust']
    end
    
}

local Stoneskin = {

	type = "Magic", 
	action = "Stoneskin",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['stoneskin']
	end,
}

sequence.routine = L{}

sequence.routine:append(IndiHaste):append(Entrust):append(IndiAcumen)

return sequence