local sequence = {}

local Haste = {

	type = "Magic", 
	action = "Haste",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['haste']
	end,
}

local IndiRefresh = {

	type = "Magic", 
	action = "Indi-Refresh",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs[541]
	end,
}

local GeoRefresh = {

	type = "Magic", 
	action = "Geo-Refresh",
	target = "<me>",
	condition =
	function()
		return (sequence.last and ((not sequence.last["Radial Arcana"]) or (os.clock() - sequence.last["Radial Arcana"].ts) > 300) and not utils.info.pet.isvalid) or false
	end,
}

local RadialArcana = {
    type = "Ability",
    action = "Radial Arcana",
    target = "<me>",
    condition =
    function()
        return utils.info.pet.isvalid
    end

}

local Stoneskin = {

	type = "Magic", 
	action = "Stoneskin",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['stoneskin']
	end,
}

sequence.routine = L{}

sequence.routine:append(IndiRefresh):append(Haste):append(Stoneskin):append(GeoRefresh):append(RadialArcana)

return sequence