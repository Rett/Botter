local sequence = {}

local ErraticFlutter = {

	type = "Magic", 
	action = "Erratic Flutter",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs.haste
	end,

}


local Cocoon = {

	type = "Magic", 
	action = "Cocoon",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['defense boost']
	end,
}

local Occultation = {
	type = "Magic", 
	action = "Occultation",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['blink']
	end,
}

local BarrierTusk = {

	type = "Magic", 
	action = "Barrier Tusk",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['phalanx']
	end,

}

local KoruMoru = {
	
	type = "Magic",
	action = "Koru-Moru",
	target = "<me>",
	condition = 
	function()
		return utils.info.NumberOfEnemies == 0 and not utils.info.PartyList.name['Koru-Moru'] and utils.info.PartyList.count < 6
	end

}

local Apururu = {
	
	type = "Magic",
	action = "Apururu (UC)",
	target = "<me>",
	condition = 
	function()
		return utils.info.NumberOfEnemies == 0 and not utils.info.PartyList.name['Apururu'] and utils.info.PartyList.count < 6
	end

}

local Joachim = {
	
	type = "Magic",
	action = "Joachim",
	target = "<me>",
	condition = 
	function()
		return utils.info.NumberOfEnemies == 0 and not utils.info.PartyList.name['Joachim'] and utils.info.PartyList.count < 6
	end

}

local King = {
	
	type = "Magic",
	action = "King of Hearts",
	target = "<me>",
	condition = 
	function()
		return utils.info.NumberOfEnemies == 0 and not utils.info.PartyList.name['KingOfHearts'] and utils.info.PartyList.count < 6
	end

}

sequence.routine = L{}


sequence.routine:append(Occultation):append(ErraticFlutter):append(BarrierTusk):append(Apururu):append(Joachim):append(KoruMoru):append(King)

return sequence