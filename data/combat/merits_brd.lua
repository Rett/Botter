local sequence = {}
sequence.last = {}



local Stoneskin = {

	type = "Magic", 
	action = "Stoneskin",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['stoneskin']
	end,
}

local Troubadour = {

	type = "Ability", 
	action = "Troubadour",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['troubadour']
	end,
}

local Nightingale = {

	type = "Ability", 
	action = "Nightingale",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['nightingale']
	end,
}

local Ballad2 = {

	type = "Magic", 
	action = "Mage's Ballad II",
	target = "<me>",
	condition =
	function()
		return utils.info.buffs.nightingale and utils.info.Buffs['ballad'] == 1
	end,
}

local Ballad3 = {

	type = "Magic", 
	action = "Mage's Ballad III",
	target = "<me>",
	condition =
	function()
		return utils.info.buffs.nightingale and not utils.info.Buffs['ballad']
	end,
}

local CureIII = {

	type = "Magic", 
	action = "Cure III",
	target = 
	function()
		local hp = 100
		local t
	
		for k,v in pairs(utils.info.PartyList.name) do
			if not L{"kingofhearts","moru-moru","joachim","apururu","luopan"}:contains(k) then
				local res = math.min(v.hpp,hp)
				t =  (res == v.hpp) and (res < 85) and v.id or t
			end
		end
	
		
		if t then
			return t
		end
	
	end,
	condition =
	function()
		local hp = 100
		local t = false
	
		for k,v in pairs(utils.info.PartyList.name) do
			if not L{"kingofhearts","moru-moru","joachim","apururu","luopan"}:contains(k) then
				local res = math.min(v.hpp,hp)
				t = (res == v.hpp) and (res < 85) and true or t
			end
		end
	
		
		return t
	end,
}

local CureIV = {

	type = "Magic", 
	action = "Cure IV",
	target = 
	function()
		local hp = 100
		local t
	
		for k,v in pairs(utils.info.PartyList.name) do
			if not L{"kingofhearts","moru-moru","joachim","apururu","luopan"}:contains(k) then
				local res = math.min(v.hpp,hp)
				t =  (res == v.hpp) and (res < 75) and v.id or t
			end
		end

		if t then
			return t
		end
	
	end,
	condition =
	function()
		local hp = 100
		local t = false
	
		for k,v in pairs(utils.info.PartyList.name) do
			if not L{"kingofhearts","moru-moru","joachim","apururu","luopan"}:contains(k) then
				local res = math.min(v.hpp,hp)
				t = (res == v.hpp) and (res < 75) and true or t
			end
		end
		
		return t
	end,

}


sequence.routine = L{}

sequence.routine:append(CureIV):append(CureIII):append(Troubadour):append(Nightingale):append(Ballad3):append(Ballad2)

return sequence