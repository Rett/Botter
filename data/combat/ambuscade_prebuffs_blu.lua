local sequence = {}

local ErraticFlutter = {

	type = "Magic", 
	action = "Erratic Flutter",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs.haste
	end,

}


local Cocoon = {

	type = "Magic", 
	action = "Cocoon",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['defense boost']
	end,
}

local Occultation = {
	type = "Magic", 
	action = "Occultation",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['blink']
	end,
}

local BarrierTusk = {

	type = "Magic", 
	action = "Barrier Tusk",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['phalanx']
	end,

}

local MagicBarrier = {

	type = "Magic", 
	action = "Magic Barrier",
	target = "<me>",
	condition =
	function()
		return not utils.info.buffs['magic shield']
	end,

}

local UnbridledLearning = {
	type = "Ability",
	action = "Unbridled Learning",
	target = "<me>",
	condition = 
	function()
		return not utils.info.Buffs['unbridled learning'] and utils.info.player.vitals.mp > 400
	end

}

local Diffusion = {
	type = "Ability",
	action = "Diffusion",
	target = "<me>",
	condition = 
	function()
		return utils.info.Buffs['unbridled learning'] and not utils.info.Buffs['diffusion']
	end

}
local MightyGuard = {

	type = "Magic", 
	action = "Mighty Guard",
	target = "<me>",
	condition =
	function()
		return utils.info.Buffs['unbridled learning'] and utils.info.player.vitals.mp > 400
	end,


}

local Cocoon = {
	type = "Magic", 
	action = "Cocoon",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['defense boost']
	end,
}

local Apururu = {
	
	type = "Magic",
	action = "Apururu (UC)",
	target = "<me>",
	condition = 
	function()
		return utils.info.NumberOfEnemies == 0 and not utils.info.PartyList.name['Apururu'] and utils.info.PartyList.count < 6
	end

}


sequence.routine = L{}


sequence.routine:append(Cocoon):append(ErraticFlutter):append(BarrierTusk):append(UnbridledLearning):append(Diffusion):append(MightyGuard):append(MagicBarrier):append(Apururu)

return sequence