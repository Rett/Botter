local sequence = {}
sequence.last = {}



local DiaII = {
	type = "Magic",
	action = "Dia II",
	target = "<bt>",
	condition = 
	function()
		return (sequence.last and ((not sequence.last["Dia II"]) or (os.clock() - sequence.last["Dia II"].ts) > 125)) and windower.ffxi.get_mob_by_target('bt') and true
	end

}

local Elegy = {
	type = "Magic",
	action = "Carnage Elegy",
	target = "<bt>",
	condition = 
	function()
		return (sequence.last and ((not sequence.last["Carnage Elegy"]) or (os.clock() - sequence.last["Carnage Elegy"].ts) > 125)) and windower.ffxi.get_mob_by_target('bt') and true
	end

}

local Stoneskin = {

	type = "Magic", 
	action = "Stoneskin",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['stoneskin']
	end,
}



sequence.routine = L{}

sequence.routine:append(DiaII):append(Elegy):append(Stoneskin)

return sequence