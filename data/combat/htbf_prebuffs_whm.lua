local sequence = {}

local LightArts = {
	type = "Action",
	action = "Light Arts",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['addendum: white'] and not utils.info.Buffs['light arts']
	end,
}

local AfflatusSolace = {
	type = "Action",
	action = "Afflatus Solace",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['afflatus solace']
	end,
}

local AddendumWhite = {
	type = "Action",
	action = "Addendum: White",
	target = "<me>",
	condition =
	function()
		return utils.info.Buffs['light arts']
	end,
}

local ShellraV = {

	type = "Magic", 
	action = "Shellra V",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['shell']
	end,
}

local ProtectraV = {

	type = "Magic", 
	action = "Protectra V",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['protect']
	end,
}

local Barwatera = {

	type = "Magic", 
	action = "Barwatera",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['barwater']
	end,
}

local Haste = {

	type = "Magic", 
	action = "Haste",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['haste']
	end,
}


local Auspice = {

	type = "Magic", 
	action = "Auspice",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['auspice']
	end,
}

local BoostSTR = {

	type = "Magic", 
	action = "Boost-STR",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['str boost']
	end,
}



sequence.routine = L{}

sequence.routine:append(LightArts):append(AfflatusSolace):append(AddendumWhite):append(Auspice):append(BoostSTR):append(Haste):append(ShellraV):append(ProtectraV):append(Barwatera)

return sequence