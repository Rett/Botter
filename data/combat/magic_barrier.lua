local sequence = {}

local Diffusion = {
	type = "Ability",
	action = "Diffusion",
	target = "<me>",
	condition = 
	function()
		return not utils.info.Buffs['diffusion']
	end

}

local MagicBarrier = {

	type = "Magic", 
	action = "Magic Barrier",
	target = "<me>",
	condition =
	function()
		return utils.info.buffs['diffusion']
	end,

}

sequence.routine = L{}


sequence.routine:append(Diffusion):append(MagicBarrier)

return sequence