local sequence = {}

local MagicFruit = {

	type = "Magic", 
	action = "Magic Fruit",
	target = "<me>",
	condition =
	function()
		return utils.info.player.hpp < 75
	end,


}

local ErraticFlutter = {

	type = "Magic", 
	action = "Erratic Flutter",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs.haste
	end,

}

local Cocoon = {

	type = "Magic", 
	action = "Cocoon",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['defense boost']
	end,


}

local Requiescat = {
	type = "Weaponskill",
	action = "Requiescat",
	target = "<t>",
	condition = 
	function()
		return utils.info.Engaged and utils.info.Target and L{"Freke","Cerberus","Orthrus Seether"}:contains(utils.info.Target.name)
	end

}

local Chant = {
	type = "Weaponskill",
	action = "Chant du Cygne",
	target = "<t>",
	condition = 
	function()
		return utils.info.Engaged and utils.info.Target ~= nil
	end
}

local UnbridledLearning = {
	type = "Ability",
	action = "Unbridled Learning",
	target = "<me>",
	condition = 
	function()
		return not utils.info.Buffs['unbridled learning'] and utils.info.player.vitals.mp > 400
	end

}

local Diffusion = {
	type = "Ability",
	action = "Diffusion",
	target = "<me>",
	condition = 
	function()
		return utils.info.Buffs['unbridled learning'] and not utils.info.Buffs['diffusion']
	end

}

local TenebralCrush = {
	type = "Magic",
	action = "Tenebral Crush",
	target = "<t>",
	condition = 
	function()
		return utils.info.Engaged and utils.info.Target and utils.info.NumberOfEnemies >= 4
	end

}

local MightyGuard = {

	type = "Magic", 
	action = "Mighty Guard",
	target = "<me>",
	condition =
	function()
		return utils.info.Buffs['unbridled learning'] and utils.info.player.vitals.mp > 400
	end,


}

sequence.routine = L{}

sequence.routine:append(MagicFruit):append(ErraticFlutter):append(TenebralCrush):append(Requiescat):append(Chant):append(UnbridledLearning):append(Diffusion):append(MightyGuard)

return sequence