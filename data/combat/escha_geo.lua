local sequence = {}
sequence.last = {}

local IndiHaste = {

	type = "Magic", 
	action = "Indi-Haste",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs[580]
	end,
}

local BlazeOfGlory = {
    type = "Ability",
    action = "Blaze of Glory",
    target = "<me>",
    condition =
    function()
        return not utils.info.pet.isvalid  and  windower.ffxi.get_mob_by_target('bt') and true
    end

}

local DiaII = {
	type = "Magic",
	action = "Dia II",
	target = "<bt>",
	condition = 
	function()
		return (sequence.last and ((not sequence.last["Dia II"]) or (os.clock() - sequence.last["Dia II"].ts) > 125)) and  windower.ffxi.get_mob_by_target('bt') and true
	end

}

local Dematerialize = {
    type = "Ability",
    action = "Dematerialize",
    target = "<me>",
    condition =
    function()
        return utils.info.pet.isvalid
    end
}

local EclipticAttrition = {
    type = "Ability",
    action = "Ecliptic Attrition",
    target = "<me>",
    condition =
    function()
        return utils.info.pet.isvalid
    end
}

local GeoFrailty = {

	type = "Magic", 
	action = "Geo-Frailty",
	target = "<bt>",
	condition =
	function()
		return utils.info.buffs.confrontation and not utils.info.pet.isvalid and windower.ffxi.get_mob_by_target('bt') and true
	end,
}

local Entrust = {
    type = "Ability",
    action = "Entrust",
    target = "<me>",
    condition = function()
        return utils.info.buffs.confrontation and settings.leader and utils.info.partylist.name[settings.leader:lower()] and not utils.info.buffs['entrust']
    end

}

local IndiTorpor = {
    type = "Magic",
    action = "Indi-Torpor",
    target =
    function()
		return utils.info.partylist.name[settings.leader:lower()] and utils.info.partylist.name[settings.leader:lower()].id or false
    end,
    condition = function()
        return utils.info.Buffs['entrust']
    end
    
}

local Stoneskin = {

	type = "Magic", 
	action = "Stoneskin",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['stoneskin']
	end,
}

local CureIII = {

	type = "Magic", 
	action = "Cure III",
	target = 
	function()
		local hp = 100
		local t
	
		for k,v in pairs(utils.info.PartyList.name) do
			if not L{"kingofhearts","moru-moru","joachim","apururu","luopan"}:contains(k) then
				local res = math.min(v.hpp,hp)
				t =  (res == v.hpp) and (res < 85) and v.id or t
			end
		end
	
		
		if t then
			return t
		end
	
	end,
	condition =
	function()
		local hp = 100
		local t = false
	
		for k,v in pairs(utils.info.PartyList.name) do
			if not L{"kingofhearts","moru-moru","joachim","apururu","luopan"}:contains(k) then
				local res = math.min(v.hpp,hp)
				t = (res == v.hpp) and (res < 85) and true or t
			end
		end
	
		
		return t
	end,
}

local CureIV = {

	type = "Magic", 
	action = "Cure IV",
	target = 
	function()
		local hp = 100
		local t
	
		for k,v in pairs(utils.info.PartyList.name) do
			if not L{"kingofhearts","moru-moru","joachim","apururu","luopan"}:contains(k) then
				local res = math.min(v.hpp,hp)
				t =  (res == v.hpp) and (res < 75) and v.id or t
			end
		end

		if t then
			return t
		end
	
	end,
	condition =
	function()
		local hp = 100
		local t = false
	
		for k,v in pairs(utils.info.PartyList.name) do
			if not L{"kingofhearts","moru-moru","joachim","apururu","luopan"}:contains(k) then
				local res = math.min(v.hpp,hp)
				t = (res == v.hpp) and (res < 75) and true or t
			end
		end
		
		return t
	end,

}



sequence.routine = L{}

sequence.routine:append(IndiHaste):append(BlazeOfGlory):append(GeoFrailty):append(Dematerialize):append(EclipticAttrition):append(Entrust):append(IndiTorpor):append(Stoneskin):append(DiaII):append(CureIV):append(CureIII)

return sequence