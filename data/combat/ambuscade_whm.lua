local sequence = {}

function sequence.in_party(index)
    return utils.info.partylist.index[index] and true

end

local CuragaIII = {

	type = "Magic", 
	action = "Curaga III",
	target = 
	function()
		local t = L{}

		for k,v in pairs(utils.info.PartyList.name) do
			if not L{"Luopan"}:contains(k) then
				if v.hpp < 75 and utils.aoe_targets(v.index,10,sequence.in_party) >= 2 then
					t:append(v.id)
				end
			end
		end
		
		if t:length() >= 2 then
			return t[1]
		end
	
	end,
	condition =
	function()
		local hp = 100
		local t = L{}
	
		for k,v in pairs(utils.info.PartyList.name) do
			if not L{"Luopan"}:contains(k) then
				if v.hpp < 75 and utils.aoe_targets(v.index,10,sequence.in_party) >= 2 then
					t:append(v.id)
				end
			end
		end
		
		return t:length() >= 2
	end,
}

local CuragaIV = {

	type = "Magic", 
	action = "Curaga IV",
	target = 
	function()
		local t = L{}
	
		for k,v in pairs(utils.info.PartyList.name) do
			if not L{"Luopan"}:contains(k) then
				if v.hpp < 65 and utils.aoe_targets(v.index,10,sequence.in_party) >= 2 then
					t:append(v.id)
				end
			end
		end
		
		if t:length() >= 2 then
			return t[1]
		end
	
	end,
	condition =
	function()
		local t = L{}
	
		for k,v in pairs(utils.info.PartyList.name) do
			if not L{"Luopan"}:contains(k) then
				if v.hpp < 65 and utils.aoe_targets(v.index,10,sequence.in_party) >= 2 then
					t:append(v.id)
				end
			end
		end
		
		return t:length() >= 2
	end,
}

local CuragaV = {

	type = "Magic", 
	action = "Curaga V",
	target = 
	function()
		local t = L{}
	
		for k,v in pairs(utils.info.PartyList.name) do
			if not L{"Luopan"}:contains(k) then
				if v.hpp < 45 and utils.aoe_targets(v.index,10,sequence.in_party) >= 2 then
					t:append(v.id)
				end
			end
		end
		
		if t:length() >= 2 then
			return t[1]
		end
	
	end,
	condition =
	function()
		local t = L{}
	
		for k,v in pairs(utils.info.PartyList.name) do
			if not L{"Luopan"}:contains(k) then
				if v.hpp < 45 and utils.aoe_targets(v.index,10,sequence.in_party) >= 2 then
					t:append(v.id)
				end
			end
		end
		
		return t:length() >= 2
	end,
}


local CureIII = {

	type = "Magic", 
	action = "Cure III",
	target = 
	function()
		local hp = 100
		local t
	
		for k,v in pairs(utils.info.PartyList.name) do
			if not L{"kingofhearts","moru-moru","joachim","apururu","luopan"}:contains(k) then
				local res = math.min(v.hpp,hp)
				t =  (res == v.hpp) and (res < 85) and v.id or t
			end
		end
	
		
		if t then
			return t
		end
	
	end,
	condition =
	function()
		local hp = 100
		local t = false
	
		for k,v in pairs(utils.info.PartyList.name) do
			if not L{"kingofhearts","moru-moru","joachim","apururu","luopan"}:contains(k) then
				local res = math.min(v.hpp,hp)
				t = (res == v.hpp) and (res < 85) and true or t
			end
		end
	
		
		return t
	end,
}

local CureIV = {

	type = "Magic", 
	action = "Cure IV",
	target = 
	function()
		local hp = 100
		local t
	
		for k,v in pairs(utils.info.PartyList.name) do
			if not L{"kingofhearts","moru-moru","joachim","apururu","luopan"}:contains(k) then
				local res = math.min(v.hpp,hp)
				t =  (res == v.hpp) and (res < 75) and v.id or t
			end
		end

		if t then
			return t
		end
	
	end,
	condition =
	function()
		local hp = 100
		local t = false
	
		for k,v in pairs(utils.info.PartyList.name) do
			if not L{"kingofhearts","moru-moru","joachim","apururu","luopan"}:contains(k) then
				local res = math.min(v.hpp,hp)
				t = (res == v.hpp) and (res < 75) and true or t
			end
		end
		
		return t
	end,

}

local CureV = {

	type = "Magic", 
	action = "Cure V",
	target = 
	function()
		local hp = 100
		local t
	
		for k,v in pairs(utils.info.PartyList.name) do
			if not L{"kingofhearts","moru-moru","joachim","apururu","luopan"}:contains(k) then
				local res = math.min(v.hpp,hp)
				t =  (res == v.hpp) and (res < 60) and v.id or t
			end
		end
		
		if t then
			return t
		end
	
	end,
	condition =
	function()
		local hp = 100
		local t = false
	
		for k,v in pairs(utils.info.PartyList.name) do
			if not L{"kingofhearts","moru-moru","joachim","apururu","luopan"}:contains(k) then
				local res = math.min(v.hpp,hp)
				t = (res == v.hpp) and (res < 60) and true or t
			end
		end
		
		return t
	end,

}

local LightArts = {
	type = "Action",
	action = "Light Arts",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['addendum: white'] and not utils.info.Buffs['light arts']
	end,
}

local Accession = {
	type = "Action",
	action = "Accession",
	target = "<me>",
	condition =
	function()
		return utils.info.Buffs['addendum: white'] or utils.info.Buffs['light arts'] and (sequence.last and ((not sequence.last["Regen IV"]) or (os.clock() - sequence.last["Regen IV"].ts) > 120))
	end,
}

local RegenIV = {

	type = "Magic", 
	action = "Regen IV",
	target = 
	function()
        return utils.info.partylist.name[settings.leader] and settings.leader or "<me>"
	end,
	condition =
	function()		
		return utils.info.buffs['accession']
	end,

}


local AfflatusSolace = {
	type = "Action",
	action = "Afflatus Solace",
	target = "<me>",
	condition =
	function()
		local mob = windower.ffxi.get_mob_by_index(99)
		return mob and mob.hpp == 0 and not utils.info.Buffs['afflatus solace']
	end,
}



sequence.routine = L{}

sequence.routine:append(CuragaV):append(CuragaIV):append(CuragaIII):append(CureV):append(CureIV):append(CureIII):append(Accession):append(RegenIV):append(LightArts):append(AfflatusSolace)

return sequence