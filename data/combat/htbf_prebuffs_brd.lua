local sequence = {}

local Paeon = {

	type = "Magic", 
	action = "Army's Paeon",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['minuet'] and not utils.info.Buffs['paeon'] 
	end,
}

local PaeonII = {

	type = "Magic", 
	action = "Army's Paeon II",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['minuet'] and utils.info.Buffs['paeon'] == 1
	end,
}

local PaeonIII = {

	type = "Magic", 
	action = "Army's Paeon III",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['minuet'] and utils.info.Buffs['paeon'] == 2
	end,
}

local March = {

	type = "Magic", 
	action = "Victory March",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['march']
	end,
}

local WaterCarol = {

	type = "Magic", 
	action = "Water Carol",
	target = "<me>",
	condition =
	function()
		return utils.info.Buffs['carol'] == 1
	end,
}

local WaterCarolII = {

	type = "Magic", 
	action = "Water Carol II",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['carol']
	end,
}

local Minuet = {

	type = "Magic", 
	action = "Valor Minuet V",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['minuet']
	end,
}

local Troubadour = {

	type = "Ability", 
	action = "Troubadour",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['troubadour']
	end,
}

local Nightingale = {

	type = "Ability", 
	action = "Nightingale",
	target = "<me>",
	condition =
	function()
		return not utils.info.Buffs['nightingale']
	end,
}


sequence.routine = L{}

sequence.routine:append(Troubadour):append(Nightingale):append(Paeon):append(PaeonII):append(PaeonIII):append(Minuet):append(WaterCarolII):append(WaterCarol)

return sequence