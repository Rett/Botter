local sequence = {}
sequence.last = {}

local MagicFruit = {

	type = "Magic", 
	action = "Magic Fruit",
    delay = 4,
	target = "<me>",
	condition =
	function()
		return utils.info.NumberOfEnemies == 0 and utils.info.player.hpp < 75
	end,


}

local ErraticFlutter = {

	type = "Magic", 
	action = "Erratic Flutter",
	target = "<me>",
	condition =
	function()
		return utils.info.NumberOfEnemies == 0 and not utils.info.buffs[33] or (sequence.last and ((not sequence.last["Erratic Flutter"]) or (os.clock() - sequence.last["Erratic Flutter"].ts) > 180))
	end

}

local Cocoon = {

	type = "Magic", 
	action = "Cocoon",
    delay = 4,
	target = "<me>",
	condition =
	function()
		return utils.info.NumberOfEnemies == 0 and not utils.info.Buffs['defense boost']
	end,


}

local BarrierTusk = {

	type = "Magic", 
	action = "Barrier Tusk",
    delay = 4,
	target = "<me>",
	condition =
	function()
		return utils.info.NumberOfEnemies == 0 and not utils.info.Buffs['phalanx']
	end,


}


local UnbridledLearning = {
	type = "Ability",
	action = "Unbridled Learning",
	target = "<me>",
	condition = 
	function()
		return utils.info.NumberOfEnemies == 0 and (sequence.last and ((not sequence.last["Carcharian Verve"]) or (os.clock() - sequence.last["Carcharian Verve"].ts) > 400)) and not utils.info.Buffs['unbridled learning']
	end

}

local Diffusion = {
	type = "Ability",
	action = "Diffusion",
	target = "<me>",
	condition = 
	function()
		return utils.info.Buffs['unbridled learning'] and not utils.info.Buffs['diffusion']
	end

}

local MagicHammer = {
	type = "Magic",
	action = "Magic Hammer",
    delay = 4,
	target = "<t>",
	condition = 
	function()
		return utils.info.Target and utils.info.Target.is_npc and math.sqrt(utils.info.Target.distance) < 6 and utils.info.player.mpp <= 50 and L{"Territorial Mantis","Rampaging Beetle","Eschan Phuabo"}:contains(utils.info.Target.name)
	end

}

local TenebralCrush = {
	type = "Magic",
	action = "Tenebral Crush",
	target = "<t>",
    delay = 4,
	condition = 
	function()
		return utils.info.Target and utils.info.Target.is_npc and math.sqrt(utils.info.Target.distance) < 6 and utils.info.Target.hpp >= 50 and utils.aoe_targets(utils.info.player.index,13) >= 2
	end

}

local SpectralFloe = {
	type = "Magic",
	action = "Spectral Floe",
	target = "<t>",
    delay = 4,
	condition = 
	function()
		return utils.info.Target and utils.info.Target.is_npc and math.sqrt(utils.info.Target.distance) < 6 and utils.info.Target.hpp >= 25
	end

}

local AnvilLightning = {
	type = "Magic",
	action = "Anvil Lightning",
    delay = 4,
	target = "<t>",
	condition = 
	function()
		return utils.info.Target and utils.info.Target.is_npc and math.sqrt(utils.info.Target.distance) < 5 and utils.info.Target.hpp >= 25
	end

}

local Subduction = {
	type = "Magic",
	action = "Subduction",
    delay = 3,
	target = "<t>",
	condition = 
	function()
		return utils.info.Target and utils.info.Target.is_npc and math.sqrt(utils.info.Target.distance) < 5
	end

}

local MightyGuard = {

	type = "Magic", 
	action = "Mighty Guard",
	target = "<me>",
	condition =
	function()
		return utils.info.Buffs['unbridled learning'] and utils.info.player.vitals.mp > 400
	end,


}

local CarcharianVerve = {

	type = "Magic", 
	action = "Carcharian Verve",
	target = "<me>",
	condition =
	function()
		return utils.info.Buffs['unbridled learning']
	end,


}

local SanguineBlade = {

	type = "Weaponskill",
	action = "Sanguine Blade",
	target = "<t>",
	condition = 
	function()
		return utils.info.engaged and utils.info.player.hpp < 60

}

sequence.routine = L{}

sequence.routine:append(MagicFruit):append(UnbridledLearning):append(CarcharianVerve):append(ErraticFlutter):append(BarrierTusk):append(Cocoon):append(MagicHammer):append(TenebralCrush):append(SpectralFloe):append(AnvilLightning):append(Subduction):append(SanguineBlade)

return sequence