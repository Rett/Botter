---------------------------------------------------------------------------
---------------------------------------------------------------------------
---------------------------------------------------------------------------
---------------------------------------------------------------------------

function get_base()

	local f, err = dofile(windower.addon_path .. 'data/base.lua')
	
	if f and not err then
		return f
	else
		error("Could not locate the base procedure file.")
		return {}
	end


end

procedure = get_base()

if not procedure then return false end

---------------------------------------------------------------------------
---------------------------------------------------------------------------
---------------- CUSTOM DEFINED SECTION: EDIT BELOW -----------------------
---------------------------------------------------------------------------
---------------------------------------------------------------------------

packets = require('packets')
packets.raw_fields.outgoing[0x05C] = L{
    {ctype='float',             label='X'},                                     -- 04
    {ctype='float',             label='Z'},                                     -- 08
    {ctype='float',             label='Y'},                                     -- 0C
    {ctype='unsigned int',      label='Target ID',          fn=id},             -- 10   NPC that you are requesting a warp from
    {ctype='unsigned int',      label='_unknown1'},                             -- 14   01 00 00 00 observed
    {ctype='unsigned short',    label='Zone'},                               	-- 18   Likely contains information about the particular warp being requested, like menu ID
    {ctype='unsigned short',    label='Menu ID'},                             	-- 1A   Likely contains information about the particular warp being requested, like menu ID
	{ctype='unsigned short',    label='Target Index',       fn=index},          -- 1C
    {ctype='unsigned short',    label='_unknown3'},                             -- 1E   Not zone ID
}

procedure._repeat = false
procedure.settings = {}
procedure.name = "HTBF (Leviathan)"

if not settings.leader or settings.leader == "" then
    notice("This routine requires that you initialize the setting 'leader' to a character that will be engaged.")
    notice("Do so by typing //coordinator set leader <name>!")
    return false
else
    notice(" ===== Settings =====")
    notice(" * Leader : %s":format(settings.leader))
	notice(" * Leeches : %s":format((settings.htbf.leeches and settings.htbf.leeches:concat(', ')) or "none"))
end


procedure.bcnm = 
{
	difficulty = 0x2, -- VE is 0xA, E is ??, normal is 0x6, D is 0x04, VD is ??
	attempt = 1,
	zone_id = 211,
	warp_request_offset = 0x40,
	warp_array = {
		[1] = {['X'] = 497.25601196289, ['Y'] = -432.833007815, ['Z'] = 55.549003601074, ['_unknown3'] = 0x9701},
		[2] = {['X'] = 17.19600105286, ['Y'] = -32.954002380371, ['Z'] = -4.4409999847412, ['_unknown3'] = 0x9801},
		[3] = {['X'] = -382.79000854492, ['Y'] = 447.09201049805, ['Z'] = -64.444999694824, ['_unknown3'] = 0x9701},
		},
    indexes = {[1] = 29, [2] = 34, [3] = 39},
	target_id = 17641518,
	target_index = 46,
	menu_id = 0x7D00
	
}

function procedure.change_sub_hack()
        
    procedure.wait(5)

	if not settings.htbf.leeches or not settings.htbf.leeches:map(string.lower):contains(utils.info.player.name:lower()) then
		if L{53,247,248,249,252}:contains(windower.ffxi.get_info().zone) then
			local job = windower.ffxi.get_player().main_job:lower()

			local mnk = packets.new('outgoing', 0x100, { ['Sub Job'] = 0x02 })
			packets.inject(mnk)
			
			if job == "blu" then
				local war = packets.new('outgoing', 0x100, { ['Sub Job'] = 0x01 })
				packets.inject(war)
			elseif job == "whm" then
				local sch = packets.new('outgoing', 0x100, { ['Sub Job'] = 0x14 })
				packets.inject(sch)
			elseif L{"geo","brd"}:contains(job) then
				local rdm = packets.new('outgoing', 0x100, { ['Sub Job'] = 0x05 })
				packets.inject(rdm)
			end
		end
	end


end

if windower.ffxi.get_player().main_job:lower() == "blu" then
        windower.send_command('lua l bloop; wait 2; blu load solo')
end


function procedure.travel_south_sandoria()

	if windower.ffxi.get_info().zone ~= 230 then
		
        procedure.wait(utils.calculate_time_offset(3))
		procedure.block("hp_click_ss")
		procedure.block("zone")
		
		for hp in L{"Home Point #1", "Home Point #2", "Home Point #3", "Home Point #4", "Home Point #5"}:it() do
			local mob = windower.ffxi.get_mob_by_name(hp)
			if mob and math.sqrt(mob.distance) < 6 then
				procedure.poke(mob.index)
				break
			end
		end
				
		if not procedure.wait(10, "hp_click_ss") then return false end
		
		return procedure.wait(20, "zone")
	else
		return true
	end

end


function procedure.to_rolandienne()

    utils.run_path('htbf/to_rolandienne')
    utils.wait_for_path()

end

function procedure.buy_warp_scroll()

    procedure.block("rolandienne_response")
    
    procedure.wait(utils.calculate_time_offset(.75))
    procedure.poke("Rolandienne")
    
    return procedure.wait(10, "rolandienne_response")

end

function procedure.to_homepoint_ss()

    utils.run_path('htbf/to_homepoint_ss')
    utils.wait_for_path()

end


function procedure.travel_north_sandoria()

	if windower.ffxi.get_info().zone ~= 231 then
		
        
		procedure.block("hp_click_ns")
		procedure.block("zone")
		
		procedure.wait(utils.calculate_time_offset(.75))
		
		for hp in L{"Home Point #1", "Home Point #2", "Home Point #3", "Home Point #4", "Home Point #5"}:it() do
			local mob = windower.ffxi.get_mob_by_name(hp)
			if mob and math.sqrt(mob.distance) < 6 then
				procedure.poke(mob.index)
				break
			end
		end
				
		if not procedure.wait(10, "hp_click_ns") then return false end
		
		return procedure.wait(20, "zone")
	else
		return true
	end

end


function procedure.get_phantom_gem()
	
	local kis = S(windower.ffxi.get_key_items())
	
	utils.run_path('htbf/to_trisvain')
	utils.wait_for_path()
	
	if not kis:contains(2619) then
		procedure.block("trisvain_response")
		procedure.wait(utils.calculate_time_offset(.75))
		procedure.poke("Trisvain")		
		return procedure.wait(10, "trisvain_response")
	else
		return true
	end
	
end

function procedure.move_to_hompoint()

	if windower.ffxi.get_info().zone ~= 160 then
		utils.run_path('htbf/to_homepoint')
		utils.wait_for_path()	
		
	else
		return true
	end

end

function procedure.travel_rancor()

    local kis = S(windower.ffxi.get_key_items())

    if not kis:contains(2619) then procedure.error("You ran out of merits!") return end

	if windower.ffxi.get_info().zone ~= 160 then
		procedure.block("hp_click_rancor")
		procedure.block("zone")
		
		procedure.wait(utils.calculate_time_offset(.75))
		procedure.poke("Home Point #2")
		
		if not procedure.wait(10, "hp_click_rancor") then return false end
		
		return procedure.wait(20, "zone")
	else
		
		return true
		
	end
end
	

function procedure.travel_cloister()
		

	if windower.ffxi.get_info().zone ~= 211 then
        procedure.wait(5) -- Initial zone in delay
		procedure.block("zone")
		procedure.wait(utils.calculate_time_offset(3))
		utils.run_path('htbf/to_cloister')
		
		return procedure.wait(20, "zone")
		
	else
		return true
	end

end

function procedure.approach_crystal()
	procedure.wait(3)
	utils.run_path("htbf/to_crystal")
	utils.wait_for_path()
end

function procedure.click_crystal()

	procedure.block('crystal_repsonse')
	procedure.wait(utils.calculate_time_offset(.75))
	procedure.poke("Water Protocrystal")
	
	return procedure.wait(10,'crystal_repsonse')

end

function procedure.enter_bc()

	procedure.block('warp_response_enter')
	
	local warp = packets.new("outgoing", 0x5c, {
			['X'] = procedure.bcnm.warp_array[procedure.bcnm.attempt]['X'],
			['Z'] = procedure.bcnm.warp_array[procedure.bcnm.attempt]['Z'],
			['Y'] = procedure.bcnm.warp_array[procedure.bcnm.attempt]['Y'],
			['Target ID'] = procedure.bcnm.target_id,
			['Target Index'] = procedure.bcnm.target_index,
			['_unknown1'] = (procedure.bcnm.warp_request_offset + procedure.bcnm.attempt) + procedure.bcnm.difficulty * 0x100,
			['Menu ID'] = procedure.bcnm.menu_id,
			['Zone'] = procedure.bcnm.zone_id,
			['_unknown3'] = procedure.bcnm.warp_array[procedure.bcnm.attempt]['_unknown3'],
			
	})
	
	packets.inject(warp)
	
	if not procedure.wait(3, 'warp_response_enter') then
		return false
	end
	
	local success = packets.new("outgoing", 0x5b, {
			['Target'] = procedure.bcnm.target_id,
			['Option Index'] = 0x0068,
			["_unknown1"] = 0,
			['Target Index'] = procedure.bcnm.target_index,
			["Automated Message"] = false,
			['Zone'] = procedure.bcnm.zone_id,
			['Menu ID'] = procedure.bcnm.menu_id
	}) 
	packets.inject(success)
	


end

function procedure.pre_buffs()
	
	utils.initiate_combat_routine('htbf_prebuffs_%s':format(windower.ffxi.get_player().main_job:lower()))
	
	procedure.wait(45)
	
end

function procedure.to_leviathan()
	
	utils.run_path('htbf/to_leviathan_%d':format(procedure.bcnm.attempt))

	utils.wait_for_path()

end

function procedure.kill_leviathan()
	procedure.block("death_watch")

	local job = windower.ffxi.get_player().main_job:lower()
	
	
	if not settings.htbf.leeches or not settings.htbf.leeches:map(string.lower):contains(utils.info.player.name:lower()) then
	
		if job == "blu" then
			utils.run_path('htbf/fight_leviathan_%d':format(procedure.bcnm.attempt))
			utils.fight_monsters(L{"Leviathan Prime"})
			utils.initiate_combat_routine("htbf_blu")
		else
			if job == "geo" then utils.follow(settings.leader or "") end
			procedure.wait(5)
			utils.initiate_combat_routine('htbf_%s':format(windower.ffxi.get_player().main_job:lower()))
		end
		
	end

    procedure.wait(10)

    local mob = windower.ffxi.get_mob_by_index(procedure.bcnm.indexes[procedure.bcnm.attempt])

	return mob and mob.hpp == 0
end

function procedure.use_warp_scroll()
    procedure.block("zone")
    procedure.wait(10)
    
    procedure.wait(utils.calculate_time_offset(3))
    windower.send_command('input /item "Instant Warp" <me>')
    
    return procedure.wait(20, "zone")

end

function procedure.home_point()
	if not utils.info.player then return false end
	
	procedure.block("zone")
	
	local p = packets.new("outgoing", 0x1A, {
		['Target'] = utils.info.player.id,
		['Target Index'] = utils.info.player.index,
		['Category'] = 0x0B
	})
	packets.inject(p)

	if not procedure.wait(15, "zone") then return false end

	procedure.current_task = procedure.task_list:length() + 1
	
end


procedure.triggers = {}

function procedure.triggers.trisvain_response(id,data)
	if not procedure.blocked("trisvain_response") then return end

	if id == 0x34 then
		local p = packets.parse('incoming',data)
		
		local mob = windower.ffxi.get_mob_by_index(p['NPC Index'])
		
		if mob and mob.name == "Trisvain" then
			
			   local inject = packets.new("outgoing", 0x5b, {
					['Target'] = p['NPC'],
					['Option Index'] = 0x0E02,
					['_unknown1'] = 0x0000,
					['Target Index'] = p['NPC Index'],
					['Zone'] = p['Zone'],
					['Menu ID'] = p['Menu ID']
				})
				packets.inject(inject)
				procedure.complete("trisvain_response")
			
			return true
		end
	
	end

end

function procedure.triggers.rolandienne_response(id,data)
	if not procedure.blocked("rolandienne_response") then return end

	if id == 0x34 then
		local p = packets.parse('incoming',data)
		
		local mob = windower.ffxi.get_mob_by_index(p['NPC Index'])
		
		if mob and mob.name == "Rolandienne" then
			   local purchase = packets.new("outgoing", 0x5b, {
					['Target'] = p['NPC'],
					['Option Index'] = 0x0001,
					['_unknown1'] = 0x0000,
                    ['Automated Message'] = true,
					['Target Index'] = p['NPC Index'],
					['Zone'] = p['Zone'],
					['Menu ID'] = p['Menu ID']
				})
                local esc = packets.new("outgoing", 0x5b, {
					['Target'] = p['NPC'],
					['Option Index'] = 0x0000,
					['_unknown1'] = 0x4000,
                    ['Automated Message'] = false,
					['Target Index'] = p['NPC Index'],
					['Zone'] = p['Zone'],
					['Menu ID'] = p['Menu ID']
				})
				packets.inject(purchase)
                packets.inject(esc)
				procedure.complete("rolandienne_response")
			
			return true
		end
	
	end

end

function procedure.triggers.crystal_repsonse(id,data)
	if not procedure.blocked("crystal_repsonse") then return end

	if id == 0x34 then
		local p = packets.parse('incoming',data)
		
		local mob = windower.ffxi.get_mob_by_index(p['NPC Index'])
		
		if mob and mob.name == "Water Protocrystal" and p['Menu ID'] == 32000 then
			
			   local inject = packets.new("outgoing", 0x5b, {
					['Target'] = p['NPC'],
					['Option Index'] = 0x00FF,
					['_unknown1'] = 0x0000,
					['Target Index'] = p['NPC Index'],
					['Automated Message'] = true,
					['Zone'] = p['Zone'],
					['Menu ID'] = p['Menu ID']
				})
				packets.inject(inject)
				procedure.complete("crystal_repsonse")
			
			return true
		end
	
	end

end

function procedure.triggers.warp_response(id,data)
	if not procedure.blocked('warp_response_enter') and 
	not procedure.blocked('warp_response_exit') then return end
	
	if id == 0x65 then
		local p = packets.parse('incoming',data)
		
		if p['_unknown1'] == 1 then
			procedure.complete('warp_response_enter')
		else
            procedure.bcnm.attempt = (procedure.bcnm.attempt % 3) + 1
        end
		
		procedure.complete('warp_response_exit')
	
	end	

end

function procedure.triggers.hp_click(id,data)
	if not procedure.blocked("hp_click_ns") and 
		not procedure.blocked("hp_click_rancor") and 
        not procedure.blocked("hp_click_ss") then return end

	if id == 0x34 then
		local p = packets.parse('incoming',data)
		
		local mob = windower.ffxi.get_mob_by_index(p['NPC Index'])
		
		if mob and L{"Home Point #1", "Home Point #2", "Home Point #3", "Home Point #4", "Home Point #5"}:contains(mob.name) then
			
            if procedure.blocked("hp_click_ss") then
			   local inject = packets.new("outgoing", 0x5b, {
					['Target'] = p['NPC'],
					['Option Index'] = 0x0002,
					['_unknown1'] = 0x0000,
					['Target Index'] = p['NPC Index'],
					['Zone'] = p['Zone'],
					['Menu ID'] = p['Menu ID']
				})
				packets.inject(inject)
				procedure.complete("hp_click_ss")
				return true
			elseif procedure.blocked("hp_click_ns") then
			   local inject = packets.new("outgoing", 0x5b, {
					['Target'] = p['NPC'],
					['Option Index'] = 0x0002,
					['_unknown1'] = 0x0004,
					['Target Index'] = p['NPC Index'],
					['Zone'] = p['Zone'],
					['Menu ID'] = p['Menu ID']
				})
				packets.inject(inject)
				procedure.complete("hp_click_ns")
				return true
			elseif procedure.blocked("hp_click_rancor") then
				local inject = packets.new("outgoing", 0x5b, {
					['Target'] = p['NPC'],
					['Option Index'] = 0x0002,
					['_unknown1'] = 0x005D,
					['Target Index'] = p['NPC Index'],
					['Zone'] = p['Zone'],
					['Menu ID'] = p['Menu ID']
				})
				packets.inject(inject)
				procedure.complete("hp_click_rancor")
				return true
			end
		end
	
	end

end

function procedure.triggers.death_check(new, old)	
	if not procedure.blocked("death_watch") then return end

	if new == 2 then 
		procedure.cancel()
		utils.finalize()
		notice("You died! Advancing to the end!")
		procedure.prioritize("death")
	end

end



function procedure.triggers.zone(id, data)	
	if not procedure.blocked('zone') then return end
	
	if id == 0x0a then
		procedure.complete("zone")
	end 

end

procedure.register_local_event('incoming chunk', procedure.triggers.zone)
procedure.register_local_event('incoming chunk', procedure.triggers.hp_click)
procedure.register_local_event('incoming chunk', procedure.triggers.trisvain_response)
procedure.register_local_event('incoming chunk', procedure.triggers.rolandienne_response)
procedure.register_local_event('incoming chunk', procedure.triggers.warp_response)
procedure.register_local_event('incoming chunk', procedure.triggers.crystal_repsonse)
procedure.register_local_event('status change', procedure.triggers.death_check)
 

procedure.add("change_sub", procedure.change_sub_hack, false)
procedure.add("travel_south_sandoria", procedure.travel_south_sandoria, false)
procedure.add("to_rolandienne", procedure.to_rolandienne, false)
procedure.add("buy_warp_scroll", procedure.buy_warp_scroll, false)
procedure.add("to_homepoint_ss", procedure.to_homepoint_ss, false)
procedure.add("travel_north_sandoria", procedure.travel_north_sandoria, false)
procedure.add("get_phantom_gem", procedure.get_phantom_gem, false)
procedure.add("move_to_hompoint", procedure.move_to_hompoint, false)
procedure.add("travel_rancor", procedure.travel_rancor, false)
procedure.add("travel_cloister", procedure.travel_cloister, false)
procedure.add("approach_crystal", procedure.approach_crystal, true)
procedure.add("click_crystal", procedure.click_crystal, true)
procedure.add("enter_bc", procedure.enter_bc, true)
procedure.add("pre_buffs", procedure.pre_buffs, false)
procedure.add("to_leviathan", procedure.to_leviathan, true)
procedure.add("kill_leviathan", procedure.kill_leviathan, false)
procedure.add("use_warp_scroll", procedure.use_warp_scroll, false)

procedure.priority_add("death", "home_point", procedure.home_point, false)

return procedure