---------------------------------------------------------------------------
---------------------------------------------------------------------------
---------------------------------------------------------------------------
---------------------------------------------------------------------------

function get_base()

	local f, err = dofile(windower.addon_path .. 'data/base.lua')
	
	if f and not err then
		return f
	else
		error("Could not locate the base procedure file.")
		return {}
	end


end

procedure = get_base()

if not procedure then return false end

---------------------------------------------------------------------------
---------------------------------------------------------------------------
---------------- CUSTOM DEFINED SECTION: EDIT BELOW -----------------------
---------------------------------------------------------------------------
---------------------------------------------------------------------------

packets = require('packets')
procedure.settings = {}
procedure.name = "Merits : Escha - Ru'Aun"

if not settings.leader or settings.leader == "" then
    notice("This routine requires that you initialize the setting 'leader' to a character that will be engaged.")
    notice("Do so by typing //coordinator set leader <name>!")
    return false
end

if not settings.merits.combat or settings.merits.combat:length() == 0 then
    notice("This routine requires that you assign players to engage.")
    notice("Do so by typing //coordinator add merits combat <name>!")
    return false
end

notice(" ===== Settings =====")
notice(" * Leader : %s":format(settings.leader))
notice(" * Combat Charcters : %s":format((settings.merits.combat and settings.merits.combat:concat(',')) or "none"))
notice(" * Leeches : %s":format((settings.merits.leeches and settings.merits.leeches:concat(',')) or "none"))
if settings.merits.follow then
	notice(" * Follow overrides : ")
	for k,v in pairs(settings.merits.follow) do
			notice("   - %s following %s":format(k,v))
	end
end

if windower.ffxi.get_player().main_job:lower() == "blu" then
    windower.send_command('blu load reisenaoe')
end

procedure.finished_merits = L{}

function procedure.validate(index)
	
    local mob = type(index) == "number" and windower.ffxi.get_mob_by_index(index) or type(index) == "string" and windower.ffxi.get_mob_by_name(index) or nil
	local valid = false
	
	if mob then
		valid = true
		if math.sqrt(mob.distance) < 6 then
			return mob
		end
    end

    if valid then
        warning("Too far from away from NPC")
    else
        warning("Unable to locate NPC.")
    end
end

function procedure.poke(index)
    local mob = procedure.validate(index)
    if mob then
        local p = packets.new('outgoing', 0x1a, {
            ["Target"] = mob.id,
            ["Target Index"] = mob.index,
        })
        packets.inject(p)
    end
end


function procedure.travel_misareaux()

	if windower.ffxi.get_info().zone ~= 25 then
		
		procedure.block("hp_click_misareaux")
		procedure.block("zone")
		
		for hp in L{"Home Point #1", "Home Point #2", "Home Point #3", "Home Point #4", "Home Point #5"}:it() do
			local mob = windower.ffxi.get_mob_by_name(hp)
			if mob and math.sqrt(mob.distance) < 6 then
                procedure.wait(utils.calculate_time_offset(2))
				procedure.poke(mob.index)
				break
			end
		end
				
		if not procedure.wait(10, "hp_click_misareaux") then return false end
		
		return procedure.wait(20, "zone")
	else
		return true
	end

end

function procedure.to_confluence()
		

	if windower.ffxi.get_info().zone ~= 25 then procedure.error("You aren't in the correct zone.") end
    
    utils.run_path('merits/to_confluence')
    utils.wait_for_path()	

end

function procedure.click_confluence()

    if windower.ffxi.get_info().zone ~= 25 then procedure.error("You aren't in the correct zone.") end
    
    procedure.block("confluence_response")
    procedure.block("zone")
    
    procedure.wait(utils.calculate_time_offset(2))
    procedure.poke("Undulating Confluence")
    
    if not procedure.wait(10, "confluence_response") then return false end
    
    return procedure.wait(15, "zone")


end

function procedure.to_dremi()
		

	if windower.ffxi.get_info().zone ~= 289 then procedure.error("You aren't in the correct zone.") end
    
    utils.run_path('merits/to_dremi')
    utils.wait_for_path()	

end

function procedure.get_mollifier()

    if windower.ffxi.get_info().zone ~= 289 then procedure.error("You aren't in the correct zone.") end
    
    procedure.block("dremi_response")
    
    procedure.wait(utils.calculate_time_offset(.75))
    procedure.poke("Dremi")
    
    return procedure.wait(10, "dremi_response")

end

function procedure.to_camp()
		

	if windower.ffxi.get_info().zone ~= 289 then procedure.error("You aren't in the correct zone.") end
    
    utils.run_path('merits/to_camp')
    utils.wait_for_path()	

end

procedure.settings.last_merit_check = os.clock()

function procedure.farm_merits()
    procedure.block("watch_my_merits")
    procedure.block("watch_merit_status")
	procedure.block("death_watch")
	
	utils.vars.settings.auto_suspend = true
    utils.vars.settings.fast_cast = .3
		
	if settings.merits.combat and settings.merits.combat:map(string.lower):contains(utils.info.player.name:lower()) then
		utils.run_path('merits/merit_loop', 'circular')
		utils.fight_monsters(L{"Eschan Phuabo", "Eschan Il'Aern", "Eschan Il'Aern's Wynav", "Eschan Zdei", "Eschan Yovra"})
	elseif settings.merits.follow[utils.info.player.name:lower()] then
		utils.follow(settings.merits.follow[utils.info.player.name:lower()])
	elseif settings.merits.leeches and settings.merits.leeches:map(string.lower):contains(utils.info.player.name:lower()) then
		utils.run_path('merits/leech')
		utils.wait_for_path()
	else
		utils.follow(settings.leader)
	end
	
	
	local job = windower.ffxi.get_player().main_job:lower()
	
	if not settings.merits.leeches or not settings.merits.leeches:map(string.lower):contains(utils.info.player.name:lower()) then	
		utils.initiate_combat_routine('merits_%s':format(job))
	end
	
	if os.clock() - procedure.settings.last_merit_check >= 180 then
		procedure.report_merits = true
		procedure.settings.last_merit_check = os.clock()
		local p = packets.new('outgoing', 0x61, {})
		packets.inject(p)
	end
    
    procedure.wait(30)

    for k,v in pairs(utils.info.alliancelist.name) do
        if not settings.merits.leeches:map(string.lower):contains(k) and not procedure.finished_merits:contains(k) then return false end
    end

    utils.vars.settings.auto_suspend = false
    windower.send_command('gs disable ring1; input /equip ring1 "Warp Ring";')
    procedure.wait(15)

end

function procedure.use_warp_ring()
    procedure.block('zone')
	procedure.block('death_watch2')
    
    procedure.wait(3)
    windower.send_command('input /item "Warp Ring" <me>')
    
    return procedure.wait(25, 'zone')
end

function procedure.home_point()
	if not utils.info.player then return false end
	
	procedure.block("zone")
	
	local p = packets.new("outgoing", 0x1A, {
		['Target'] = utils.info.player.id,
		['Target Index'] = utils.info.player.index,
		['Category'] = 0x0B
	})
	packets.inject(p)

	if not procedure.wait(15, "zone") then return false end
	
	procedure.current_task = 1
	
end

function procedure.home_point2()
	if not utils.info.player then return false end
	
	procedure.block("zone")
	
	local p = packets.new("outgoing", 0x1A, {
		['Target'] = utils.info.player.id,
		['Target Index'] = utils.info.player.index,
		['Category'] = 0x0B
	})
	packets.inject(p)

	if not procedure.wait(15, "zone") then return false end
	
	procedure.current_task = procedure.tasks:length() + 1
	
end

function procedure.enable_ring()
     windower.send_command('gs enable ring1')
end

procedure.triggers = {}


function procedure.triggers.hp_click(id,data)
	if not procedure.blocked("hp_click_misareaux") then return end

	if id == 0x34 then
		local p = packets.parse('incoming',data)
		
		local mob = windower.ffxi.get_mob_by_index(p['NPC Index'])
		
		if mob and L{"Home Point #1", "Home Point #2", "Home Point #3", "Home Point #4", "Home Point #5"}:contains(mob.name) then
           local inject = packets.new("outgoing", 0x5b, {
                ['Target'] = p['NPC'],
                ['Option Index'] = 0x0002,
                ['_unknown1'] = 0x0075,
                ['Target Index'] = p['NPC Index'],
                ['Zone'] = p['Zone'],
                ['Menu ID'] = p['Menu ID']
            })
            packets.inject(inject)
            procedure.complete("hp_click_misareaux")
            return true
		end
	
	end
end

function procedure.triggers.confluence_click(id,data)
	if not procedure.blocked("confluence_response") then return end

	if id == 0x34 then
		local p = packets.parse('incoming',data)
		
		local mob = windower.ffxi.get_mob_by_index(p['NPC Index'])
		
		if mob and L{"Undulating Confluence"}:contains(mob.name) then
           local inject = packets.new("outgoing", 0x5b, {
                ['Target'] = p['NPC'],
                ['Option Index'] = 0x0001,
                ['_unknown1'] = 0x0000,
                ['Target Index'] = p['NPC Index'],
                ['Zone'] = p['Zone'],
                ['Menu ID'] = p['Menu ID']
            })
            packets.inject(inject)
            procedure.complete("confluence_response")
            return true
		end
	
	end

end

function procedure.triggers.dremi_click(id,data)
	if not procedure.blocked("dremi_response") then return end

	if id == 0x34 then
		local p = packets.parse('incoming',data)
		
		local mob = windower.ffxi.get_mob_by_index(p['NPC Index'])
		
		if mob and L{"Dremi"}:contains(mob.name) then
           local silt = p['Menu Parameters']:unpack('I', 5)
           
            if silt < 500 then 
                procedure.error("You do not have enough silt to buy a mollifier!")
                return
            end
           
        
           local mollifier = packets.new("outgoing", 0x5b, {
                ['Target'] = p['NPC'],
                ['Option Index'] = 0x0A04,
                ['_unknown1'] = 0x0000,
                ['Automated Message'] = true,
                ['Target Index'] = p['NPC Index'],
                ['Zone'] = p['Zone'],
                ['Menu ID'] = p['Menu ID']
            })
            local esc = packets.new("outgoing", 0x5b, {
                ['Target'] = p['NPC'],
                ['Option Index'] = 0x0000,
                ['_unknown1'] = 0x4000,
                ['Automated Message'] = false,
                ['Target Index'] = p['NPC Index'],
                ['Zone'] = p['Zone'],
                ['Menu ID'] = p['Menu ID']
            })
            packets.inject(mollifier)
            packets.inject(esc)
            procedure.complete("dremi_response")
            return true
		end
	
	end

end

function procedure.triggers.watch_my_merits(id,data)
    if not procedure.blocked("watch_my_merits") then return end
    if id == 0x063 and data:byte(5) == 2 then  
        local number_of_merits = data:byte(11)%128
        local maximum_merits = data:byte(0x0D)%128
        if number_of_merits == maximum_merits and procedure.report_merits then
            procedure.report_merits = false
			windower.send_command('input /p finished my merits')
            if not procedure.finished_merits:contains(utils.info.player.name:lower()) then
                procedure.finished_merits:append(utils.info.player.name:lower())
            end
        end
    end
end

function procedure.triggers.watch_merit_status(msg, sender, mode)
    if not procedure.blocked("watch_merit_status") then return end

    sender = sender:sub(1,15)
    
    if mode == 4 and msg == "finished my merits" and not procedure.finished_merits:contains(sender:lower()) then
        procedure.finished_merits:append(sender:lower())
    end

end

function procedure.triggers.death_check(new, old)	
	if not procedure.blocked("death_watch") or not
		procedure.blocked("death_watch2") then return end

	if new == 2 then 
		if procedure.blocked("death_watch") then
			procedure.cancel()
			utils.finalize()
			notice("You died! Sending you back to the beginning!")
			procedure.prioritize("death")
		else
			procedure.cancel()
			utils.finalize()
			notice("You died! Sending you to the end!")
			procedure.prioritize("death2")
		end
	end
end



function procedure.triggers.zone(id, data)	
	if not procedure.blocked('zone') then return end
	
	if id == 0x0a then
		procedure.complete("zone")
	end 

end

procedure.register_local_event('incoming chunk', procedure.triggers.zone)
procedure.register_local_event('incoming chunk', procedure.triggers.hp_click)
procedure.register_local_event('incoming chunk', procedure.triggers.confluence_click)
procedure.register_local_event('incoming chunk', procedure.triggers.dremi_click)
procedure.register_local_event('incoming chunk', procedure.triggers.watch_my_merits)
procedure.register_local_event('chat message', procedure.triggers.watch_merit_status)
procedure.register_local_event('status change', procedure.triggers.death_check)


procedure.add("travel_misareaux", procedure.travel_misareaux, false)
procedure.add("to_confluence", procedure.to_confluence, false)
procedure.add("click_confluence", procedure.click_confluence, false)
procedure.add("to_dremi", procedure.to_dremi, false)
procedure.add("get_mollifier", procedure.get_mollifier, false)
procedure.add("to_camp", procedure.to_camp, false)
procedure.add("farm_merits", procedure.farm_merits, true)
procedure.add("use_warp_ring", procedure.use_warp_ring, false)
procedure.add("enable_ring", procedure.enable_ring, false)

procedure.priority_add("death", "home_point", procedure.home_point, false)
procedure.priority_add("death2", "home_point2", procedure.home_point2, false)


return procedure