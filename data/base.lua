---------------------------------------------------------------------------
---------------------------------------------------------------------------
---------------------- DO NOT DELETE THIS SECTION -------------------------
---------------------------------------------------------------------------
---------------------------------------------------------------------------
local procedure = {}
settings = config.load(defaults)
procedure.abort = false
procedure.registered_events = L {}
procedure.task_list = L {}
procedure.priority_list = {}
procedure.current_task = 1
procedure.blocks = S {}
procedure.internal_error = false
procedure.exiting = false
procedure.first = true
procedure.functions = {}
procedure.iterations = 0

function procedure.error(msg)
    if msg then
        error(msg)
    end
    procedure.internal_error = true
end

function procedure.is_error()
    return procedure.internal_error
end

--[[

For Coordinator use.

]]
function procedure.get_task(first_try)
    procedure.first = first_try
    
    local task
    
    for k, v in pairs(procedure.priority_list) do
        if v.active then
            task = v.task_list[v.current_task]
            break
        end
    end
    
    if not task then
        task = procedure.task_list[procedure.current_task]
    end
    
    if not task then
        return false
    end
    
    if not procedure.functions[task] then
        return false
    end
    
    procedure.abort = false
    
    return procedure.functions[task]
end

--[[

Regisers an event and adds it to the list of events this procedure has registered.

]]
function procedure.register_local_event(event, funct)
    procedure.registered_events:append(windower.register_event(event, funct))
end

--[[

Sets the task completion variable to false (can be redundant, use at your discretion).
Called at the beginning of a task.

]]
function procedure.initialize()
    procedure.exiting = false
    procedure.abort = false
end

function procedure.cancel()
    procedure.abort = true
    procedure.blocks:clear()
end

function procedure.wait(timeout, block)
    local start = os.time()
    while not procedure.exiting and not procedure.abort and not procedure.is_complete(block) and (not timeout or (timeout and ((os.time() - start) < timeout))) do
        coroutine.sleep(.1)
    end
    
    if timeout and (os.time() - start >= timeout) then
        return false
    else
        return true
    end
end

function procedure.block(to_block)
    if procedure.first and to_block then
        procedure.blocks[to_block] = true
    end
end

function procedure.blocked(check_block)
    if check_block then
        return procedure.blocks[check_block]
    end
    
    return false
end

function procedure.complete(block)
    if block then
        procedure.blocks[block] = false
    end
end

function procedure.is_complete(block)
    if block then
        return not procedure.blocks[block]
    end
    
    return false
end

function procedure.exit()
    procedure.exiting = true
    procedure.iterations = 0

    --Unregister events
    for k,v in pairs(procedure.registered_events) do
        windower.unregister_event(v)
    end
end

--[[

Unregisters local registered events and restores the task completion variable to false.
Call at the end of a task.

]]
function procedure.finalize()
    procedure.blocks:clear()
end

function procedure.advance_step()
    for k, v in pairs(procedure.priority_list) do
        if v.active then
            v.current_task = v.current_task + 1
            if v.current_task > v.task_list:length() then
                v.active = false
            end
            return 
        end
    end
    
    procedure.current_task = procedure.current_task + 1
end

function procedure.prioritize(name)
    if not procedure.priority_list[name] and not procedure.priority_list[name].active then
        return 
    end
    
    procedure.priority_list[name].current_task = 1
    procedure.priority_list[name].active = true
end

function procedure.add(s, func, sync)
    procedure.internal_add(s, func, sync, nil)
end

function procedure.priority_add(name, s, func, sync)
    procedure.internal_add(s, func, sync, name)
end

function procedure.internal_add(s, func, sync, priority)
    if type(s) ~= "string" or s:length() == 0 then
        procedure.error("You must provided a valid status when adding a task to the list.")
        return 
    end
    
    if type(func) ~= "function" then
        procedure.error(("add: %s is not a function."):format(tostring(f)))
        return 
    end
    
    if priority then
        if not procedure.priority_list[priority] then
            procedure.priority_list[priority] = {active = false, current_task = 1, task_list = L {}}
        end
        
        procedure.priority_list[priority].task_list:append(s)
    else
        procedure.task_list:append(s)
    end
    
    procedure.functions[s] = {name = s, f = func, synchronized = sync or false}
end
--[[

Common functions:

]]
function procedure.validate(index)
    local mob = type(index) == "number" and windower.ffxi.get_mob_by_index(index) or type(index) == "string" and windower.ffxi.get_mob_by_name(index) or nil
    local valid = false
    
    if mob then
        valid = true
        if math.sqrt(mob.distance) < 8 then
            return mob
        end
    end
    
    if valid then
        warning("Too far from away from NPC")
    else
        if type(index) == "number" then
            procedure.request_pop(index)
        end
        warning("Unable to locate NPC.")
    end
end

function procedure.poke(index)
    local mob = procedure.validate(index)
    if mob then
        local p = packets.new('outgoing', 0x1a, {["Target"] = mob.id, ["Target Index"] = mob.index})
        packets.inject(p)
    end
end

function procedure.request_pop(index)
    if index then
        local p = packets.new('outgoing', 0x16, {["Target Index"] = index})
        packets.inject(p)
    end
end

function procedure.party_leader()
    utils.refresh_info()
    
    if not utils.info._party then
        return false
    end
    
    return utils.info._party.party1_leader == utils.info.player.id
end

function procedure.restart()
    procedure.current_task = 1
end

return procedure
