---------------------------------------------------------------------------
---------------------------------------------------------------------------
---------------------------------------------------------------------------
---------------------------------------------------------------------------

function get_base()

	local f, err = dofile(windower.addon_path .. 'data/base.lua')
	
	if f and not err then
		return f
	else
		error("Could not locate the base procedure file.")
		return {}
	end


end

procedure = get_base()

if not procedure then return false end

---------------------------------------------------------------------------
---------------------------------------------------------------------------
---------------- CUSTOM DEFINED SECTION: EDIT BELOW -----------------------
---------------------------------------------------------------------------
---------------------------------------------------------------------------

packets = require('packets')
procedure.settings = {}
procedure.name = "Ambuscade (April Vol. 1)"

if not settings.leader or settings.leader == "" then
    notice("This routine requires that you initialize the setting 'leader' to a character that will be engaged.")
    notice("Do so by typing //coordinator set leader <name>!")
    return false
else
    notice(" ===== Settings =====")
    notice(" * Leader : %s":format(settings.leader))
	notice(" * Leeches : %s":format((settings.ambuscade.leeches and settings.ambuscade.leeches:concat(',')) or "none"))
end


if windower.ffxi.get_player().main_job:lower() == "blu" then
        windower.send_command('lua l bloop; wait 2; blu load solo')
end


function procedure.change_sub_hack()
        
    procedure.wait(5)

	if not settings.htbf.leeches or not settings.htbf.leeches:map(string.lower):contains(utils.info.player.name:lower()) then
		if L{53,247,248,249,252}:contains(windower.ffxi.get_info().zone) then
			local job = windower.ffxi.get_player().main_job:lower()

			local mnk = packets.new('outgoing', 0x100, { ['Sub Job'] = 0x02 })
			packets.inject(mnk)
			
			if job == "blu" then
				local war = packets.new('outgoing', 0x100, { ['Sub Job'] = 0x01 })
				packets.inject(war)
			elseif job == "whm" then
				local sch = packets.new('outgoing', 0x100, { ['Sub Job'] = 0x14 })
				packets.inject(sch)
			elseif L{"geo","brd"}:contains(job) then
				local rdm = packets.new('outgoing', 0x100, { ['Sub Job'] = 0x05 })
				packets.inject(rdm)
			end
		end
	end


end

function procedure.accept()

	local c = packets.new('outgoing',0x10d, {
		['RoE Quest'] = 3998
	})
	local a = packets.new('outgoing',0x10c, {['RoE Quest'] = 3998 })
	
	packets.inject(c)
	procedure.wait(4)
	packets.inject(a)

end

function procedure.travel_south_sandoria()

	if windower.ffxi.get_info().zone ~= 230 then
		
        procedure.wait(utils.calculate_time_offset(3))
		procedure.block("hp_click_ss")
		procedure.block("zone")
		
		for hp in L{"Home Point #1", "Home Point #2", "Home Point #3", "Home Point #4", "Home Point #5"}:it() do
			local mob = windower.ffxi.get_mob_by_name(hp)
			if mob and math.sqrt(mob.distance) < 6 then
				procedure.poke(mob.index)
				break
			end
		end
				
		if not procedure.wait(10, "hp_click_ss") then return false end
		
		return procedure.wait(20, "zone")
	else
		return true
	end

end


function procedure.to_rolandienne()

    utils.run_path('htbf/to_rolandienne')
    utils.wait_for_path()

end

function procedure.buy_warp_scroll()

    procedure.block("rolandienne_response")
    
    procedure.wait(utils.calculate_time_offset(.75))
    procedure.poke("Rolandienne")
    
    return procedure.wait(10, "rolandienne_response")

end

function procedure.to_homepoint_ss()

    utils.run_path('htbf/to_homepoint_ss')
    utils.wait_for_path()

end

function procedure.wait10()
	procedure.wait(10)
end

function procedure.to_xarc()
	
	procedure.block("zone")
	procedure.block("hp_click_xarc")
	
	for hp in L{"Home Point #1", "Home Point #2", "Home Point #3", "Home Point #4", "Home Point #5"}:it() do
		local mob = windower.ffxi.get_mob_by_name(hp)
		if mob and math.sqrt(mob.distance) < 6 then
			procedure.poke(mob.index)
			break
		end
	end
	
	
	if not procedure.wait(10, "hp_click_xarc") then return false end
	
	return procedure.wait(20, "zone")

end

function procedure.kill_gigas()

	local kis = S(windower.ffxi.get_key_items())
	
	if not kis:contains(3052) then
		procedure.wait(5)
	    local job = windower.ffxi.get_player().main_job:lower()
		utils.initiate_combat_routine('htbf_%s':format(job))
		
		if job == "blu" then
			utils.run_path('ambuscade/gigas')
			utils.fight_monsters(L{"Gigas Hurler", "Gigas Lopper", "Gigas Slugger", "Gigas Flogger"})
		else
			utils.follow(settings.leader or "")
		end		
		
		return false
	end

end

function procedure.use_warp_scroll()
    procedure.block("zone")
    procedure.wait(10)
    
    procedure.wait(utils.calculate_time_offset(3))
    windower.send_command('input /item "Instant Warp" <me>')
    
    return procedure.wait(20, "zone")

end

function procedure.travel_mhaura()

	if windower.ffxi.get_info().zone ~= 249 then
		
        procedure.wait(utils.calculate_time_offset(3))
		procedure.block("hp_click_mhaura")
		procedure.block("zone")
		
		for hp in L{"Home Point #1", "Home Point #2", "Home Point #3", "Home Point #4", "Home Point #5"}:it() do
			local mob = windower.ffxi.get_mob_by_name(hp)
			if mob and math.sqrt(mob.distance) < 6 then
				procedure.poke(mob.index)
				break
			end
		end
				
		if not procedure.wait(10, "hp_click_mhaura") then return false end
		
		return procedure.wait(20, "zone")
	else
		return true
	end

end

function procedure.magic_barrier()

	if procedure.party_leader() then
		utils.initiate_combat_routine('magic_barrier')
		procedure.wait(8)
	end


end

function procedure.to_tome()

	if procedure.party_leader() then
		procedure.wait(3)
		utils.run_path('ambuscade/to_tome')
		utils.wait_for_path()
	end

end

function procedure.click_tome()

	if procedure.party_leader() then
		procedure.block('tome_response')
		
		procedure.poke(150)
	
		return procedure.wait(10, 'tome_response')
	end

end

function procedure.ambuscade_zone()

	procedure.block('zone')
	
	return procedure.wait(60, 'zone')

end

function procedure.pre_buffs()
	
	procedure.wait(15)
	
	utils.initiate_combat_routine('ambuscade_prebuffs_%s':format(windower.ffxi.get_player().main_job:lower()))
	
	procedure.wait(45)
	
end


function procedure.kill_monsters()
	procedure.block("death_watch")

	if not settings.ambuscade.leeches or not settings.ambuscade.leeches:map(string.lower):contains(utils.info.player.name:lower()) then
	
		local job = windower.ffxi.get_player().main_job:lower()
		
		if job == "geo" then utils.follow(settings.leader or "") end
		
		utils.initiate_combat_routine('ambuscade_%s':format(windower.ffxi.get_player().main_job:lower()))
		
		for target in L{99,101,102}:it() do
			local mob = windower.ffxi.get_mob_by_index(target)
			
			if not mob then 
				return false
			elseif mob and mob.hpp ~= 0 then
				if job == "blu" then
					utils.fight_monsters(L{mob.name})
					utils.follow(mob.index)
					procedure.wait(5)
					return false
				else
					utils.follow(settings.leader or "")
					procedure.wait(5)
					return false
				end
			end
		end
	end
	
end


function procedure.to_homepoint_mhaura()
	
	procedure.wait(5)
	utils.run_path('ambuscade/to_homepoint_mhaura')
	utils.wait_for_path()

end


function procedure.home_point()
	if not utils.info.player then return false end
	
	procedure.block("zone")
	
	local p = packets.new("outgoing", 0x1A, {
		['Target'] = utils.info.player.id,
		['Target Index'] = utils.info.player.index,
		['Category'] = 0x0B
	})
	packets.inject(p)

	if not procedure.wait(15, "zone") then return false end

	procedure.current_task = procedure.task_list:length() + 1
	
end


procedure.triggers = {}

function procedure.triggers.warp_response(id,data)
	if not procedure.blocked('warp_response_enter') and 
	not procedure.blocked('warp_response_exit') then return end
	
	if id == 0x65 then
		local p = packets.parse('incoming',data)
		
		if p['_unknown1'] == 1 then
			procedure.complete('warp_response_enter')
		else
            procedure.bcnm.attempt = (procedure.bcnm.attempt % 3) + 1
        end
		
		procedure.complete('warp_response_exit')
	
	end	

end

function procedure.triggers.hp_click(id,data)
	if not procedure.blocked("hp_click_ss") and 
		not procedure.blocked("hp_click_xarc") and
		not procedure.blocked("hp_click_mhaura") 
		then return end

	if id == 0x34 then
		local p = packets.parse('incoming',data)
		
		local mob = windower.ffxi.get_mob_by_index(p['NPC Index'])
		
		if mob and L{"Home Point #1", "Home Point #2", "Home Point #3", "Home Point #4", "Home Point #5"}:contains(mob.name) then
			
            if procedure.blocked("hp_click_xarc") then
			   local inject = packets.new("outgoing", 0x5b, {
					['Target'] = p['NPC'],
					['Option Index'] = 0x002,
					['_unknown1'] = 0x006f,
					['Target Index'] = p['NPC Index'],
					['Zone'] = p['Zone'],
					['Menu ID'] = p['Menu ID']
				})
				packets.inject(inject)
				procedure.complete("hp_click_xarc")
				return true
			elseif procedure.blocked("hp_click_ss") then
			   local inject = packets.new("outgoing", 0x5b, {
					['Target'] = p['NPC'],
					['Option Index'] = 0x0002,
					['_unknown1'] = 0x0000,
					['Target Index'] = p['NPC Index'],
					['Zone'] = p['Zone'],
					['Menu ID'] = p['Menu ID']
				})
				packets.inject(inject)
				procedure.complete("hp_click_ss")
				return true
			elseif procedure.blocked("hp_click_mhaura") then
			   local inject = packets.new("outgoing", 0x5b, {
					['Target'] = p['NPC'],
					['Option Index'] = 0x0002,
					['_unknown1'] = 0x0028,
					['Target Index'] = p['NPC Index'],
					['Zone'] = p['Zone'],
					['Menu ID'] = p['Menu ID']
				})
				packets.inject(inject)
				procedure.complete("hp_click_mhaura")
				return true
			end
		end
	
	end

end

function procedure.triggers.rolandienne_response(id,data)
	if not procedure.blocked("rolandienne_response") then return end

	if id == 0x34 then
		local p = packets.parse('incoming',data)
		
		local mob = windower.ffxi.get_mob_by_index(p['NPC Index'])
		
		if mob and mob.name == "Rolandienne" then
			   local purchase = packets.new("outgoing", 0x5b, {
					['Target'] = p['NPC'],
					['Option Index'] = 0x0001,
					['_unknown1'] = 0x0000,
                    ['Automated Message'] = true,
					['Target Index'] = p['NPC Index'],
					['Zone'] = p['Zone'],
					['Menu ID'] = p['Menu ID']
				})
                local esc = packets.new("outgoing", 0x5b, {
					['Target'] = p['NPC'],
					['Option Index'] = 0x0000,
					['_unknown1'] = 0x4000,
                    ['Automated Message'] = false,
					['Target Index'] = p['NPC Index'],
					['Zone'] = p['Zone'],
					['Menu ID'] = p['Menu ID']
				})
				packets.inject(purchase)
                packets.inject(esc)
				procedure.complete("rolandienne_response")
			
			return true
		end
	
	end

end

function procedure.triggers.tome_response(id,data)
	if not procedure.blocked("tome_response") then return end

	if id == 0x34 then
		local p = packets.parse('incoming',data)
		
		local mob = windower.ffxi.get_mob_by_index(p['NPC Index'])
		
		if mob and mob.name == "Ambuscade Tome" then
			   local reserve = packets.new("outgoing", 0x5b, {
					['Target'] = p['NPC'],
					['Option Index'] = 0x0004,
					['_unknown1'] = 0x0000,
                    ['Automated Message'] = true,
					['Target Index'] = p['NPC Index'],
					['Zone'] = p['Zone'],
					['Menu ID'] = p['Menu ID']
				})
				local esc = packets.new("outgoing", 0x5b, {
					['Target'] = p['NPC'],
					['Option Index'] = 0x0004,
					['_unknown1'] = 0x0000,
                    ['Automated Message'] = false,
					['Target Index'] = p['NPC Index'],
					['Zone'] = p['Zone'],
					['Menu ID'] = p['Menu ID']
				})
				packets.inject(reserve)
				packets.inject(esc)
				procedure.complete("tome_response")
			
			return true
		end
	
	end

end

function procedure.triggers.death_check(new, old)	
	if not procedure.blocked("death_watch") then return end

	if new == 2 then 
		procedure.cancel()
		utils.finalize()
		notice("You died! Advancing to the end!")
		procedure.prioritize("death")
	end

end



function procedure.triggers.zone(id, data)	
	if not procedure.blocked('zone') then return end
	
	if id == 0x0a then
		procedure.complete("zone")
	end 

end

procedure.register_local_event('incoming chunk', procedure.triggers.hp_click)
procedure.register_local_event('incoming chunk', procedure.triggers.zone)
procedure.register_local_event('incoming chunk', procedure.triggers.rolandienne_response)
procedure.register_local_event('incoming chunk', procedure.triggers.tome_response)
procedure.register_local_event('status change', procedure.triggers.death_check)

procedure.add("travel_south_sandoria", procedure.travel_south_sandoria, false)
procedure.add("to_rolandienne", procedure.to_rolandienne, false)
procedure.add("buy_warp_scroll", procedure.buy_warp_scroll, false)
procedure.add("to_homepoint_ss", procedure.to_homepoint_ss, false)
procedure.add("accept", procedure.accept, false)
procedure.add("to_xarc", procedure.to_xarc, true)
procedure.add("wait10", procedure.wait10, true)
procedure.add("kill_gigas", procedure.kill_gigas, false)
procedure.add("use_warp_scroll", procedure.use_warp_scroll, false)
procedure.add("change_sub_hack", procedure.change_sub_hack, false)
procedure.add("travel_mhaura", procedure.travel_mhaura, true)
procedure.add("magic_barrier", procedure.magic_barrier, false)
procedure.add("to_tome", procedure.to_tome, false)
procedure.add("click_tome", procedure.click_tome, false)
procedure.add("ambuscade_zone", procedure.ambuscade_zone, true)
procedure.add("pre_buffs", procedure.pre_buffs, false)
procedure.add("kill_monsters", procedure.kill_monsters, false)
procedure.add("ambuscade_zone", procedure.ambuscade_zone, false)
procedure.add("to_homepoint_mhaura", procedure.to_homepoint_mhaura, false)

procedure.priority_add("death", "home_point", procedure.home_point, false)

return procedure