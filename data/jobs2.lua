---------------------------------------------------------------------------
---------------------------------------------------------------------------
---------------------------------------------------------------------------
---------------------------------------------------------------------------

function get_base()

	local f, err = dofile(windower.addon_path .. 'data/base.lua')
	
	if f and not err then
		return f
	else
		error("Could not locate the base procedure file.")
		return {}
	end


end

procedure = get_base()

if not procedure then return false end

---------------------------------------------------------------------------
---------------------------------------------------------------------------
---------------- CUSTOM DEFINED SECTION: EDIT BELOW -----------------------
---------------------------------------------------------------------------
---------------------------------------------------------------------------

packets = require('packets')

procedure.settings = {}
procedure.name = "Job Change : HTBF"

if not settings.leader or settings.leader == "" then
    notice("This routine requires that you initialize the setting 'leader' to a character that will be engaged.")
    notice("Do so by typing //coordinator set leader <name>!")
    return false
end

function procedure.job_change()
        
    procedure.wait(2)

	if settings.jobs2[utils.info.player.name:lower()] then

	
		if L{53,247,248,249,252}:contains(windower.ffxi.get_info().zone) then
			if not res then return end
			
			local job

			for k,v in pairs(res.jobs) do
				if S{v.ens:lower(), v.en:lower()}:contains(settings.jobs2[utils.info.player.name:lower()]:lower()) then
					job = k
					break
				end
			end
			
			if not job then return end
			
			local switch = packets.new('outgoing', 0x100, { ['Main Job'] = job, ['Sub Job'] = 0x05 })
			packets.inject(switch)
		end

	end


end



procedure.add("job change", procedure.job_change, false)

return procedure
