utils = {}
utils.vars = {}
utils.commands = {}

function pathsearch(files_list)

    -- base directory search order:
    -- windower
    
    -- sub directory search order:
    -- libs-dev (only in windower addon path)
    -- libs (only in windower addon path)
    -- data/player.name
    -- data/common
    -- data
    
    local coordinator_data = windower.addon_path .. 'utils/'
    
    local search_path = {
        [1] = coordinator_data,
        [2] = windower.addon_path .. 'libs/utils/',
    }
    local normal_path

    for _,basepath in ipairs(search_path) do
        if windower.dir_exists(basepath) then
            for i,v in ipairs(files_list) do
                if v ~= '' then
                    normal_path = basepath .. v
                    
                    if normal_path and windower.file_exists(normal_path) then
                        return normal_path,basepath,v
                    end
                end
            end
        end
    end
    
    return false
end

require('lists')
require('pack')
JSON = require('utils/json')
file = require('files')
res = require('resources')

utils.c_buffer = require('utils/circ_buff') 
utils.paths = require('utils/paths')
require('utils/combat')
require('utils/movement')
require('utils/helpers')
require('utils/engage')
require('utils/userinfo')

utils.info = utils.make_user_table()

utils.vars._static = {
    target_list = L{},
    path = {},
    stuck_detection = true,
    engage_distance = 20,
    minimum_recast = 0,
    ja_delay = 1.2,
    ws_delay = 4.2,
    ma_delay = 6,
    fast_cast = .8,
    stop_vector = {0,0,0},
    circ_buffer_length = 5,
    approach_mobile_low = 0.2,
    approach_mobile_high = 0.7,
    approach_stationary_low = 1,
    approach_stationary_high = 1.3,
    facing_offset = 0,
    enemy_attack_interval = 4,
    passive_delay = .05,
    path_departure_distance = 35,
    max_z_difference = 5,
    ranges = {[0]=0,
        [2] = 1.70,
        [3] = 1.490909,
        [4] = 1.44,
        [5] = 1.377778,
        [6] = 1.30,
        [7] = 1.20,
        [8] = 1.30,
        [9] = 1.377778,
        [10] = 1.45,
        [11] = 1.490909,
        [12] = 1.70,
    },
    pass_through_targs = {['<t>']=true,['<me>']=true,['<ft>']=true,['<scan>']=true,['<bt>']=true,['<lastst>']=true,
    ['<r>']=true,['<pet>']=true,['<p0>']=true,['<p1>']=true,['<p2>']=true,['<p3>']=true,['<p4>']=true,
    ['<p5>']=true,['<a10>']=true,['<a11>']=true,['<a12>']=true,['<a13>']=true,['<a14>']=true,['<a15>']=true,
    ['<a20>']=true,['<a21>']=true,['<a22>']=true,['<a23>']=true,['<a24>']=true,['<a25>']=true,['<st>']=true,
    ['<stnpc>']=true,['<stal>']=true,['<stpc>']=true,['<stpt>']=true},
    
    unify_prefix = {['/ma'] = '/ma', ['/magic']='/ma',['/jobability'] = '/ja',['/ja']='/ja',['/item']='/item',['/song']='/ma',
    ['/so']='/ma',['/ninjutsu']='/ma',['/weaponskill']='/ws',['/ws']='/ws',['/ra']='/ra',['/rangedattack']='/ra',['/nin']='/ma',
    ['/throw']='/ra',['/range']='/ra',['/shoot']='/ra',['/monsterskill']='/ms',['/ms']='/ms',['/pet']='/ja',['Monster']='Monster',['/bstpet']='/ja'}
}

utils.vars.settings = {}
utils.vars._global = {}
utils.vars.lasttime = os.clock()
utils.vars.lastactiondelay = 0
utils.vars.monster_array = {}
utils.vars.delay = utils.vars._static.passive_delay

utils.cb = {}
utils.circ_buffer = {}

utils.validabils = {}
utils.validabils['english'] = {['/ma'] = {}, ['/ja'] = {}, ['/ws'] = {}, ['/item'] = {}, ['/ra'] = {}, ['/ms'] = {}, ['/pet'] = {}, ['/trig'] = {}, ['/echo'] = {}}
utils.validabils['french'] = {['/ma'] = {}, ['/ja'] = {}, ['/ws'] = {}, ['/item'] = {}, ['/ra'] = {}, ['/ms'] = {}, ['/pet'] = {}, ['/trig'] = {}, ['/echo'] = {}}
utils.validabils['german'] = {['/ma'] = {}, ['/ja'] = {}, ['/ws'] = {}, ['/item'] = {}, ['/ra'] = {}, ['/ms'] = {}, ['/pet'] = {}, ['/trig'] = {}, ['/echo'] = {}}
utils.validabils['japanese'] = {['/ma'] = {}, ['/ja'] = {}, ['/ws'] = {}, ['/item'] = {}, ['/ra'] = {}, ['/ms'] = {}, ['/pet'] = {}, ['/trig'] = {}, ['/echo'] = {}}

function utils.make_abil(abil,lang,i)
    if not abil[lang] or not abil.prefix then return end
    local sp,pref = abil[lang]:lower(), utils.vars._static.unify_prefix[abil.prefix:lower()]
    utils.validabils[lang][pref][sp] = i
end

function utils.make_entry(v,i)
    utils.make_abil(v,'english',i)
    utils.make_abil(v,'german',i)
    utils.make_abil(v,'french',i)
    utils.make_abil(v,'japanese',i)
end

for i,v in pairs(res.spells) do
    if not T{363,364}:contains(i) then
        utils.make_entry(v,i)
    end
end

for i,v in pairs(res.job_abilities) do
    utils.make_entry(v,i)
end

for i,v in pairs(res.weapon_skills) do
    v.type = 'WeaponSkill'
    utils.make_entry(v,i)
end

for i,v in pairs(res.monster_abilities) do
    v.type = 'MonsterSkill'
    utils.make_entry(v,i)
end

for i,v in pairs(res.items) do
    v.prefix = '/item'
    utils.make_entry(v,i)
end


function utils.finalize()

    if utils.vars.running then
        windower.ffxi.run(false)
    end

    utils.vars.settings.target_list:clear()
    utils.vars.combat_routine = nil
    if utils.vars.running then
        utils.vector_run(utils.vars._static.stop_vector)
        utils.vars.running = nil
    end
    utils.vars.fighting = nil
    utils.vars.pathing_complete = false
    utils.vars.settings.path = nil

end

function utils.decode_json_file(path)
    local import,msg = file.read(path)
    local temp_settings = nil
    
    if import then
        temp_settings = JSON:decode(import)
        for i,v in pairs(temp_settings) do
            temp_settings[i] = JSON:decode(v)
        end
        
        if temp_settings.path then
            return temp_settings.path
        end
    end
end

function utils.load_path(file_name)
        local file_path = 'data/paths/'.. file_name ..'.json'

        local path = utils.decode_json_file(file_path)
        
        if path then
            utils.vars.settings.path = path
            return true
        else
            print("Failed to load path %s":format(file_name))
            return false
        end
end

function utils.run_path(path_name,p_mode)

    p_mode = p_mode or 'linear'
    
    if not L{'linear','circular'}:contains(p_mode) then return end
    
    if not utils.vars.running and utils.load_path(path_name) then
        utils.vars.running = {mode = p_mode, last_vector = utils.vars._static.stop_vector, path = utils.paths['new_%s_path':format(p_mode)](utils.vars.settings.path)}
        utils.enable_stuck_detection()
    end

end

function utils.follow(character) 
    local targ
    
    if character then
        targ = windower.ffxi.get_mob_by_index(tonumber(character) or -1) or windower.ffxi.get_mob_by_name(character)
        if targ then
            if utils.vars.running and utils.vars.running.mode == "follow" then 
                utils.vars.running.target = targ
            elseif not utils.vars.running then
                utils.vars.running = {mode = "follow", last_vector = utils.vars._static.stop_vector, target=targ, ts=os.clock()}
            end
        end
    end
end

function utils.initiate_combat_routine(file_name, overwrite)
    if utils.vars.combat_routine and not overwrite then return end
    
    local path = windower.addon_path .. 'data/combat/'.. file_name ..'.lua'

    if windower.file_exists(path) then
    
        local import,msg = dofile(path)
    
        if import then
            utils.vars.combat_routine = import
            if not utils.vars.combat_routine.last then
                utils.vars.combat_routine.last = utils.make_user_table()
            end
        else
            error("Could not load the specified combat routine.")
        end
    else
        error("Could not find the specified combat routine (%s)":format(file_name))
    end
end

function utils.wait_for_path(timeout)
    if not utils.vars.running then
        return true
    end
    
    if utils.vars.running.path:mode() == "circular" then
        error("You cannot wait for a circular path to finish.")
        return true
    end


    local start = os.time()
    while utils.vars.running and (not utils.vars.running.path or (utils.vars.running.path and utils.dist(utils.get_c_coords(), utils.vars.running.path:last()) > 1.5)) and (not timeout or (timeout and ((os.time() - start) < timeout)))  do
        coroutine.sleep(.1)
    end
    
    if timeout and (os.time() - start >= timeout) then
        return false
    else
        utils.vector_run(utils.vars._static.stop_vector)
        utils.vars.running = nil
        return true
    end
end

function utils.fight_monsters(list, engage_mobs)

    if engage_mobs == nil then
        engage_mobs = true
    end
    
    if type(list) ~= "table" then return end
        
    utils.vars.settings.target_list:clear()	
        
    for mob in list:it() do
        utils.vars.settings.target_list:append(mob:lower())
    end

    if not utils.vars.fighting then		
        utils.vars.fighting = {engaged = {index=({}).index,ts=os.clock()}, suspended=false, engage = engage_mobs}
    end

end

function utils.initialize()
    utils.vars.monster_array = {}
    utils.vars.last_ws_time = 0
    utils.vars.running = nil
    utils.vars.fighting = nil
    utils.refresh_info()
    utils.settings = setmetatable(table.clear(utils.vars.settings),{__index = utils.vars._static, __newindex = rawset})
end

utils.vars._global.circ_buffer = utils.c_buffer.new(utils.vars._static.circ_buffer_length)


function utils.inc_packet_updates(id,org,new,is_injected,is_blocked)
    if is_injected then return end
    local t = os.clock()-.5 -- 500ms lag between client->server->client minimum
    if id == 0x00E then
        local index = org:unpack('H',9)
        
        if org:byte(11)%2 == 1 then -- First bit of byte 11 is a flag for position updating
            if not utils.vars.monster_array[index] then utils.vars.monster_array[index] = {} end
            utils.vars.monster_array[index].s_ts = t
            utils.vars.monster_array[index].s_coords = {org:unpack('f',13),org:unpack('f',17),org:unpack('f',21)}
        end
        
        if org:byte(11)%8 >= 4 then
            if not utils.vars.monster_array[index] then utils.vars.monster_array[index] = {} end
            utils.vars.monster_array[index].hpp = org:byte(31)
            --print('Index %d now has HPP %d':format(index, utils.vars.monster_array[index].hpp))
        end
        
        if utils.vars.monster_array[index] then
            utils.vars.monster_array[index].model = org:unpack('H',0x33) -- All packets seem to include model, but sometimes they arrive before a position entry is made in the array (somehow)
        end
   elseif id == 0x0D then
        local index = org:unpack('H',9)
        
         if org:byte(11)%2 == 1 then -- First bit of byte 11 is a flag for position updating
            if not utils.vars.monster_array[index] then utils.vars.monster_array[index] = {} end
            utils.vars.monster_array[index].s_ts = t
            utils.vars.monster_array[index].s_coords = {org:unpack('f',13),org:unpack('f',17),org:unpack('f',21)}
        end
        
        if org:byte(11)%8 >= 4 then
            if not utils.vars.monster_array[index] then utils.vars.monster_array[index] = {} end
            utils.vars.monster_array[index].hpp = org:byte(31)
            --print('Index %d now has HPP %d':format(index, utils.vars.monster_array[index].hpp))
        end
   
   elseif id == 0x28 then
        local act = windower.packets.parse_action(org)
        for i,v in pairs(act.targets) do
            if (utils.info.party_list and utils.info.party_list[v.id]) or (not utils.info.party_list and utils.info.player and v.id == utils.info.player.id) then
                local potential_attacker = windower.ffxi.get_mob_by_id(act.actor_id)
                if potential_attacker and utils.valid_monster(potential_attacker.index,false) and utils.vars.monster_array[potential_attacker.index] then
                    utils.vars.monster_array[potential_attacker.index].attacking = 'attacking'
                    utils.vars.monster_array[potential_attacker.index].last_attack = os.clock()
                    utils.vars.monster_array[potential_attacker.index].last_target = utils.info.partylist and utils.info.partylist.id[v.id] and utils.info.partylist.id[v.id].index or utils.info.player.index -- This or should never work.
                    break
                end
            end
        end
    end
end

function utils.out_packet_updates(id,org,new,is_injected,is_blocked)
    if id == 0x015 then
        local X = org:unpack('f',5)
        local Z = org:unpack('f',9)
        local Y = org:unpack('f',13)
        utils.vars._global.circ_buffer:append({X,Z,Y})
        if not utils.info.player then return end
        if not utils.vars.monster_array[utils.info.player.index] then utils.vars.monster_array[utils.info.player.index] = {attacking=false} end
        utils.vars.monster_array[utils.info.player.index].s_coords = {X,Z,Y}
        utils.vars.monster_array[utils.info.player.index].s_ts = os.clock()
    end
end

function utils.run()
    
    local inc_packet_event = windower.register_event('incoming chunk',utils.inc_packet_updates)
    local zone_event = windower.register_event('zone change',utils.initialize)
    local out_packet_event = windower.register_event('outgoing chunk',utils.out_packet_updates)
    
    while true do
        if utils.vars.running or utils.vars.fighting or utils.vars.combat_routine then

            if utils.vars.running and tonumber(utils.vars.running.suspended) and (utils.vars.running.suspended - os.clock() < 0) then
                utils.resume()
            end

            utils.refresh_info()
        
            local c_coords = utils.get_c_coords()
        
        
            if utils.vars.fighting and utils.vars.fighting.target and utils.valid_monster(utils.vars.fighting.target.index) and
                (not utils.vars.running or not utils.vars.running.target or utils.vars.running.target.index ~= utils.vars.fighting.target.index) then
                
                if utils.vars.running and utils.vars.running.mode == 'fighting' then
                    utils.vars.running.target = {index = utils.vars.fighting.target.index, ts = os.clock()}
                else
                    utils.vars.fighting.cached_running = utils.vars.running
                    utils.vars.running = {
                        mode = 'fighting',
                        last_vector = utils.vars._static.stop_vector,
                        target = {index = utils.vars.fighting.target.index, ts=os.clock()}
                        }
                end
            elseif utils.vars.fighting and (not utils.vars.fighting.target or not utils.valid_monster(utils.vars.fighting.target.index)) and utils.vars.fighting.cached_running then
                utils.vars.running = utils.vars.fighting.cached_running
                utils.vars.fighting.cached_running = nil
            end
                
            if utils.vars.running and not utils.vars.running.suspended then
                if utils.vars.settings.stuck_detection and utils.vars._global.circ_buffer:full() then
                    local cur,max_dist = utils.vars._global.circ_buffer[-1],0
                    for t = 1,utils.vars._static.circ_buffer_length-1 do
                        if utils.vars._global.circ_buffer[t] then
                            -- Find the greatest distance between the most recent point and other points in the buffer.
                            max_dist = math.max(utils.dist(cur,utils.vars._global.circ_buffer[t]),max_dist)
                        end
                    end
                    if max_dist < 2 and not utils.vars.running.stuck and not utils.tab_comp(utils.vars.running.last_vector,utils.vars._static.stop_vector) then
                    -- Moved only 2 yalms in 1.5 seconds, probably stuck
                        utils.vars.running.stuck = 1
                    end
                end
                
                if windower.ffxi.get_player().target_locked then
                    -- Being locked on interferes with pathing.
                    windower.send_command('input /lockon off')
                    --player.target_locked = false
                end
                
                utils.movement_logic(c_coords)

            end
                    
            if utils.vars.fighting and not utils.vars.fighting.suspended then
                utils.engaging_logic(c_coords)
            end
            
            if os.clock() > utils.vars.lasttime + utils.vars.delay then
                utils.vars.lasttime = os.clock()
                utils.vars.delay = utils.vars._static.passive_delay
                
                if utils.vars.combat_routine then
                    utils.combat_logic()
                end				
            
            end
            
            coroutine.sleep(.1)
        else
            coroutine.sleep(.5)
        end

    end
    
    windower.unregister_event(inc_packet_event)
    windower.unregister_event(out_packet_event)
    windower.unregister_event(zone_event)
    
end

utils.initialize()
    
for i,v in pairs(windower.ffxi.get_mob_array()) do
    utils.vars.monster_array[v.index] = {}
    utils.vars.monster_array[v.index].s_ts = os.clock()
    utils.vars.monster_array[v.index].s_coords = {v.x,v.z,v.y}
    utils.vars.monster_array[v.index].attacking = false
end


utils.coroutine = coroutine.schedule(utils.run, 0)


return utils
