-- Display object
local texts = require('texts')

-- Conventional settings layout
local default_settings = {}
default_settings.numplayers = 8
default_settings.sbcolor = 204
default_settings.showallidps = true
default_settings.resetfilters = true
default_settings.visible = true
default_settings.showfellow = true
default_settings.UpdateFrequency = 0.5
default_settings.combinepets = true

default_settings.display = {}
default_settings.display.pos = {}
default_settings.display.pos.x = 0
default_settings.display.pos.y = 100

default_settings.display.bg = {}
default_settings.display.bg.alpha = 200
default_settings.display.bg.red = 0
default_settings.display.bg.green = 0
default_settings.display.bg.blue = 0

default_settings.display.text = {}
default_settings.display.text.size = 10
default_settings.display.text.font = 'Courier New'
default_settings.display.text.fonts = {}
default_settings.display.text.alpha = 255
default_settings.display.text.red = 255
default_settings.display.text.green = 255
default_settings.display.text.blue = 255

local Display = {
    visible = true,
    settings = nil,
    tb_name = 'Botter',
    options_table = T{}
}
Display.__index = Display
setmetatable(Display, {
  __call = function (cls, ...)
    return cls.new(...)
  end,
})

local valid_fonts = T{
    'fixedsys',
    'lucida console',
    'courier',
    'courier new',
    'ms mincho',
    'consolas',
    'dejavu sans mono'
}

local valid_fields = T{
    'name',
    'dps',
    'percent',
    'total',
    'mavg',
    'mrange',
    'critavg',
    'critrange',
    'ravg',
    'rrange',
    'rcritavg',
    'rcritrange',
    'acc',
    'racc',
    'crit',
    'rcrit',
    'wsavg'
}

function Display:set_position(posx, posy)
    self.text:pos(posx, posy)
end

function Display:new(title)
    local self = setmetatable({}, Display)
    self.settings = default_settings
    self.__index = self
    self.visible = self.settings.visible

    self.text = texts.new(self.settings.display, settings)

    if not valid_fonts:contains(self.settings.display.text.font:lower()) then
        error('Invalid font specified: ' .. self.settings.display.text.font)
        self.text:font(self.settings.display.text.font)
        self.text:size(self.settings.display.text.fontsize)
    else
        self.text:font(self.settings.display.text.font, 'consolas', 'courier new', 'monospace')
        self.text:size(self.settings.display.text.size)
    end

    self:visibility(self.visible)

    self.tb_name = title
    self:update()

    return self
end


function Display:visibility(v)
    if v then
        self.text:show()
    else
        self.text:hide()
    end

    self.visible = v
    self.settings.visible = v

    self:update()
end

function Display:build_header()
    return '%s\n':format(self.tb_name) .. '%s\n':format("-":rep(23))
end

-- Updates the main display with current filter/damage/dps status
function Display:update()
    if not self.visible then
        -- no need build a display while it's hidden
        return
    end

    local display_table = T{}
--[[
    local player_lines = 0
    local alli_damage = 0
    for k, v in pairs(damage_table) do
        if player_lines < self.settings.numplayers then
            local dps
            if dps_clock.clock == 0 then
                dps = "N/A"
            else
                dps = '%.2f':format(math.round(v[2] / dps_clock.clock, 2))
            end

            local percent
            if total_damage > 0 then
                percent = '(%.1f%%)':format(100 * v[2] / total_damage)
            else
                percent = '(0%)'
            end
            display_table:append('%-16s%7d%8s %7s':format(v[1], v[2], percent, dps))
        end
        alli_damage = alli_damage + v[2] -- gather this even for players not displayed
        player_lines = player_lines + 1
    end

    if self.settings.showallidps and dps_clock.clock > 0 then
        display_table:append('-':rep(17))
        display_table:append('Alli DPS: ' .. '%7.1f':format(alli_damage / dps_clock.clock))
    end--]]
    for k, v in pairs(self.options_table) do
        display_table:append(k .. ". " .. v)
    end

    self.text:text(self:build_header() .. table.concat(display_table, '\n'))
end

function Display:update_options(options_table)
    self.options_table = options_table
    self:update()
end


-- Takes a table of elements to be wrapped across multiple lines and returns
-- a table of strings, each of which fits within one FFXI line.
local function wrap_elements(elements, header, sep)
    local max_line_length = 120 -- game constant
    if not sep then
        sep = ', '
    end

    local lines = T{}
    local current_line = nil
    local line_length

    local i = 1
    while i <= #elements do
        if not current_line then
            current_line = T{}
            line_length = header:len()
            lines:append(current_line)
        end

        local new_line_length = line_length + elements[i]:len() + sep:len()
        if new_line_length > max_line_length then
            current_line = T{}
            lines:append(current_line)
            new_line_length = elements[i]:len() + sep:len()
        end

        current_line:append(elements[i])
        line_length = new_line_length
        i = i + 1
    end

    local baked_lines = lines:map(function (ls) return ls:concat(sep) end)
    if header:len() > 0 and #baked_lines > 0 then
        baked_lines[1] = header .. baked_lines[1]
    end

    return baked_lines
end


local function slow_output(chatprefix, lines, limit)
    -- this is funky but if we don't wait like this, the lines will spew too fast and error
    windower.send_command(lines:map(function (l) return chatprefix .. l end):concat('; wait 1.2 ; '))
end

function Display:reset()
end

_x = 0
_y = 0

-- Handle drag and drop
windower.register_event('mouse', function(type, x, y, delta, blocked)
    if blocked then
        return
    end

    _x = x
    _y = y
end)

function Display:selected()
    return self.text:hover(_x, _y)
end

function Display:get_text(selection)
    return self.options_table[selection]
end


return Display